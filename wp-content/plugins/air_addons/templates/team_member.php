<?php if (!defined('ABSPATH')) die('-1');

$css = $atts['css'] ? $atts['css'] : '';
$ex_class = $atts['ex_class'] ? ' ' . str_replace('.', '', $atts['ex_class']) : '';

$container_classes = function_exists('vc_shortcode_custom_css_class') ?
	vc_shortcode_custom_css_class($css, ' ') . $ex_class : '';

$grid_id = esc_attr(str_replace(':', '_', $atts['grid_id']));

?>

<div class="vc_clearfix"><div class="vc_clearfix wpb_content_element<?php echo esc_attr($container_classes); ?>">

	<div
		id="<?php echo $grid_id; ?>"
		class="
			team-member
			_<?php echo esc_attr($atts['text_position']); ?>
			_<?php echo esc_attr($atts['color_scheme']); ?>
			<?php echo ($atts['boxed'] ? '_boxed' : ''); ?>
		"
	>
		<div
			class="team-member__img-wrapper"
			<?php if (!empty($atts['img_overlay_color'])) {?>
				style="background-color:<?php echo esc_attr($atts['img_overlay_color']); ?>;"
			<?php } ?>
		>
			<img
				src="<?php
					$attachment_image_src = wp_get_attachment_image_src(absint($atts['img']), $atts['img_size']);
					echo esc_url($attachment_image_src[0]);
				?>"
				alt="<?php echo esc_attr($atts['title']); ?>"
				class="
					team-member__img
					<?php echo ($atts['img_scale'] ? '_scale' : ''); ?>
					_<?php echo esc_attr($atts['img_filter']); ?>
				"
			>

			<?php if ($atts['img_overlay'] != '') { ?>
				<div class="
					team-member__img-overlay
					_<?php echo esc_attr($atts['img_overlay']); ?>
					_<?php echo esc_attr($atts['img_overlay_type']); ?>
				"></div>
			<?php } ?>
		</div>

		<div class="
			team-member__content
			_<?php echo esc_attr($atts['text_vertical_align']); ?>
			_<?php echo esc_attr($atts['text_align']); ?>
		">

			<h5 class="team-member__title"><?php echo esc_attr($atts['title']); ?></h5>
			<div class="team-member__subtitle">
				<span class="team-member__subtitle-inner"><?php echo esc_attr($atts['job']); ?></span>
			</div>

			<?php if ($content != '' && $atts['text_position'] == '') { ?>
				<div class="team-member__desc"><?php echo wpb_js_remove_wpautop($content, true); ?></div>
			<?php } ?>

			<?php
			$social_links = '';

			foreach (self::$social_icons as $id => $icon_and_name) {
				if ($atts[$id] != '') {
					$social_links .= '
						<a
							href="' . esc_url($atts[$id]) . '"
							class="team-member__social-link _' . esc_attr($id) . '"
						>
							<i class="fa fa-' . esc_attr($id) . '"></i>
						</a>
					';
				}
			}

			if ($social_links != '') {
				echo '
					<div class="team-member__social-links ' . ($atts['brand_colors'] ? 'brand-colors' : '') . '">'
						. $social_links .
					'</div>
				';
			}
			?>

		</div>
	</div>

</div></div>
