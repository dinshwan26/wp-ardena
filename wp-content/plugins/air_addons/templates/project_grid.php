<?php if (!defined('ABSPATH')) die('-1');

$css = $atts['css'] ? $atts['css'] : '';
$ex_class = $atts['ex_class'] ? ' ' . str_replace('.', '', $atts['ex_class']) : '';

$container_classes = function_exists('vc_shortcode_custom_css_class') ?
	vc_shortcode_custom_css_class($css, ' ') . $ex_class : '';

$grid_id = esc_attr(str_replace(':', '_', $atts['grid_id']));

$gap = absint($atts['gap']);

$columns = absint($atts['items_columns']);

$column_classes = self::get_responsive_column_classes($columns);

$project_classes = array('project-card');

if ($atts['animation']) {
	$project_classes[] = '_has-animation';
	$project_classes[] = '_animation_' . $atts['animation'];
}

if ($atts['large_font_size']) {
	$project_classes[] = '_large';
}

?>

<div class="vc_clearfix"><div class="vc_clearfix wpb_content_element<?php echo esc_attr($container_classes); ?>">

	<?php
	$categories = get_terms(self::$taxonomy);
	if ($atts['filter'] && !empty($categories) && !is_wp_error($categories)) {
	?>
		<nav class="
			projects-categories
			js-masonry-filter
			text-<?php echo esc_attr($atts['filter_align']); ?>
		">

			<a
				href="#<?php echo $grid_id; ?>"
				data-filter="*"
				class="
					projects-categories__link
					_active
					js-masonry-filter-link
					<?php if ($atts['light_text_filter']) { echo '_light'; } ?>
				"
			>
				<?php esc_html_e('All', 'air_addons'); ?>
			</a>

			<?php foreach ($categories as $category) { ?>
				<a
					href="#<?php echo $grid_id; ?>"
					data-filter=".js-masonry-sizer, .js-masonry-filter-by-<?php echo esc_attr($category->slug); ?>"
					class="
						projects-categories__link
						js-masonry-filter-link
						<?php if ($atts['light_text_filter']) { echo '_light'; } ?>
					"
				>
					<?php echo esc_attr($category->name); ?>
				</a>
			<?php } ?>

		</nav>
	<?php } ?>

	<div
		id="<?php echo $grid_id; ?>"
		class="row js-masonry"
		style="margin-right:-<?php echo $gap; ?>px;margin-left:-<?php echo $gap; ?>px;"
	>

		<div class="js-masonry-sizer <?php echo esc_attr(join(' ', $column_classes)); ?>"></div>

		<?php
		while ($projects->have_posts()) { $projects->the_post();

			$double_width = false;
			$double_height = false;
			if ($atts['double'] && class_exists('MintOptions')) {
				$double_width = MintOptions::get_metaboxes_option('local_single_project--double_width');
				$double_height = MintOptions::get_metaboxes_option('local_single_project--double_height');
			}

			$column_classes = self::get_responsive_column_classes($columns, $double_width);

			$categories = get_the_terms(get_the_ID(), self::$taxonomy);

			$categories_html = '';
			$filter_classes = array();

			if (!empty($categories) && !is_wp_error($categories)) {
				$ar_categories = array();

				foreach ($categories as $category) {
					$ar_categories[] = '<span class="project-card__category">' . $category->name . '</span>';
					$filter_classes[] = 'js-masonry-filter-by-' . $category->slug;
				}

				$categories_html = '<div class="project-card__categories">';
				$categories_html .= join(", ", $ar_categories);
				$categories_html .= '</div>';
			}

			?>

			<div
				class="
					project-card-wrapper
					<?php echo esc_attr(join(' ', $column_classes)); ?>
					<?php echo esc_attr(join(' ', $filter_classes)); ?>
				"
				style="
					margin-bottom: <?php echo 2*$gap; ?>px;
					padding-right: <?php echo $gap; ?>px;
					padding-left: <?php echo $gap; ?>px;
				"
			>
				<div class="animate-on-screen js-animate-on-screen">
					<article <?php post_class($project_classes); ?> id="project-<?php the_ID(); ?>">
						<div class="project-card__img-wrapper">
							<?php
							if (has_post_thumbnail()) {

								$post_thumbnail_id = get_post_thumbnail_id();

								$img = wp_get_attachment_image_src($post_thumbnail_id, $atts['img_size']);
								$img_path = get_attached_file($post_thumbnail_id);
								$img_src = '';

								if ($double_width && !$double_height) {
									$img_src = self::get_custom_size_image_src($img_path, $img[1], $img[2]/2, true);
								} elseif ($double_height && !$double_width) {
									$img_src = self::get_custom_size_image_src($img_path, $img[1]/2, $img[2], true);
								}

								if (!$img_src) {
									$img_src = $img[0];
								}
							?>
								<img
									alt="<?php the_title(); ?>"
									src="<?php echo esc_url($img_src); ?>"
									class="project-card__img"
								>
							<?php } ?>

							<div
								class="project-card__img-overlay"
								style="background-color:<?php echo esc_attr($atts['img_overlay']); ?>"
							></div>

							<div class="project-card__img-icon"></div>
						</div>

						<div class="project-card__content-wrapper">
							<div class="project-card__content text-<?php echo esc_attr($atts['title_align']); ?>">
								<?php echo wp_kses($categories_html, 'post'); ?>
								<?php the_title('<h5 class="project-card__title">', '</h5>'); ?>
								<div class="project-card__content-icon"></div>
							</div>
						</div>

						<a
							href="<?php echo esc_url(get_permalink()); ?>"
							class="project-card__link"
							title="<?php the_title(); ?>"
						></a>
					</article>
				</div>
			</div>

		<?php } ?>

	</div>

</div></div>
