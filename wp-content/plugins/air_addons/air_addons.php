<?php
/*
 *  Plugin Name:  Air Addons
 *  Plugin URI:   http://air.redbrush.eu
 *  Author:       Redbrush
 *  Author URI:   http://redbrush.eu
 *  Description:  Premium addons for Visual Composer
 *  Version:      1.0.2
 *  Text Domain:  air_addons
 *  Domain Path:  /languages/
 */

if (!defined('ABSPATH')) die('-1');

define('AIR_ADDONS_DIR', plugin_dir_path(__FILE__));

require_once AIR_ADDONS_DIR . 'classes/Addons.php';
require_once AIR_ADDONS_DIR . 'classes/Projects.php';
require_once AIR_ADDONS_DIR . 'classes/shortcodes/ProjectGrid.php';
require_once AIR_ADDONS_DIR . 'classes/shortcodes/TeamMember.php';
