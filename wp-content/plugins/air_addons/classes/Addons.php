<?php if (!defined('ABSPATH')) die('-1');


class AirAddons {


	private static $_instance = null;

	protected static $prefix = 'air';

	protected static $title = 'Air Addons';

	protected static $plugin_dir = AIR_ADDONS_DIR;

	protected static $post_type = 'project';

	protected static $taxonomy = 'projects_category';

	protected static $social_icons = array(
		// don't forget to add brand colors in CSS
		'link'          => '<i class="fa fa-lg fa-fw fa-link"></i> &nbsp; Custom Link',
		'envelope'      => '<i class="fa fa-lg fa-fw fa-envelope"></i> &nbsp; Mail',
		'facebook'      => '<i class="fa fa-lg fa-fw fa-facebook"></i> &nbsp; Facebook',
		'twitter'       => '<i class="fa fa-lg fa-fw fa-twitter"></i> &nbsp; Twitter',
		'instagram'     => '<i class="fa fa-lg fa-fw fa-instagram"></i> &nbsp; Instagram',
		'vk'            => '<i class="fa fa-lg fa-fw fa-vk"></i> &nbsp; VK',
		'pinterest'     => '<i class="fa fa-lg fa-fw fa-pinterest"></i> &nbsp; Pinterest',
		'linkedin'      => '<i class="fa fa-lg fa-fw fa-linkedin"></i> &nbsp; LinkedIn',
		'dribbble'      => '<i class="fa fa-lg fa-fw fa-dribbble"></i> &nbsp; Dribbble',
		'behance'       => '<i class="fa fa-lg fa-fw fa-behance"></i> &nbsp; Behance',
		'google-plus'   => '<i class="fa fa-lg fa-fw fa-google-plus"></i> &nbsp; Google+',
		'youtube'       => '<i class="fa fa-lg fa-fw fa-youtube"></i> &nbsp; YouTube',
		'vimeo-square'  => '<i class="fa fa-lg fa-fw fa-vimeo-square"></i> &nbsp; Vimeo',
		'flickr'        => '<i class="fa fa-lg fa-fw fa-flickr"></i> &nbsp; Flickr',
		'tumblr'        => '<i class="fa fa-lg fa-fw fa-tumblr"></i> &nbsp; Tumblr',
		'foursquare'    => '<i class="fa fa-lg fa-fw fa-foursquare"></i> &nbsp; FourSquare',
		'wordpress'     => '<i class="fa fa-lg fa-fw fa-wordpress"></i> &nbsp; WordPress',
		'stumbleupon'   => '<i class="fa fa-lg fa-fw fa-stumbleupon"></i> &nbsp; StumbleUpon',
		'soundcloud'    => '<i class="fa fa-lg fa-fw fa-soundcloud"></i> &nbsp; SoundCloud',
		'vine'          => '<i class="fa fa-lg fa-fw fa-vine"></i> &nbsp; Vine',
		'skype'         => '<i class="fa fa-lg fa-fw fa-skype"></i> &nbsp; Skype',
		'github'        => '<i class="fa fa-lg fa-fw fa-github"></i> &nbsp; GitHub',
		'bitbucket'     => '<i class="fa fa-lg fa-fw fa-bitbucket"></i> &nbsp; Bitbucket',
		'twitch'        => '<i class="fa fa-lg fa-fw fa-twitch"></i> &nbsp; Twitch',
		'weibo'         => '<i class="fa fa-lg fa-fw fa-weibo"></i> &nbsp; Weibo',
		'tencent-weibo' => '<i class="fa fa-lg fa-fw fa-tencent-weibo"></i> &nbsp; Tecent Weibo',
		'renren'        => '<i class="fa fa-lg fa-fw fa-renren"></i> &nbsp; RenRen',
		'xing'          => '<i class="fa fa-lg fa-fw fa-xing"></i> &nbsp; Xing',
	);


	private function __construct() {}


	public static function init() {
		if(is_null(self::$_instance)) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}


	protected static function get_image_sizes($size = '') {
		global $_wp_additional_image_sizes;

		$sizes = array();
		$intermediate_image_sizes = get_intermediate_image_sizes();

		// Create the full array with sizes and crop info
		foreach($intermediate_image_sizes as $_size) {
			if (in_array($_size, array('thumbnail', 'medium', 'large'))) {
				$sizes[$_size] = array(
					'width'  => get_option($_size . '_size_w'),
					'height' => get_option($_size . '_size_h'),
					'crop'   => (bool) get_option($_size . '_crop'),
				);
			} elseif (isset($_wp_additional_image_sizes[$_size])) {
				$sizes[$_size] = array(
					'width'  => $_wp_additional_image_sizes[$_size]['width'],
					'height' => $_wp_additional_image_sizes[$_size]['height'],
					'crop'   => $_wp_additional_image_sizes[$_size]['crop']
				);
			}
		}

		// Get only 1 size if found
		if ($size) {
			if (isset($sizes[$size])) {
				return $sizes[$size];
			} else {
				return false;
			}
		}

		return $sizes;
	}


	protected static function get_image_size_names() {
		$size_names = array();
		$sizes = self::get_image_sizes();
		$names = apply_filters('image_size_names_choose', array(
			'thumbnail' => esc_html__('Thumbnail'),
			'medium'    => esc_html__('Medium'),
			'large'     => esc_html__('Large'),
			'full'      => esc_html__('Full Size'),
		));

		$size_names['full'] = $names['full'];

		foreach ($sizes as $id => $size) {
			$size_names[$id] = (empty($names[$id]) ? '' : $names[$id] . ' ') . $size['width'] . '×' . $size['height'];
		}

		return $size_names;
	}


	public static function get_custom_size_image_src($file, $width, $height, $crop = false) {
		if ($width || $height) {
			$editor = wp_get_image_editor($file);

			if (is_wp_error($editor) || is_wp_error($editor->resize($width, $height, $crop)))
				return false;

			$resized_file = $editor->save();

			if (!is_wp_error($resized_file) && $resized_file) {
				$upload_dir = wp_upload_dir();
				return str_replace($upload_dir['basedir'], $upload_dir['baseurl'], $resized_file['path']);
			}
		}
		return false;
	}


	public static function get_responsive_column_classes($columns = 1, $double_width = false) {

		$classes = array();

		if ($double_width) {

			if ($columns == 1) {
				$classes[] = 'col-xs-12';
			} elseif ($columns == 2) {
				$classes[] = 'col-xs-12';
			} elseif ($columns == 3) {
				$classes[] = 'col-xs-12 col-md-8';
			} elseif ($columns == 4) {
				$classes[] = 'col-xs-12 col-sm-8 col-md-6';
			} else {
				$classes[] = 'col-xs-12 col-sm-6 col-md-4';
			}

		} else {

			if ($columns == 1) {
				$classes[] = 'col-xs-12';
			} elseif ($columns == 2) {
				$classes[] = 'col-xs-12 col-md-6';
			} elseif ($columns == 3) {
				$classes[] = 'col-xs-12 col-sm-6 col-md-4';
			} elseif ($columns == 4) {
				$classes[] = 'col-xs-12 col-sm-4 col-md-3';
			} else {
				$classes[] = 'col-xs-6 col-sm-3 col-md-2';
			}

		}

		return $classes;

	}

}

// AirAddons::init();
