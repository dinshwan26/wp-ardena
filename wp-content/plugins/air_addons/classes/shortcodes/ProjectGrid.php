<?php if (!defined('ABSPATH')) die('-1');


class AirShortcodeProjectGrid extends AirAddons {


	private static $_instance = null;

	private static $shortcode = null;


	private function __construct() {
		self::$shortcode = self::$prefix . '_vc_' . self::$post_type . '_grid';

		add_action('init', array($this, 'integrate_with_vc'));

		add_filter(
			'vc_autocomplete_' . self::$shortcode . '_include_callback',
			array($this, 'grid_include_callback'), 10, 1
		);
		add_filter(
			'vc_autocomplete_' . self::$shortcode . '_include_render',
			array($this, 'grid_include_render'), 10, 1
		);
		add_filter(
			'vc_autocomplete_' . self::$shortcode . '_taxonomies_callback',
			array($this, 'grid_taxonomies_callback'), 10, 1
		);
		add_filter(
			'vc_autocomplete_' . self::$shortcode . '_taxonomies_render',
			array($this, 'grid_taxonomies_render'), 10, 1
		);

		// Creating a shortcode addon
		add_shortcode(self::$shortcode, array($this, 'render'));
	}


	public static function init() {
		if(is_null(self::$_instance)) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}


	public function grid_include_callback($search_string) {
		$query = $search_string;
		$data = array();
		$args = array(
			's' => $query,
			'post_type' => self::$post_type,
		);
		$args['vc_search_by_title_only'] = true;
		$args['numberposts'] = - 1;
		if (0 === strlen($args['s'])) {
			unset($args['s']);
		}
		add_filter('posts_search', 'vc_search_by_title_only', 500, 2);
		$posts = get_posts($args);
		if (is_array($posts) && ! empty($posts)) {
			foreach ($posts as $post) {
				$data[] = array(
					'value' => $post->ID,
					'label' => $post->post_title,
					'group' => $post->post_type,
				);
			}
		}

		return $data;
	}


	public function grid_include_render($value) {
		$post = get_post($value['value']);

		return is_null($post) ? false : array(
			'value' => $post->ID,
			'label' => $post->post_title,
			'group' => $post->post_type,
		);
	}


	public function grid_taxonomies_callback($search_string) {
		$data = array();
		$categories = get_terms(self::$taxonomy, array(
			'hide_empty' => false,
			'search' => $search_string,
		));
		if (! empty($categories) && ! is_wp_error($categories)){
			foreach ($categories as $category) {
				if (is_object($category)) {
					$data[] = array(
						'label'    => $category->name,
						'value'    => $category->term_id,
						'group_id' => $category->taxonomy,
					);
				}
			}
		}

		return $data;
	}


	public function grid_taxonomies_render($term) {
		$terms = get_terms(self::$taxonomy, array(
			'include' => array($term['value']),
			'hide_empty' => false,
		));
		$data = false;
		if (is_array($terms) && 1 === count($terms)) {
			$term = $terms[0];
			$data = vc_get_term_object($term);
		}

		return $data;
	}


	public function integrate_with_vc() {
		if (!function_exists('vc_map')) {
			return;
		}

		vc_map(array(
			'name' => esc_html__('Project Grid', 'air_addons'),
			'base' => self::$shortcode,
			'icon' => 'icon-wpb-application-icon-large',
			'content_element' => true,
			'category' => self::$title,
			'description' => esc_html__('Posts, pages or custom posts in grid', 'air_addons'),
			'params' => array(
				array(
					'type' => 'checkbox',
					'heading' => esc_html__('List of IDs', 'air_addons'),
					'param_name' => 'ids',
					'value' => array(esc_html__('Yes', 'air_addons') => 'yes'),
				),
				array(
					'type' => 'autocomplete',
					'heading' => __('Include only', 'air_addons'),
					'param_name' => 'include',
					'description' => __('Add projects by title.', 'air_addons'),
					'settings' => array(
						'multiple' => true,
						'sortable' => true,
						'groups' => true,
					),
					'dependency' => array(
						'element' => 'ids',
						'value' => array('yes'),
					),
				),
				array(
					'type' => 'checkbox',
					'heading' => esc_html__('Show filter', 'air_addons'),
					'param_name' => 'filter',
					'value' => array(esc_html__('Yes', 'air_addons') => 'yes'),
					'description' => esc_html__('Append filter to grid.', 'air_addons'),
				),
				array(
					'type' => 'dropdown',
					'heading' => esc_html__('Alignment', 'air_addons'),
					'param_name' => 'filter_align',
					'value' => array(
						esc_html__('Center', 'air_addons') => 'center',
						esc_html__('Left', 'air_addons') => 'left',
						esc_html__('Right', 'air_addons') => 'right',
					),
					'std' => 'center',
					'dependency' => array(
						'element' => 'filter',
						'value' => array('yes'),
					),
					'description' => esc_html__('Select filter alignment.', 'air_addons'),
				),
				array(
					'type' => 'checkbox',
					'heading' => esc_html__('Light text for filter', 'air_addons'),
					'param_name' => 'light_text_filter',
					'value' => array(esc_html__('Yes', 'air_addons') => 'yes'),
				),
				array(
					'type' => 'autocomplete',
					'heading' => esc_html__('Narrow data source', 'air_addons'),
					'param_name' => 'taxonomies',
					'settings' => array(
						'multiple' => true,
						'min_length' => 1,
						'groups' => true,
						'unique_values' => true,
						'display_inline' => true,
						'delay' => 500,
						'auto_focus' => true,
					),
					'param_holder_class' => 'vc_not-for-custom',
					'description' => esc_html__('Enter categories, tags or custom taxonomies.', 'air_addons'),
					'dependency' => array(
						'element' => 'ids',
						'value_not_equal_to' => array('yes'),
					),
				),
				array(
					'type' => 'textfield',
					'heading' => esc_html__('Total items', 'air_addons'),
					'param_name' => 'items',
					'value' => 9,
					'description' => esc_html__('Set max limit for items in grid or enter -1 to display all.', 'air_addons'),
					'dependency' => array(
						'element' => 'ids',
						'value_not_equal_to' => array('yes'),
					),
				),
				array(
					'type' => 'dropdown',
					'heading' => esc_html__('Grid columns', 'air_addons'),
					'param_name' => 'items_columns',
					'value' => array(
						'4 columns' => 4,
						'3 columns' => 3,
						'2 columns' => 2,
						'1 column' => 1,
					),
					'std' => 3,
					'description' => esc_html__('Select number of single grid columns.', 'air_addons'),
					'edit_field_class' => 'vc_col-sm-6 vc_column',
				),
				array(
					'type' => 'dropdown',
					'heading' => esc_html__('Gap', 'air_addons'),
					'param_name' => 'gap',
					'value' => array(
						'0px' => 0,
						'2px' => 1,
						'4px' => 2,
						'10px' => 5,
						'20px' => 10,
						'30px' => 15,
					),
					'std' => 15,
					'description' => esc_html__('Select gap between grid columns.', 'air_addons'),
					'edit_field_class' => 'vc_col-sm-6 vc_column',
				),
				array(
					'type' => 'checkbox',
					'heading' => esc_html__('Use double height and double width view', 'air_addons'),
					'param_name' => 'double',
					'value' => array(esc_html__('Yes', 'air_addons') => 'yes'),
				),
				array(
					'type' => 'dropdown',
					'heading' => esc_html__('Image size', 'air_addons'),
					'param_name' => 'img_size',
					'value' => array_flip(self::get_image_size_names()),
					'dependency' => array(
						'not_empty' => true,
					),
				),
				array(
					'type' => 'checkbox',
					'heading' => esc_html__('Large font size', 'air_addons'),
					'param_name' => 'large_font_size',
					'value' => array(esc_html__('Yes', 'air_addons') => 'yes'),
				),
				array(
					'type' => 'colorpicker',
					'heading' => esc_html__('Color to overlay image on hover', 'air_addons'),
					'param_name' => 'img_overlay',
				),
				array(
					'type' => 'dropdown',
					'heading' => esc_html__('Title align', 'air_addons'),
					'param_name' => 'title_align',
					'value' => array(
						esc_html__('Left', 'air_addons') => 'left',
						esc_html__('Center', 'air_addons') => 'center',
						esc_html__('Right', 'air_addons') => 'right',
					),
					'std' => 'center',
				),
				array(
					'type' => 'dropdown',
					'heading' => esc_html__('Animation on hover', 'air_addons'),
					'param_name' => 'animation',
					'value' => array(
						'No' => '',
						'Simple' => '1',
						'Simple (reverse)' => '2',
						'Blur' => '3',
						'Colorful' => '4',
						'Bordered' => '5',
						'Slice' => '6',
						'Grayscale' => '7',
						'Boxed' => '8',
						'Boxed Big' => '9',
						'Lined' => '10',
						'Lined Vertical' => '11',
					),
					'std' => '',
				),
				array(
					'type' => 'vc_grid_id',
					'param_name' => 'grid_id',
				),
				array(
					'type' => 'textfield',
					'heading' => esc_html__('Extra class name', 'air_addons'),
					'param_name' => 'ex_class',
					'description' => esc_html__('Style particular content element differently - add a class name and refer to it in custom CSS.', 'air_addons'),
				),
				array(
					'type' => 'css_editor',
					'heading' => esc_html__('CSS box', 'air_addons'),
					'param_name' => 'css',
					'group' => esc_html__('Design Options', 'air_addons'),
				),
			),
		));
	}


	public function render($atts, $content = null) {
		$atts = shortcode_atts(array(
			'ids' => false,
			'include' => '',
			'filter' => false,
			'filter_align' => 'center',
			'light_text_filter' => false,
			'taxonomies' => '',
			'items' => 9,
			'items_columns' => 3,
			'gap' => 15,
			'double' => false,
			'img_size' => 'full',
			'large_font_size' => false,
			'img_overlay' => '',
			'title_align' => 'center',
			'animation' => false,
			'grid_id' => false,
			'ex_class' => false,
			'css' => false,
		), $atts);

		if ($atts['ids']) {
			$args = array(
				'post_type' => self::$post_type,
				'post__in'  => explode(", ", $atts['include']),
			);
		} else {
			$args = array(
				'post_type' => self::$post_type,
				'posts_per_page' => $atts['items'],
				'post_status' => 'publish',
				'tax_query' => !$atts['taxonomies'] ?  array() : array(
					array(
						'taxonomy' => self::$taxonomy,
						'field'    => 'term_id',
						'terms'    => explode(", ", $atts['taxonomies']),
					),
				),
			);
		}

		$projects = new WP_Query($args);

		if ($projects->have_posts()) {
			ob_start();
			require self::$plugin_dir . 'templates/project_grid.php';
			$output = ob_get_contents();
			ob_get_clean();

			wp_reset_postdata();

			return $output;
		}
	}


}

AirShortcodeProjectGrid::init();
