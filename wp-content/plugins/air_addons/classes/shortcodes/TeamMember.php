<?php if (!defined('ABSPATH')) die('-1');


class AirShortcodeTeamMember extends AirAddons {


	private static $_instance = null;

	private static $shortcode = null;


	private function __construct() {
		self::$shortcode = self::$prefix . '_vc_team_member';

		add_action('init', array($this, 'integrate_with_vc'));

		// Creating a shortcode addon
		add_shortcode(self::$shortcode, array($this, 'render'));
	}


	public static function init() {
		if(is_null(self::$_instance)) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}


	public function integrate_with_vc() {
		if (!function_exists('vc_map')) {
			return;
		}

		vc_map(array(
			'name' => esc_html__('Team Member', 'air_addons'),
			'base' => self::$shortcode,
			'content_element' => true,
			'category' => self::$title,
			'description' => esc_html__('Display a team member with effects, description and social icons', 'air_addons'),
			'params' => array(
				array(
					'type' => 'textfield',
					'param_name' => 'title',
					'heading' => esc_html__('Name', 'air_addons'),
					'description' => esc_html__('Type your team member name.', 'air_addons'),
					'admin_label' => true,
				),
				array(
					'type' => 'textfield',
					'param_name' => 'job',
					'heading' => esc_html__('Job Title', 'air_addons'),
					'description' => esc_html__('Type your team member job title, e.g. Manager.', 'air_addons'),
				),
				array(
					'type' => 'attach_image',
					'param_name' => 'img',
					'heading' => esc_html__('Photo', 'air_addons'),
					'description' => esc_html__('Upload or select a photo from media gallery.', 'air_addons'),
					'value' => '',
				),
				array(
					'type' => 'dropdown',
					'param_name' => 'img_size',
					'heading' => esc_html__('Photo size', 'air_addons'),
					'value' => array_flip(self::get_image_size_names()),
					'dependency' => array(
						'element' => 'img',
						'not_empty' => true,
					),
				),
				array(
					'type' => 'textarea_html',
					'param_name' => 'content',
					'heading' => esc_html__('Description', 'air_addons'),
					'value' => '',
				),
				array(
					'type' => 'dropdown',
					'param_name' => 'text_position',
					'heading' => esc_html__('Text Position', 'air_addons'),
					'value' => array(
						esc_html__('Text and social links after photo', 'air_addons') => '',
						esc_html__('Text and social links on second slide (on hover)', 'air_addons') => 'card',
					),
					'std' => '',
				),
				array(
					'type' => 'dropdown',
					'param_name' => 'text_vertical_align',
					'heading' => esc_html__('Text Vertical Alignment', 'air_addons'),
					'value' => array(
						esc_html__('Top', 'air_addons') => 'top',
						esc_html__('Middle', 'air_addons') => 'middle',
						esc_html__('Bottom', 'air_addons') => 'bottom',
					),
					'std' => 'middle',
					'dependency' => array(
						'element' => 'text_position',
						'not_empty' => true,
					),
				),
				array(
					'type' => 'dropdown',
					'param_name' => 'text_align',
					'heading' => esc_html__('Text Alignment', 'air_addons'),
					'value' => array(
						esc_html__('Center', 'air_addons') => 'center',
						esc_html__('Left', 'air_addons') => 'left',
						esc_html__('Right', 'air_addons') => 'right',
					),
					'std' => 'center',
				),
				array(
					'type' => 'dropdown',
					'param_name' => 'color_scheme',
					'heading' => esc_html__('Color scheme', 'air_addons'),
					'value' => array(
						esc_html__('Light', 'air_addons') => 'light',
						esc_html__('Dark', 'air_addons') => 'dark',
					),
					'std' => 'light',
					'dependency' => array(
						'element' => 'text_position',
						'is_empty' => true,
					),
				),
				array(
					'type' => 'checkbox',
					'param_name' => 'boxed',
					'heading' => esc_html__('Boxed', 'air_addons'),
					'dependency' => array(
						'element' => 'text_position',
						'is_empty' => true,
					),
				),
				array(
					'type' => 'checkbox',
					'param_name' => 'img_scale',
					'heading' => esc_html__('Scale photo on hover', 'air_addons'),
					'dependency' => array(
						'element' => 'text_position',
						'not_empty' => true,
					),
				),
				array(
					'type' => 'dropdown',
					'param_name' => 'img_filter',
					'heading' => esc_html__('Filter for photo on hover', 'air_addons'),
					'value' => array(
						'Disable' => '',
						'Grayscale (colored to gray)' => 'gray-on-hover',
						'Grayscale (gray to colored)' => 'colored-on-hover',
					),
					'std' => '',
					'dependency' => array(
						'element' => 'text_position',
						'not_empty' => true,
					),
				),
				array(
					'type' => 'dropdown',
					'param_name' => 'img_overlay',
					'heading' => esc_html__('Overlay', 'air_addons'),
					'value' => array(
						esc_html__('Disable', 'air_addons') => '',
						esc_html__('Show always', 'air_addons') => 'show',
						esc_html__('Show on hover', 'air_addons') => 'show-on-hover',
						esc_html__('Hide on hover', 'air_addons') => 'hide-on-hover',
					),
					'std' => '',
					'dependency' => array(
						'element' => 'text_position',
						'not_empty' => true,
					),
				),
				array(
					'type' => 'dropdown',
					'param_name' => 'img_overlay_type',
					'heading' => esc_html__('Overlay Type', 'air_addons'),
					'value' => array(
						esc_html__('Color', 'air_addons') => '',
						esc_html__('Gradient', 'air_addons') => 'gradient',
					),
					'std' => '',
					'dependency' => array(
						'element' => 'img_overlay',
						'not_empty' => true,
					),
				),
				array(
					'type' => 'colorpicker',
					'param_name' => 'img_overlay_color',
					'heading' => esc_html__('Overlay Color', 'air_addons'),
					'dependency' => array(
						'element' => 'img_overlay_type',
						'is_empty' => true,
					),
				),
				array(
					'type' => 'vc_grid_id',
					'param_name' => 'grid_id',
				),
				array(
					'type' => 'textfield',
					'param_name' => 'ex_class',
					'heading' => esc_html__('Extra class name', 'air_addons'),
					'description' => esc_html__('Style particular content element differently - add a class name and refer to it in custom CSS.', 'air_addons'),
				),
				array(
					'type' => 'css_editor',
					'param_name' => 'css',
					'heading' => esc_html__('CSS box', 'air_addons'),
					'group' => esc_html__('Design Options', 'air_addons'),
				),
			),
		));

		$social_links = array();
		$social_links[] = array(
			'type' => 'checkbox',
			'param_name' => 'brand_colors',
			'heading' => esc_html__('Use brand colors', 'air_addons'),
			'group' => esc_html__('Social links', 'air_addons'),
		);
		foreach (self::$social_icons as $id => $icon_and_name) {
			$social_links[] = array(
				'type' => 'textfield',
				'param_name' => $id,
				'heading' => $icon_and_name,
				'group' => esc_html__('Social links', 'air_addons'),
			);
		}
		vc_add_params(self::$shortcode, $social_links);
	}


	public function render($atts, $content = null) {
		$default_social_links = array();
		foreach (self::$social_icons as $id => $icon_and_name) {
			$default_social_links[$id] = '';
		}

		$default_atts = array_merge(array(
			'title' => '',
			'job' => '',
			'img' => '',
			'img_size' => 'full',
			'color_scheme' => 'light',
			'boxed' => '',
			'text_position' => '',
			'text_vertical_align' => 'middle',
			'text_align' => 'center',
			'img_scale' => '',
			'img_filter' => '',
			'img_overlay' => '',
			'img_overlay_type' => '',
			'img_overlay_color' => '',
			'grid_id' => false,
			'ex_class' => false,
			'css' => false,
			'brand_colors' => false,
		), $default_social_links);

		$atts = shortcode_atts($default_atts, $atts);

		ob_start();
		require self::$plugin_dir . 'templates/team_member.php';
		$output = ob_get_contents();
		ob_get_clean();

		return $output;
	}


}

AirShortcodeTeamMember::init();
