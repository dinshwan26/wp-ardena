<?php
/**
 * The template part for displaying results in search pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 */

$post_type = get_post_type();

?>

<div class="search-results-card">
	<div class="search-results-card__img-wrapper">
		<?php if (has_post_thumbnail() && wp_get_attachment_image_src(get_post_thumbnail_id(), 'medium')[0] != '') { ?>
			<div
				class="search-results-card__img"
				style="background-image:url(<?php
					$attachment_image_src = wp_get_attachment_image_src(get_post_thumbnail_id(), 'medium');
					echo esc_url($attachment_image_src[0]);
				?>);"
			></div>
		<?php } ?>
	</div>

	<div class="search-results-card__content">
		<div class="search-results-card__label">
			<?php if ($post_type == 'post') { ?>
				<span class="icon-paper"></span>
				<?php esc_html_e('Post', 'mint'); ?>
			<?php } elseif ($post_type == 'page') { ?>
				<span class="icon-paper"></span>
				<?php esc_html_e('Page', 'mint'); ?>
			<?php } elseif ($post_type == 'product') { ?>
				<span class="icon-bag"></span>
				<?php esc_html_e('Product', 'woocommerce'); ?>
			<?php } elseif ($post_type == 'project') { ?>
				<span class="icon-briefcase"></span>
				<?php esc_html_e('Project', 'mint'); ?>
			<?php } ?>
		</div>

		<?php the_title(sprintf('
			<header><h5 class="search-results-card__title">
				<a href="%s" rel="bookmark" class="search-results-card__link">', esc_url(get_permalink())), '</a>
			</h5></header>
		'); ?>

		<div class="search-results-card__desc">
			<?php the_excerpt(); ?>
		</div>
	</div>

	<?php if ($post_type == 'post') { ?>
		<div class="search-results-card__meta">
			<a href="<?php echo esc_url(get_permalink()); ?>" rel="bookmark" class="search-results-card__link">
				<time datetime="<?php echo esc_attr(get_the_date('c')); ?>">
					<?php echo esc_html(get_the_date()); ?>
				</time>
			</a>
		</div>
	<?php } ?>
</div>
