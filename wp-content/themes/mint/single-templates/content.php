<?php
if (
	has_post_thumbnail() &&
	MintOptions::get('single_post--featured_image') &&
	(!MintHelpers::is_title_wrapper() || !MintOptions::get('title_wrapper--featured_image_on_bg'))
) {
	the_post_thumbnail('large', array('title' => esc_attr(get_the_title()), 'class' => 'post-single__img'));
}
?>

<?php if (!MintHelpers::is_title_wrapper()) { ?>
	<?php get_template_part('single-templates/part', 'top'); ?>
<?php } ?>

<div class="post-single__content">
	<?php the_content(); ?>
</div>

<?php get_template_part('single-templates/part', 'bottom'); ?>
