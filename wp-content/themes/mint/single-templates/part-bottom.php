<?php
$author = MintOptions::get('single_post--author');
$share = MintOptions::get('single_post--share');
?>

<?php
wp_link_pages(array(
	'before'      => '<nav class="post-pagination">',
	'after'       => '</nav>',
	'link_before' => '<span>',
	'link_after'  => '</span>',
));
?>

<?php if (get_the_tags() && MintOptions::get('single_post--tags')) { ?>
	<div class="post-single__tags">
		<?php the_tags('<h5 class="post-single__tags-title">'. esc_html__('Tags:', 'mint') . '</h5>', ''); ?>
	</div>
<?php } ?>

<?php if ($author || $share) { ?>
	<div class="row"><?php

		if ($author) {
			?>
			<div class="
				<?php
				if ($share) { echo 'col-md-4'; }
				else { echo 'col-md-12'; }
				?>
			">
				<?php MintHelpers::post_author(); ?>
			</div>
			<?php
		}

		if ($share) {
			?>
			<div class="
				<?php
				if ($author) { echo 'col-md-8 text-right-md'; }
				else { echo 'col-md-12'; }
				?>
			">
				<?php MintModules::share_buttons(); ?>
			</div>
			<?php
		}

	?></div>
<?php } ?>
