<div class="post-single__meta">

	<?php if (MintOptions::get('single_post--date')) { ?>
		<?php esc_html_e('Posted on', 'mint'); ?>
		<time datetime="<?php echo esc_attr(get_the_date('c')); ?>" class="post-single__meta-date">
			<?php echo esc_html(get_the_date()); ?>
		</time>
	<?php } ?>

	<?php if (MintOptions::get('single_post--author')) { ?>
		<span class="post-single__meta-separator">/</span>
		<?php esc_html_e("by", 'mint'); ?>
		<a href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>" class="post-single__meta-author-link">
			<?php the_author(); ?>
		</a>
	<?php } ?>

	<?php if (MintHelpers::is_categorized_blog() && MintOptions::get('single_post--categories')) { ?>
		<span class="post-single__meta-separator">/</span>
		<?php esc_html_e("in", 'mint'); ?>
		<span class="post-single__meta-categories"><?php the_category(', '); ?></span>
	<?php } ?>

	<?php if (
		!post_password_required() &&
		comments_open() &&
		get_comments_number() != 0 &&
		MintOptions::get('single_post--comments_counter')
	) { ?>
		<span class="post-single__meta-separator">/</span>
		<a href="<?php comments_link(); ?>" class="post-single__meta-comments-link">
			<span class="icon-speech-bubble"></span>
			<?php comments_number('', esc_html__('1 comment', 'mint'), esc_html__('% comments', 'mint')); ?>
		</a>
	<?php } ?>

</div>

<?php the_title('<header><h1 class="post-single__title">', '</h1></header>'); ?>
