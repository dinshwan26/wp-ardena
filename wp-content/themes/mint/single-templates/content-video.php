<?php $current_post_content = MintPostFormats::video(); ?>

<?php if (!MintHelpers::is_title_wrapper()) { ?>
	<?php get_template_part('single-templates/part', 'top'); ?>
<?php } ?>

<div class="post-single__content">
	<?php echo apply_filters('the_content', $current_post_content); ?>
</div>

<?php get_template_part('single-templates/part', 'bottom'); ?>
