<?php
/**
 * The template for displaying all pages.
 */

get_header(); ?>

	<?php while (have_posts()) { the_post(); ?>

		<article id="page-<?php the_ID(); ?>" <?php post_class(); ?>>
			<?php the_content(); ?>

			<?php
			wp_link_pages(array(
				'before'      => '<nav class="post-pagination">',
				'after'       => '</nav>',
				'link_before' => '<span>',
				'link_after'  => '</span>',
			));
			?>
		</article>

		<?php
		if ((comments_open() || get_comments_number()) && MintOptions::get('general--page_comments')) {
			comments_template();
		}
		?>

	<?php } ?>

<?php get_footer(); ?>
