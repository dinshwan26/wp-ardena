<?php
/**
 * The template for displaying all single projects.
 */

get_header(); ?>

	<?php while (have_posts()) { the_post(); ?>

		<article id="project-<?php the_ID(); ?>" <?php post_class(); ?>>
			<?php the_content(); ?>

			<?php
			wp_link_pages(array(
				'before'      => '<nav class="post-pagination">',
				'after'       => '</nav>',
				'link_before' => '<span>',
				'link_after'  => '</span>',
			));
			?>
		</article>

		<?php
		if (MintOptions::get('single_project--nav')) {
			MintHelpers::post_navigation();
		}
		?>

	<?php } ?>

<?php get_footer(); ?>
