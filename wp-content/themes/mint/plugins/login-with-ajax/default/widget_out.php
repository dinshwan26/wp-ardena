<?php
/**
 * This is the page users will see logged out.
 * You can edit this, but for upgrade safety you should copy and modify this file into your template folder.
 * The location from within your template folder is plugins/login-with-ajax/
 * (create these directories if they don't exist)
 */
?>

<div class="lwa lwa-default"><?php //class must be here, and if this is a template, class name should be that of template directory ?>
	<form class="lwa-form js-lwa-login" action="<?php echo esc_attr(LoginWithAjax::$url_login); ?>" method="post">

		<h6 class="lwa__title"><?php esc_html_e('Log In', 'login-with-ajax'); ?></h6>

		<?php if (!empty($lwa_data['remember'])) { ?>
			<a
				class="js-lwa-show-remember lwa__remember-link"
				href="<?php echo esc_url(LoginWithAjax::$url_remember); ?>"
				title="<?php esc_attr_e('Password Lost and Found', 'login-with-ajax'); ?>"
			>
				<?php esc_attr_e('Lost your password?', 'login-with-ajax'); ?>
			</a>
		<?php } ?>

		<div class="lwa__row _big">
			<span class="lwa-status"></span>
		</div>

		<div class="lwa__row">
			<label class="lwa__field-wrapper" title="<?php esc_html_e('Username', 'login-with-ajax'); ?>">
				<span class="lwa__field-icon"><span class="icon-head"></span></span>
				<input
					type="text"
					name="log"
					placeholder="<?php esc_html_e('Username', 'login-with-ajax'); ?>"
					class="lwa__field"
				>
			</label>
		</div>

		<div class="lwa__row">
			<label class="lwa__field-wrapper" title="<?php esc_html_e('Password', 'login-with-ajax'); ?>">
				<span class="lwa__field-icon"><span class="icon-lock"></span></span>
				<input
					type="password"
					name="pwd"
					placeholder="<?php esc_html_e('Password', 'login-with-ajax'); ?>"
					class="lwa__field"
				>
			</label>
		</div>

		<?php do_action('login_form'); ?>

		<div class="lwa__row _big">
			<label>
				<input name="rememberme" type="checkbox" value="forever">
				<?php esc_html_e('Remember Me', 'login-with-ajax'); ?>
			</label>
		</div>


		<div class="lwa__row">
			<input
				type="submit"
				name="wp-submit"
				id="lwa_wp-submit"
				value="<?php esc_attr_e('Log In', 'login-with-ajax'); ?>"
				tabindex="100"
			>
			<input type="hidden" name="lwa_profile_link" value="<?php echo esc_attr($lwa_data['profile_link']); ?>">
			<input type="hidden" name="login-with-ajax" value="login">
			<?php if (!empty($lwa_data['redirect'])) { ?>
				<input type="hidden" name="redirect_to" value="<?php echo esc_url($lwa_data['redirect']); ?>">
			<?php } ?>
		</div>

		<?php if (get_option('users_can_register') && !empty($lwa_data['registration'])) { ?>
			<div class="lwa__row">
				<a
					href="<?php echo esc_url(LoginWithAjax::$url_register); ?>"
					class="js-lwa-show-register button _light"
				>
					<?php esc_html_e('Register', 'login-with-ajax'); ?>
				</a>
			</div>
		<?php } ?>

	</form>


	<?php if (!empty($lwa_data['remember'])) { ?>
		<form
			class="lwa-remember js-lwa-remember"
			action="<?php echo esc_attr(LoginWithAjax::$url_remember); ?>"
			method="post"
			style="display:none;"
		>

			<h6 class="lwa__title"><?php esc_html_e('Forgotten Password', 'login-with-ajax'); ?></h6>

			<div class="lwa__row _big">
				<span class="lwa-status"></span>
			</div>

			<div class="lwa__row">
				<label class="lwa__field-wrapper" title="<?php esc_html_e('Enter username or email', 'login-with-ajax'); ?>">
					<span class="lwa__field-icon"><span class="icon-head"></span></span>
					<input
						type="text"
						name="user_login"
						class="lwa__field"
						placeholder="<?php esc_html_e('Enter username or email', 'login-with-ajax'); ?>"
					>
				</label>
				<?php do_action('lostpassword_form'); ?>
			</div>

			<div class="lwa__separator"></div>

			<div class="lwa__row">
				<input
					type="submit"
					value="<?php esc_attr_e("Get New Password", 'login-with-ajax'); ?>"
					class="lwa-button-remember"
				>
				<input type="hidden" name="login-with-ajax" value="remember">
			</div>

			<div class="lwa__row">
				<a href="#" class="js-lwa-hide-remember button _light">
					<?php esc_html_e('Cancel', 'login-with-ajax'); ?>
				</a>
			</div>

		</form>
	<?php } ?>


	<?php if (get_option('users_can_register') && !empty($lwa_data['registration'])) { ?>
		<div class="lwa-register lwa-register-default js-lwa-register" style="display:none;">

			<h6 class="lwa__title"><?php esc_html_e('Register For This Site', 'login-with-ajax'); ?></h6>

			<div class="lwa__row _big">
				<em><?php esc_html_e('A password will be e-mailed to you.', 'login-with-ajax'); ?></em>
			</div>

			<form
				class="lwa-register-form"
				action="<?php echo esc_attr(LoginWithAjax::$url_register); ?>"
				method="post"
			>

				<div class="lwa__row _big">
					<span class="lwa-status"></span>
				</div>

				<div class="lwa__row">
					<label class="lwa__field-wrapper" title="<?php esc_html_e('Username', 'login-with-ajax'); ?>">
						<span class="lwa__field-icon"><span class="icon-head"></span></span>
						<input
							type="text"
							name="user_login"
							id="user_login"
							placeholder="<?php esc_html_e('Username', 'login-with-ajax'); ?>"
							class="lwa__field"
						>
					</label>
				</div>

				<div class="lwa__row">
					<label class="lwa__field-wrapper" title="<?php esc_html_e('E-mail', 'login-with-ajax'); ?>">
						<span class="lwa__field-icon"><span class="icon-head"></span></span>
						<input
							type="email"
							name="user_email"
							id="user_email"
							placeholder="<?php esc_html_e('E-mail', 'login-with-ajax'); ?>"
							class="lwa__field"
						>
					</label>
				</div>

				<div class="lwa__separator"></div>

				<div class="lwa__row">
					<?php do_action('register_form'); ?>
					<?php do_action('lwa_register_form'); ?>
					<input
						type="submit"
						name="wp-submit"
						id="wp-submit"
						class="button-primary"
						value="<?php esc_attr_e('Register', 'login-with-ajax'); ?>"
						tabindex="100"
					>
					<input type="hidden" name="login-with-ajax" value="register">
				</div>

				<div class="lwa__row">
					<a href="#" class="js-lwa-hide-register button _light">
						<?php esc_html_e('Cancel', 'login-with-ajax'); ?>
					</a>
				</div>

			</form>

		</div>
	<?php } ?>

</div>
