<?php
/**
 * The main template file.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 */

get_header();


$post_type = esc_attr(get_post_type());
$content_width = esc_attr(MintOptions::get('layout--content_width'));

if ($post_type == 'post') {


	global $mint_post_loop;
	$mint_post_loop = 1;

	$posts_template = MintOptions::get('posts--template');

	$classes = array();
	$classes[] = 'row js-masonry';

	if ($posts_template == 'metro' && $content_width == 'expanded') {
		$classes[] = '_expanded';
	}
	?>

	<?php if ($posts_template != 'standard' && $posts_template != 'boxed') { ?>
		<div class="<?php echo esc_attr(join(' ', $classes)); ?>">
	<?php } elseif (!MintHelpers::get_sidebar_location()) { ?>
		<div class="row">
			<div class="col-lg-10 col-lg-offset-1">
	<?php } ?>

	<?php if (have_posts()) { ?>

		<?php while (have_posts()) { the_post(); ?>

			<?php get_template_part( 'post-templates/template', $posts_template ); ?>

		<?php } ?>

	<?php } else { ?>

		<?php get_template_part( 'content', 'none' ); ?>

	<?php } ?>

	<?php if ($posts_template != 'standard' && $posts_template != 'boxed') { ?>
		</div>
		<?php MintHelpers::posts_navigation(); ?>
	<?php } elseif (!MintHelpers::get_sidebar_location()) { ?>
				<?php MintHelpers::posts_navigation(); ?>
			</div>
		</div>
	<?php } else { ?>
		<?php MintHelpers::posts_navigation(); ?>
	<?php } ?>


<?php } elseif ( $post_type == 'project' && class_exists('AirAddons') ) { ?>


	<?php if (have_posts()) { ?>

		<?php
		$categories = get_terms('projects_category');
		if (!empty($categories) && !is_wp_error($categories) && !is_tax('projects_category')) {
		?>
			<div class="row">
				<nav class="projects-categories js-masonry-filter __<?php echo esc_attr(MintOptions::get('projects--categories_align')); ?>">
					<a
						href="#projects"
						data-filter="*"
						class="projects-categories__link _active js-masonry-filter-link"
					>
						<?php esc_html_e('All', 'mint'); ?>
					</a>
					<?php foreach ($categories as $category) { ?>
						<a
							href="#projects"
							data-filter=".js-masonry-sizer, .js-masonry-filter-by-<?php echo esc_attr($category->slug); ?>"
							class="projects-categories__link js-masonry-filter-link"
						>
							<?php echo esc_attr($category->name); ?>
						</a>
					<?php } ?>
				</nav>
			</div>
		<?php } ?>

		<?php $gap = absint(MintOptions::get('projects--gap')) / 2; ?>

		<div
			id="projects"
			class="row js-masonry"
			style="margin-right:-<?php echo $gap; ?>px;margin-left:-<?php echo $gap; ?>px;"
		>

			<div class="
				js-masonry-sizer
				<?php echo esc_attr(
					join(
						' ',
						AirAddons::get_responsive_column_classes(MintOptions::get('projects--columns'))
					)
				); ?>
			"></div>

			<?php while (have_posts()) { the_post(); ?>

				<?php get_template_part( 'content', 'project' ); ?>

			<?php } ?>

		</div>

	<?php } else { ?>

		<?php get_template_part( 'content', 'none' ); ?>

	<?php } ?>


	<?php MintHelpers::posts_navigation('projects'); ?>


<?php } else { ?>


	<?php while (have_posts()) { the_post(); ?>

		<article id="<?php echo $post_type; ?>-<?php the_ID(); ?>" <?php post_class(); ?>>
			<?php the_content(); ?>

			<?php
			wp_link_pages( array(
				'before'      => '<nav class="post-pagination">',
				'after'       => '</nav>',
				'link_before' => '<span>',
				'link_after'  => '</span>',
			) );
			?>
		</article>

		<?php
		if ((comments_open() || get_comments_number()) && MintOptions::get('general--page_comments')) {
			comments_template();
		}
		?>

	<?php } ?>


<?php } ?>


<?php get_footer(); ?>
