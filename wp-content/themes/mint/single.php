<?php
/**
 * The template for displaying all single posts and attachments.
 */

get_header(); ?>

	<?php while (have_posts()) { the_post(); ?>

		<article id="post-<?php the_ID(); ?>" <?php post_class('post-single'); ?>>
			<?php get_template_part('single-templates/content', get_post_format()); ?>
		</article>

		<?php
		if (comments_open() || get_comments_number()) {
			comments_template();
		}
		?>

		<?php
		if (MintOptions::get('single_post--nav')) {
			MintHelpers::post_navigation();
		}
		?>

	<?php } ?>

<?php get_footer(); ?>
