<?php
/**
 * The template part for displaying projects.
 */

// don't load directly
if (!defined('ABSPATH')) die('-1');

$double_width = MintOptions::get_metaboxes_option('local_single_project--double_width');
$double_height = MintOptions::get_metaboxes_option('local_single_project--double_height');

$column_classes = AirAddons::get_responsive_column_classes(MintOptions::get('projects--columns'), $double_width);

$gap = absint(MintOptions::get('projects--gap')) / 2;

$categories_html = '';
$filter_classes = array();

$categories = get_the_terms(get_the_ID(), 'projects_category');

if (!empty($categories) && !is_wp_error($categories)) {

	$ar_categories = array();

	foreach ($categories as $category) {
		$ar_categories[] = $category->name;
		$filter_classes[] = 'js-masonry-filter-by-' . $category->slug;
	}

	$categories_html = '<div class="project-card__categories">';
	$categories_html .= join(", ", $ar_categories);
	$categories_html .= '</div>';
}

$project_classes = array('project-card');
$project_classes[] = '_' . MintOptions::get('projects--font_size');
$animation = MintOptions::get('projects--animation');
if ($animation) {
	$project_classes[] = '_has-animation';
	$project_classes[] = '_animation_' . $animation;
}

?>

<div
	class="
		project-card-wrapper
		<?php echo esc_attr(join(' ', $column_classes)); ?>
		<?php echo esc_attr(join(' ', $filter_classes)); ?>
	"
	style="
		margin-bottom: <?php echo 2*$gap; ?>px;
		padding-right: <?php echo $gap; ?>px;
		padding-left: <?php echo $gap; ?>px;
	"
>
	<div class="animate-on-screen js-animate-on-screen">
		<article <?php post_class($project_classes); ?> id="project-<?php the_ID(); ?>">
			<div class="project-card__img-wrapper">
				<?php
				if (has_post_thumbnail()) {
					$post_thumbnail_id = get_post_thumbnail_id();

					$img = wp_get_attachment_image_src($post_thumbnail_id, MintOptions::get('projects--img_size'));
					$img_path = get_attached_file($post_thumbnail_id);
					$img_src = '';

					if ($double_width && !$double_height) {
						$img_src = AirAddons::get_custom_size_image_src($img_path, $img[1], $img[2]/2, true);
					} elseif ($double_height && !$double_width) {
						$img_src = AirAddons::get_custom_size_image_src($img_path, $img[1]/2, $img[2], true);
					}
					if (!$img_src) {
						$img_src = $img[0];
					}
				?>
					<img
						alt="<?php the_title(); ?>"
						src="<?php echo esc_url($img_src); ?>"
						class="project-card__img"
					>
				<?php } ?>

				<div
					class="project-card__img-overlay"
					style="background-color:<?php
						$bg_overlay = MintOptions::get('projects--bg_overlay');
						echo esc_attr($bg_overlay['rgba']);
					?>"
				></div>

				<div class="project-card__img-icon"></div>
			</div>

			<div class="project-card__content-wrapper">
				<div class="project-card__content text-<?php echo esc_attr(MintOptions::get('projects--align')); ?>">
					<?php echo wp_kses_post($categories_html); ?>
					<?php the_title('<h5 class="project-card__title">', '</h5>'); ?>
					<div class="project-card__content-icon"></div>
				</div>
			</div>

			<a
				href="<?php echo esc_url(get_permalink()); ?>"
				class="project-card__link"
				title="<?php the_title(); ?>"
			></a>
		</article>
	</div>
</div>
