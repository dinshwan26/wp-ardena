<?php
/**
 * The template for displaying product content within loops
 *
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     3.0.0
 */

if (!defined('ABSPATH')) {
	exit;
}

global $product, $woocommerce_loop;

// Ensure visibility
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}

// Store loop count we're currently on
if ( empty( $woocommerce_loop['loop'] ) ) {
	$woocommerce_loop['loop'] = 0;
}

// Store column count for displaying the grid
if ( empty( $woocommerce_loop['columns'] ) ) {
	$woocommerce_loop['columns'] = apply_filters('loop_shop_columns', 4);
}

$column_classes = MintHelpers::get_responsive_column_classes($woocommerce_loop['columns'], $woocommerce_loop['loop']);

?>

<div class="<?php echo esc_attr(join(' ', $column_classes)); ?>">
	<div class="animate-on-screen js-animate-on-screen">
		<div <?php post_class('product-card'); ?>>

			<div class="product-card__top">
				<?php
				/**
				 * woocommerce_before_shop_loop_item hook.
				 *
				 * @hooked woocommerce_show_product_loop_sale_flash - 10
				 * @hooked woocommerce_template_loop_product_thumbnail - 10
				 */
				do_action( 'woocommerce_before_shop_loop_item' );
				?>
			</div>

			<div class="product-card__bottom">
				<?php
				/**
				 * woocommerce_before_shop_loop_item_title hook
				 */
				do_action( 'woocommerce_before_shop_loop_item_title' );
				?>

				<h6 class="product-card__title">
					<?php
					/**
					 * woocommerce_shop_loop_item_title hook
					 *
					 * @hooked woocommerce_template_loop_product_title - 10
					 */
					do_action( 'woocommerce_shop_loop_item_title' );
					?>
				</h6>

				<?php
				/**
				 * woocommerce_after_shop_loop_item_title hook
				 *
				 * @hooked woocommerce_template_loop_price - 10
				 */
				do_action( 'woocommerce_after_shop_loop_item_title' );
				?>
			</div>

			<?php
			$attachment_ids = $product->get_gallery_attachment_ids();
			if ($attachment_ids) {
				foreach ($attachment_ids as $attachment_id) {

					$image = wp_get_attachment_image_src($attachment_id, 'shop_catalog');

					if (empty($image)) continue;

					?>
					<div
						class="product-card__back-img"
						style="background-image:url(<?php echo esc_url($image[0]); ?>);"
					></div>
					<?php

					break;
				}
			}
			?>

			<a
				href="<?php the_permalink(); ?>"
				title="<?php the_title(); ?>"
				class="product-card__link"
			></a>

			<?php if (class_exists('YITH_WCWL')) { ?>
				<div class="product-card__add-to-wishlist add-to-wishlist-wrapper">
					<?php echo do_shortcode('[yith_wcwl_add_to_wishlist]'); ?>
				</div>
			<?php } ?>

			<div class="product-card__buttons">
				<?php
				/**
				 * woocommerce_after_shop_loop_item hook
				 *
				 * @hooked quick_view - 1
				 * @hooked woocommerce_template_loop_add_to_cart - 10
				 */
				do_action( 'woocommerce_after_shop_loop_item' );
				?>
			</div>

		</div>
	</div>
</div>
