<?php
/**
 * Edit address form
 *
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     3.0.9
 */

if (!defined('ABSPATH')) {
	exit;
}

$page_title = ($load_address === 'billing') ?
	esc_html__('Billing address', 'woocommerce') :
	esc_html__('Shipping address', 'woocommerce');

do_action('woocommerce_before_edit_account_address_form'); ?>

<?php if (!$load_address) { ?>

	<?php wc_get_template('myaccount/my-address.php'); ?>

<?php } else { ?>

	<div class="o-hidden"><div class="row">
		<div class="col-md-8 col-md-offset-2">
			<form method="post" class="wc-form">

				<h3 class="wc-form__title">
					<?php echo apply_filters('woocommerce_my_account_edit_address_title', $page_title, $load_address); ?>
				</h3>

				<?php do_action("woocommerce_before_edit_address_form_{$load_address}"); ?>

				<?php
				foreach ($address as $key => $field) {
					if (isset($field['country_field'], $address[$field['country_field']])) {
						$field['country'] = wc_get_post_data_by_key(
							$field['country_field'],
							$address[$field['country_field']]['value']
						);
					}
					woocommerce_form_field($key, $field, wc_get_post_data_by_key($key, $field['value']));
				}
				?>

				<?php do_action("woocommerce_after_edit_address_form_{$load_address}"); ?>

				<p class="form-row form-row-wide">
					<input type="submit" class="full-width" name="save_address" value="<?php esc_attr_e('Save address', 'woocommerce'); ?>">
					<?php wp_nonce_field('woocommerce-edit_address'); ?>
					<input type="hidden" name="action" value="edit_address">
				</p>

			</form>
		</div>
	</div></div>

<?php } ?>

<?php do_action('woocommerce_after_edit_account_address_form'); ?>
