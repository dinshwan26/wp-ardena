<?php
/**
 * Lost password reset form.
 *
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     3.0.0
 */

if (!defined('ABSPATH')) {
	exit;
}

wc_print_notices(); ?>

<div class="o-hidden"><div class="row">
	<div class="col-md-6 col-md-offset-3">

		<h1 class="text-center"><?php esc_html_e('Reset password', 'woocommerce'); ?></h1>

		<form method="post" class="woocommerce-ResetPassword lost_reset_password wc-form">

			<p class="wc-lead text-center"><?php echo apply_filters('woocommerce_reset_password_message', esc_html__('Enter a new password below.', 'woocommerce')); ?></p>

			<p class="form-row">
				<label for="password_1">
					<?php esc_html_e('New password', 'woocommerce'); ?>
					<abbr class="required" title="required">*</abbr>
				</label>
				<input type="password" class="woocommerce-Input woocommerce-Input--text input-text" name="password_1" id="password_1">
			</p>

			<p class="form-row">
				<label for="password_2">
					<?php esc_html_e('Re-enter new password', 'woocommerce'); ?>
					<abbr class="required" title="required">*</abbr>
				</label>
				<input type="password" class="woocommerce-Input woocommerce-Input--text input-text" name="password_2" id="password_2">
			</p>

			<input type="hidden" name="reset_key" value="<?php echo esc_attr($args['key']); ?>">
			<input type="hidden" name="reset_login" value="<?php echo esc_attr($args['login']); ?>">

			<?php do_action('woocommerce_resetpassword_form'); ?>

			<p class="form-row">
				<input type="hidden" name="wc_reset_password" value="true">
				<input type="submit" class="full-width" value="<?php esc_attr_e('Save', 'woocommerce'); ?>">
			</p>

			<?php wp_nonce_field('reset_password'); ?>

		</form>

	</div>
</div></div>
