<?php
/**
 * Lost password form
 *
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     3.0.0
 */

if (!defined('ABSPATH')) {
	exit;
}

wc_print_notices(); ?>

<div class="row">
	<div class="col-md-6 col-md-offset-3">

		<h1 class="text-center"><?php esc_html_e('Reset password', 'woocommerce'); ?></h1>

		<form method="post" class="woocommerce-ResetPassword lost_reset_password wc-form">

			<p class="wc-lead text-center"><?php echo apply_filters('woocommerce_lost_password_message', esc_html__('Lost your password? Please enter your username or email address. You will receive a link to create a new password via email.', 'woocommerce')); ?></p>

			<p class="woocommerce-FormRow woocommerce-FormRow--first form-row">
				<label for="user_login">
					<?php esc_html_e('Username or email', 'woocommerce'); ?>
					<abbr class="required" title="required">*</abbr>
				</label>
				<input class="woocommerce-Input woocommerce-Input--text input-text" type="text" name="user_login" id="user_login" required>
			</p>

			<?php do_action('woocommerce_lostpassword_form'); ?>

			<p class="woocommerce-FormRow form-row">
				<input type="hidden" name="wc_reset_password" value="true">
				<input type="submit" class="full-width" value="<?php esc_attr_e('Reset password', 'woocommerce'); ?>">
			</p>

			<?php wp_nonce_field('lost_password'); ?>

		</form>

	</div>
</div>
