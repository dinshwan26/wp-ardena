<?php
/**
 * Share template
 *
 * @author      Your Inspiration Themes
 * @package     YITH WooCommerce Wishlist
 * @version     2.0.13
 */

if (!defined('YITH_WCWL')) {
	exit;
}

MintModules::share_buttons();
