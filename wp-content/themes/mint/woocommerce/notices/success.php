<?php
/**
 * Show messages
 *
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     1.6.4
 */

if (!defined('ABSPATH')) {
	exit;
}

if (!$messages) {
	return;
}

?>

<div class="wc-message _success">
	<div class="wc-message__icon"><i class="fa fa-check"></i></div>
	<div class="wc-message__content">
		<?php foreach ($messages as $message) { ?>
			<div class="woocommerce-message"><?php echo wp_kses_post($message); ?></div>
		<?php } ?>
	</div>
</div>
