<?php
/**
 * Show error messages
 *
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     1.6.4
 */

if (!defined('ABSPATH')) {
	exit;
}

if (!$messages) {
	return;
}

?>

<div class="wc-message _error">
	<div class="wc-message__icon"><i class="fa fa-exclamation-triangle"></i></div>
	<div class="wc-message__content">
		<ul class="woocommerce-error">
			<?php foreach ($messages as $message) { ?>
				<li><?php echo wp_kses_post($message); ?></li>
			<?php } ?>
		</ul>
	</div>
</div>
