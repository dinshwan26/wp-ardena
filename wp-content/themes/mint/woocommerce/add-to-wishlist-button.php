<?php
/**
 * Add to wishlist button template
 *
 * @author      Your Inspiration Themes
 * @package     YITH WooCommerce Wishlist
 * @version     2.0.0
 */

global $product;

?>

<a
	href="<?php echo esc_url(add_query_arg('add_to_wishlist', $product_id)); ?>"
	rel="nofollow"
	data-product-id="<?php echo esc_attr($product_id); ?>"
	data-product-type="<?php echo esc_attr($product_type); ?>"
	class="add-to-wishlist__link <?php echo esc_attr($link_classes); ?>"
	title="<?php echo esc_html($label); ?>"
>
	<span class="add-to-wishlist__icon">
		<?php echo empty($icon) ? '<span class="icon-heart"></span>' : wp_kses_post($icon); ?>
	</span>
	<span class="add-to-wishlist__text"><?php echo esc_html($label); ?></span>
</a>
