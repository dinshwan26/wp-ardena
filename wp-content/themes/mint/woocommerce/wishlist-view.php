<?php
/**
 * Wishlist page template
 *
 * @author      Your Inspiration Themes
 * @package     YITH WooCommerce Wishlist
 * @version     2.0.12
 */

if (!defined('YITH_WCWL')) {
	exit;
}

?>

<?php do_action('yith_wcwl_before_wishlist_form', $wishlist_meta); ?>

<form
	class="woocommerce wishlist"
	id="yith-wcwl-form"
	action="<?php echo esc_url(YITH_WCWL()->get_wishlist_url('view' . ($wishlist_meta['is_default'] != 1 ? '/' . $wishlist_meta['wishlist_token'] : ''))); ?>"
	method="post"
>

	<?php wp_nonce_field('yith-wcwl-form', 'yith_wcwl_form_nonce'); ?>


	<?php do_action('yith_wcwl_before_wishlist'); ?>


	<div class="products-table-wrapper">
	<table
		class="products-table _full-width shop_table wishlist_table"
		data-pagination="<?php echo esc_attr($pagination); ?>"
		data-per-page="<?php echo esc_attr($per_page); ?>"
		data-page="<?php echo esc_attr($current_page); ?>"
		data-id="<?php echo (is_user_logged_in()) ? esc_attr($wishlist_meta['ID']) : ''; ?>"
		data-token="<?php echo (!empty($wishlist_meta['wishlist_token']) && is_user_logged_in()) ? esc_attr($wishlist_meta['wishlist_token']) : ''; ?>"
	>

		<thead>
			<tr>

				<?php $column_count = 0; ?>

				<?php if ($show_cb) { ?>
					<th class="products-table__title product-checkbox"><?php $column_count++; ?>
						<input type="checkbox" value="" name="" id="bulk_add_to_cart">
					</th>
				<?php } ?>

				<th class="products-table__title product-thumbnail"><?php $column_count++; ?></th>

				<th class="products-table__title text-left product-name"><?php $column_count++; ?>
					<span class="nobr">
						<?php echo apply_filters(
							'yith_wcwl_wishlist_view_name_heading',
							esc_html__('Product Name', 'yith-woocommerce-wishlist')
						); ?>
					</span>
				</th>

				<?php if ($show_price) { ?>
					<th class="products-table__title product-price"><?php $column_count++; ?>
						<span class="nobr">
							<?php echo apply_filters(
								'yith_wcwl_wishlist_view_price_heading',
								esc_html__('Unit Price', 'yith-woocommerce-wishlist')
							); ?>
						</span>
					</th>

				<?php } ?>

				<?php if ($show_stock_status) { ?>
					<th class="products-table__title product-stock-stauts"><?php $column_count++; ?>
						<span class="nobr">
							<?php echo apply_filters(
								'yith_wcwl_wishlist_view_stock_heading',
								esc_html__('Stock Status', 'yith-woocommerce-wishlist')
							); ?>
						</span>
					</th>
				<?php } ?>

				<?php if ($show_last_column) { ?>
					<th class="products-table__title product-add-to-cart"><?php $column_count++; ?></th>
				<?php } ?>

				<?php if ($is_user_owner) { ?>
					<th class="products-table__title product-remove"><?php $column_count++; ?></th>
				<?php } ?>

			</tr>
		</thead>

		<tbody>

			<?php
			if (count($wishlist_items) > 0) {
				foreach($wishlist_items as $item) {
					global $product;

					if (function_exists('wc_get_product')) {
						$product = wc_get_product($item['prod_id']);
					} else {
						$product = get_product($item['prod_id']);
					}

					if ($product !== false && $product->exists()) {
						$availability = $product->get_availability();
						$stock_status = $availability['class'];
						?>

						<tr
							class="products-table__item"
							id="yith-wcwl-row-<?php echo esc_attr($item['prod_id']); ?>"
							data-row-id="<?php echo esc_attr($item['prod_id']); ?>"
						>

							<?php if ($show_cb) { ?>
								<td class="products-table__item-column product-checkbox">
									<input
										type="checkbox"
										value="<?php echo esc_attr($item['prod_id']); ?>"
										name="add_to_cart[]"
										<?php echo ($product->product_type != 'simple') ? 'disabled' : ''; ?>
									>
								</td>
							<?php } ?>

							<td class="products-table__item-column _thumbnail product-thumbnail">
								<a href="<?php echo esc_url(get_permalink(apply_filters('woocommerce_in_cart_product', $item['prod_id']))); ?>">
									<?php echo esc_html($product->get_image()); ?>
								</a>
							</td>

							<td class="products-table__item-column _product product-name">
								<a href="<?php echo esc_url(get_permalink(apply_filters('woocommerce_in_cart_product', $item['prod_id']))); ?>">
									<?php echo apply_filters('woocommerce_in_cartproduct_obj_title', $product->get_title(), $product); ?>
								</a>
								<?php do_action('yith_wcwl_table_after_product_name', $item); ?>
							</td>

							<?php if ($show_price) { ?>
								<td class="products-table__item-column _price product-price">
									<?php
									if (is_a($product, 'WC_Product_Bundle')) {
										if ($product->min_price != $product->max_price) {
											echo sprintf('%s - %s', wc_price($product->min_price), wc_price($product->max_price));
										} else {
											echo wc_price($product->min_price);
										}
									} elseif ($product->price != '0') {
										echo wp_kses_post($product->get_price_html());
									} else {
										echo apply_filters('yith_free_text', esc_html__('Free!', 'yith-woocommerce-wishlist'));
									}
									?>
								</td>
							<?php } ?>

							<?php if ($show_stock_status) { ?>
								<td class="products-table__item-column product-stock-status">
									<?php
									if ($stock_status == 'out-of-stock') {
										$stock_status = "Out";
										echo '<span class="product-label wishlist-out-of-stock">'
											. esc_html__('Out of Stock', 'yith-woocommerce-wishlist') .
										'</span>';
									} else {
										$stock_status = "In";
										echo '<span class="product-label _in-stock wishlist-in-stock">'
											. esc_html__('In Stock', 'yith-woocommerce-wishlist') .
										'</span>';
									}
									?>
								</td>
							<?php } ?>

							<?php if ($show_last_column) { ?>
								<td class="products-table__item-column product-add-to-cart">
									<?php if ($show_add_to_cart && isset($stock_status) && $stock_status != 'Out') { ?>
										<?php
										if (function_exists('woocommerce_template_loop_add_to_cart')) {
											woocommerce_template_loop_add_to_cart();
										} else {
											wc_get_template('loop/add-to-cart.php');
										}
										?>
									<?php } ?>
								</td>
							<?php } ?>

							<?php if ($is_user_owner) { ?>
								<td class="products-table__item-column _remove product-remove">
									<a
										href="<?php echo esc_url(add_query_arg('remove_from_wishlist', $item['prod_id'])); ?>"
										class="remove remove_from_wishlist"
										title="<?php esc_attr_e('Remove this product', 'yith-woocommerce-wishlist'); ?>"
									>
										<span class="icon-cross"></span>
									</a>
								</td>
							<?php } ?>
						</tr>
					<?php
					}
				}
			} else { ?>

				<tr>
					<td
						class="products-table__item-column wishlist-empty"
						colspan="<?php echo esc_attr($column_count); ?>"
					>
						<?php esc_html_e('No products were added to the wishlist', 'yith-woocommerce-wishlist'); ?>
					</td>
				</tr>

			<?php } ?>

			<?php if (!empty($page_links)) { ?>
				<tr class="pagination-row">
					<td
						class="products-table__item-column"
						colspan="<?php echo esc_attr($column_count); ?>"
					>
						<?php echo wp_kses_post($page_links); ?>
					</td>
				</tr>
			<?php } ?>

		</tbody>

		<tfoot>
			<tr>
				<td class="products-table__footer" colspan="<?php echo esc_attr($column_count); ?>">

					<?php if ($show_cb) { ?>
						<div class="custom-add-to-cart-button-cotaniner">
							<a
								href="<?php echo esc_url(add_query_arg(array('wishlist_products_to_add_to_cart' => '', 'wishlist_token' => $wishlist_meta['wishlist_token']))); ?>"
								class="button alt"
								id="custom_add_to_cart"
							>
								<?php echo apply_filters('yith_wcwl_custom_add_to_cart_text', esc_html__('Add the selected products to the cart', 'yith-woocommerce-wishlist')); ?>
							</a>
						</div>
					<?php } ?>

					<?php if (is_user_logged_in() && $is_user_owner && $show_ask_estimate_button && $count > 0) { ?>
						<div class="ask-an-estimate-button-container">
							<a
								href="<?php echo ($additional_info) ? '#ask_an_estimate_popup' : esc_url($ask_estimate_url); ?>"
								class="button ask-an-estimate-button"
								<?php echo ($additional_info) ? 'data-rel="prettyPhoto[ask_an_estimate]"' : ''; ?>
							>
								<?php echo apply_filters('yith_wcwl_ask_an_estimate_icon', '<span class="icon-bag"></span>'); ?>
								<?php echo apply_filters('yith_wcwl_ask_an_estimate_text', esc_html__('Ask for an estimate', 'yith-woocommerce-wishlist')); ?>
							</a>
						</div>
					<?php } ?>

					<?php
					do_action('yith_wcwl_before_wishlist_share');

					if (is_user_logged_in() && $is_user_owner && $wishlist_meta['wishlist_privacy'] != 2 && $share_enabled) {
						yith_wcwl_get_template('share.php', $share_atts);
					}

					do_action('yith_wcwl_after_wishlist_share');
					?>

				</td>
			</tr>
		</tfoot>

	</table>

	<?php wp_nonce_field('yith_wcwl_edit_wishlist_action', 'yith_wcwl_edit_wishlist'); ?>

	<?php if ($wishlist_meta['is_default'] != 1) { ?>
		<input type="hidden" value="<?php echo esc_attr($wishlist_meta['wishlist_token']); ?>" name="wishlist_id" id="wishlist_id">
	<?php } ?>

	<?php do_action('yith_wcwl_after_wishlist'); ?>

</form>

<?php do_action('yith_wcwl_after_wishlist_form', $wishlist_meta); ?>

<?php if ($additional_info) { ?>
	<div id="ask_an_estimate_popup">
		<form action="<?php echo esc_url($ask_estimate_url); ?>" method="post" class="wishlist-ask-an-estimate-popup">
			<?php if (!empty($additional_info_label)) { ?>
				<label for="additional_notes"><?php echo esc_html($additional_info_label); ?></label>
			<?php } ?>
			<textarea id="additional_notes" name="additional_notes"></textarea>

			<button class="ask-an-estimate-button ask-an-estimate-button-popup" >
				<?php echo apply_filters('yith_wcwl_ask_an_estimate_icon', '<span class="icon-bag"></span>'); ?>
				<?php esc_html_e('Ask for an estimate', 'yith-woocommerce-wishlist'); ?>
			</button>
		</form>
	</div>
<?php } ?>
