<?php
/**
 * Loop Add to Cart
 *
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     3.0.0
 */

if (!defined('ABSPATH')) {
	exit;
}

global $product;

echo apply_filters(
	'woocommerce_loop_add_to_cart_link',

	'<div class="add-to-cart-wrapper">
		<a
			href="' . esc_url($product->add_to_cart_url()) . '"
			rel="nofollow"
			data-product_id="' . esc_attr($product->get_id()) . '"
			data-product_sku="' . esc_attr($product->get_sku()) . '"
			data-quantity="' . esc_attr(isset($quantity) ? $quantity : 1) .'"
			class="
				add-to-cart
				button
				_' . esc_attr($product->product_type) . '
				' . (empty($class) ? '' : esc_attr($class)) . '
			"
		>
			<span class="add-to-cart__icon">
				<span class="icon-bag"></span>
			</span>
			<span class="add-to-cart__loader">
				<span class="icon-check"></span>
			</span>
			<span class="add-to-cart__text">' . esc_html($product->add_to_cart_text()) . '</span>
		</a>
	</div>',

	$product
);
