<?php
/**
 * Loop Price
 *
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     1.6.4
 */

if (!defined('ABSPATH')) {
	exit;
}

global $product;

$suffix = 'card';

if ($products_template = MintOptions::get('products--template')) {
	$suffix = $products_template;
}

?>

<?php if ($price_html = $product->get_price_html()) { ?>
	<div class="product-<?php echo esc_attr($suffix); ?>__price price"><?php echo wp_kses_post($price_html); ?></div>
<?php } ?>
