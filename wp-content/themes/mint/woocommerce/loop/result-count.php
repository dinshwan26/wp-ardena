<?php
/**
 * Result Count
 *
 * Shows text: Showing x - x of x results.
 *
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     3.0.0
 */

if (!defined('ABSPATH')) {
	exit;
}

global $wp_query;

?><div class="row"><?php

if (!woocommerce_products_will_display())
	return;

?>
	<div class="col-sm-6">
		<div class="products-list-counter">
			<?php
			$paged    = max(1, $wp_query->get('paged'));
			$per_page = $wp_query->get('posts_per_page');
			$total    = $wp_query->found_posts;
			$first    = ($per_page * $paged) - $per_page + 1;
			$last     = min($total, $wp_query->get('posts_per_page') * $paged);

			if ($total <= $per_page || -1 === $per_page) {
				/* translators: %d: total results */
				printf(
					_n(
						'Showing the single result',
						'Showing all %d results',
						$total,
						'woocommerce'
					),
					$total
				);
			} else {
				/* translators: 1: first result 2: last result 3: total results */
				printf(
					_nx(
						'Showing the single result',
						'Showing %1$d&ndash;%2$d of %3$d results',
						$total,
						'with first and last result',
						'woocommerce'
					),
					$first,
					$last,
					$total
				);
			}
			?>
		</div>
	</div>
