<?php
/**
 * Product loop sale flash
 *
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     1.6.4
 */

if (!defined('ABSPATH')) {
	exit;
}

global $post, $product;

$suffix = 'card';

if ($products_template = MintOptions::get('products--template')) {
	$suffix = $products_template;
}

?>

<?php if ($product->is_on_sale()) { ?>

	<?php echo apply_filters(
		'woocommerce_sale_flash',
		'<div class="product-' . esc_attr($suffix) . '__sale-label-wrapper">
			<div class="product-label _sale _' . esc_attr($suffix) . '">'
				. esc_html__('Sale!', 'woocommerce') .
			'</div>
		</div>',
		$post,
		$product
	); ?>

<?php } ?>
