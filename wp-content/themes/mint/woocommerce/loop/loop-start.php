<?php
/**
 * Product Loop Start
 *
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     2.0.0
 */

if (MintOptions::get('products--template') == 'tile') {

	$classes = '';

	if (MintOptions::get('layout--content_width') == 'expanded') {
		$classes = 'row _expanded';
	}

	?><div class="<?php echo esc_attr($classes); ?>"><div class="product-tiles-wrapper"><?php

} else {

	?><div><div class="clearfix"><?php

}
