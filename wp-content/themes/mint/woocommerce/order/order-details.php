<?php
/**
 * Order details
 *
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     3.0.0
 */

if (!defined('ABSPATH')) {
	exit;
}

if (!$order = wc_get_order($order_id)) {
	return;
}

$show_purchase_note = $order->has_status(apply_filters('woocommerce_purchase_note_order_statuses', array('completed', 'processing')));
$show_customer_details = is_user_logged_in() && $order->get_user_id() === get_current_user_id();

?>

<div class="o-hidden"><div class="row">

	<div class="col-sm-6">
		<div class="wc-box">
			<h2 class="wc-box__title"><?php esc_html_e('Order details', 'woocommerce'); ?></h2>
			<table class="shop_table order_details">
				<thead>
					<tr>
						<th class="product-name"><?php esc_html_e('Product', 'woocommerce'); ?></th>
						<th class="product-total"><?php esc_html_e('Total', 'woocommerce'); ?></th>
					</tr>
				</thead>
				<tbody>
					<?php
					foreach($order->get_items() as $item_id => $item) {
						$product = apply_filters('woocommerce_order_item_product', $item->get_product(), $item);

						wc_get_template('order/order-details-item.php', array(
							'order'              => $order,
							'item_id'            => $item_id,
							'item'               => $item,
							'show_purchase_note' => $show_purchase_note,
							'purchase_note'      => $product ? $product->get_purchase_note() : '',
							'product'            => $product,
						));
					}
					?>
					<?php do_action('woocommerce_order_items_table', $order); ?>
				</tbody>
				<tfoot>
					<?php foreach ($order->get_order_item_totals() as $key => $total) { ?>
						<tr>
							<th scope="row"><?php echo wp_kses_post($total['label']); ?></th>
							<td><?php echo wp_kses_post($total['value']); ?></td>
						</tr>
					<?php } ?>
				</tfoot>
			</table>

			<?php do_action('woocommerce_order_details_after_order_table', $order); ?>

		</div>
	</div>

	<?php if ($show_customer_details) { ?>
		<?php wc_get_template('order/order-details-customer.php', array('order' =>  $order)); ?>
	<?php } ?>

</div></div>
