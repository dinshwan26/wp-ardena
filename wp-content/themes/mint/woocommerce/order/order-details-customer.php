<?php
/**
 * Order Customer Details
 *
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     3.0.0
 */

if (!defined('ABSPATH')) {
	exit;
}

?>

<div class="col-sm-6">
	<div class="wc-box">

		<h2 class="wc-box__title"><?php esc_html_e('Customer details', 'woocommerce'); ?></h2>

		<table class="shop_table customer_details"><tfoot>
			<?php if ($order->get_customer_note()) { ?>
				<tr>
					<th><?php esc_html_e('Note:', 'woocommerce'); ?></th>
					<td><?php echo wptexturize($order->get_customer_note()); ?></td>
				</tr>
			<?php } ?>

			<?php if ($order->get_billing_email()) { ?>
				<tr>
					<th><?php esc_html_e('Email:', 'woocommerce'); ?></th>
					<td><?php echo esc_html($order->get_billing_email()); ?></td>
				</tr>
			<?php } ?>

			<?php if ($order->get_billing_phone()) { ?>
				<tr>
					<th><?php esc_html_e('Phone:', 'woocommerce'); ?></th>
					<td><?php echo esc_html($order->get_billing_phone()); ?></td>
				</tr>
			<?php } ?>

			<?php do_action('woocommerce_order_details_after_customer_details', $order); ?>
		</tfoot></table>

		<h3 class="wc-box__title"><?php esc_html_e('Billing address', 'woocommerce'); ?></h3>
		<address>
			<?php echo ($address = $order->get_formatted_billing_address()) ? $address : esc_html__('N/A', 'woocommerce'); ?>
		</address>

		<?php if (!wc_ship_to_billing_address_only() && $order->needs_shipping_address()) { ?>
			<h3 class="wc-box__title"><?php esc_html_e('Shipping address', 'woocommerce'); ?></h3>
			<address>
				<?php echo ($address = $order->get_formatted_shipping_address()) ? $address : esc_html__('N/A', 'woocommerce'); ?>
			</address>
		<?php } ?>

	</div>
</div>
