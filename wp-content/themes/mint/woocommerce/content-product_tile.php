<?php
/**
 * The template for displaying product content within loops
 *
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     2.6.1
 */

if (!defined('ABSPATH')) {
	exit;
}

global $post, $product;

// Ensure visibility
if (empty($product) || !$product->is_visible()) {
	return;
}

?>

<div <?php post_class('product-tile'); ?>>
	<div class="animate-on-screen js-animate-on-screen">
		<div class="product-tile__inner">

			<div class="product-tile__left">
				<a
					href="<?php the_permalink(); ?>"
					title="<?php the_title(); ?>"
					class="product-tile__link"
				>
					<?php
					/**
					 * woocommerce_before_shop_loop_item hook.
					 *
					 * @hooked woocommerce_show_product_loop_sale_flash - 10
					 * @hooked woocommerce_template_loop_product_thumbnail - 10
					 */
					do_action( 'woocommerce_before_shop_loop_item' );
					?>
				</a>
			</div>

			<div class="product-tile__right">
				<?php
				/**
				 * woocommerce_before_shop_loop_item_title hook
				 */
				do_action( 'woocommerce_before_shop_loop_item_title' );
				?>

				<h6 class="product-tile__title">
					<a
						href="<?php the_permalink(); ?>"
						title="<?php the_title(); ?>"
						class="product-tile__link"
					>
						<?php
						/**
						 * woocommerce_shop_loop_item_title hook
						 *
						 * @hooked woocommerce_template_loop_product_title - 10
						 */
						do_action( 'woocommerce_shop_loop_item_title' );
						?>
					</a>
				</h6>

				<?php if ($post->post_excerpt) { ?>
					<div class="product-tile__desc">
						<?php echo apply_filters('woocommerce_short_description', $post->post_excerpt); ?>
					</div>
				<?php } ?>

				<?php
				/**
				 * woocommerce_after_shop_loop_item_title hook
				 *
				 * @hooked woocommerce_template_loop_price - 10
				 */
				do_action( 'woocommerce_after_shop_loop_item_title' );
				?>

				<div class="product-tile__buttons">
					<?php if (class_exists('YITH_WCWL')) { ?>
						<div class="product-tile__add-to-wishlist add-to-wishlist-wrapper">
							<?php echo do_shortcode('[yith_wcwl_add_to_wishlist]'); ?>
						</div>
					<?php } ?>

					<?php
					/**
					 * woocommerce_after_shop_loop_item hook
					 *
					 * @hooked quick_view - 1
					 * @hooked woocommerce_template_loop_add_to_cart - 10
					 */
					do_action( 'woocommerce_after_shop_loop_item' );
					?>
				</div>
			</div>

		</div>
	</div>
</div>
