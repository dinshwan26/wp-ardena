<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive.
 *
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     2.0.0
 */

if (!defined('ABSPATH')) {
	exit;
}

$products_template_name = 'product';

$products_template = MintOptions::get('products--template');

if ($products_template == 'tile') {
	$products_template_name = 'product_tile';
}

get_header('shop'); ?>

	<?php
	/**
	 * woocommerce_before_main_content hook
	 *
	 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
	 * @hooked WC_Structured_Data::generate_website_data() - 30
	 */
	do_action('woocommerce_before_main_content');
	?>

		<?php if (MintOptions::get('products--category_image')) { ?>

			<div class="products-category-img"><?php do_action('woocommerce_category_image'); ?></div>

		<?php } ?>

		<?php if (have_posts()) { ?>

			<?php
			/**
			 * woocommerce_before_shop_loop hook
			 *
			 * @hooked wc_print_notices - 10
			 * @hooked woocommerce_result_count - 20
			 * @hooked woocommerce_catalog_ordering - 30
			 */
			do_action('woocommerce_before_shop_loop');
			?>

			<?php woocommerce_product_loop_start(); ?>

				<?php
				if (!MintHelpers::is_title_wrapper() && $products_template != 'tile') {
					woocommerce_product_subcategories();
				}
				?>

				<?php while (have_posts()) { the_post(); ?>

					<?php
					/**
					 * woocommerce_shop_loop hook.
					 *
					 * @hooked WC_Structured_Data::generate_product_data() - 10
					 */
					do_action('woocommerce_shop_loop');
					?>

					<?php wc_get_template_part('content', $products_template_name); ?>

				<?php } ?>

			<?php woocommerce_product_loop_end(); ?>

			<?php
			/**
			 * woocommerce_after_shop_loop hook
			 *
			 * @hooked woocommerce_pagination - 10
			 */
			do_action('woocommerce_after_shop_loop');
			?>

		<?php } elseif (!woocommerce_product_subcategories(array(
			'before' => woocommerce_product_loop_start(false),
			'after' => woocommerce_product_loop_end(false)
		))) { ?>

			<?php
			/**
			 * woocommerce_no_products_found hook.
			 *
			 * @hooked wc_no_products_found - 10
			 */
			do_action('woocommerce_no_products_found');
			?>

		<?php } ?>

	<?php
	/**
	 * woocommerce_after_main_content hook
	 *
	 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
	 */
	do_action('woocommerce_after_main_content');
	?>

<?php get_footer('shop'); ?>
