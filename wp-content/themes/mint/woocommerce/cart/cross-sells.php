<?php
/**
 * Cross-sells
 *
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     3.0.0
 */

if (!defined('ABSPATH')) {
	exit;
}

if ($cross_sells) { ?>

	<div class="col-md-12">
		<div class="cross-sells">
			<h2 class="cross-sells__title"><?php esc_html_e('You may be interested in&hellip;', 'woocommerce') ?></h2>

			<?php woocommerce_product_loop_start(); ?>

				<?php foreach ($cross_sells as $cross_sell) { ?>

					<?php
					$post_object = get_post($cross_sell->get_id());

					setup_postdata($GLOBALS['post'] =& $post_object);

					wc_get_template_part('content', 'product');
					?>

				<?php } ?>

			<?php woocommerce_product_loop_end(); ?>
		</div>
	</div>

<?php }

wp_reset_postdata();
