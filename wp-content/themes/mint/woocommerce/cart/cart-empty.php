<?php
/**
 * Empty cart page
 *
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     3.1.0
 */

if (!defined('ABSPATH')) {
	exit;
}

wc_print_notices();

?>

<div class="no-results-page">
	<p class="no-results-page__label _cart"><span class="icon-bag"></span></p>

	<h2 class="no-results-page__title">
		<?php esc_html_e('Your cart is currently empty.', 'woocommerce'); ?>
	</h2>

	<?php if (wc_get_page_id('shop') > 0) { ?>
		<p class="no-results-page__desc">
			<a
				class="no-results-page__link wc-backward"
				href="<?php echo esc_url(
					apply_filters('woocommerce_return_to_shop_redirect', wc_get_page_permalink('shop'))
				); ?>"
			>
				<?php esc_html_e('Return To Shop', 'woocommerce'); ?>
			</a>
		</p>
	<?php } ?>
</div>
