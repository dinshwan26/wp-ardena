<?php
/**
 * Add to wishlist template
 *
 * @author      Your Inspiration Themes
 * @package     YITH WooCommerce Wishlist
 * @version     2.0.0
 */

global $product;

$browse_wishlist_text = apply_filters('yith-wcwl-browse-wishlist-label', $browse_wishlist_text);

?>

<div class="add-to-wishlist yith-wcwl-add-to-wishlist add-to-wishlist-<?php echo esc_attr($product_id); ?>">

	<?php if (!($disable_wishlist && !is_user_logged_in())) { ?>

		<div
			class="
				yith-wcwl-add-button
				<?php echo ($exists && !$available_multi_wishlist) ? 'hide': 'show'; ?>
			"
			style="display:<?php echo ($exists && !$available_multi_wishlist) ? 'none': 'block'; ?>"
		>
			<?php yith_wcwl_get_template('add-to-wishlist-' . $template_part . '.php', $atts); ?>
		</div>

		<div class="yith-wcwl-wishlistaddedbrowse hide" style="display:none;">
			<a
				href="<?php echo esc_url($wishlist_url); ?>"
				class="add-to-wishlist__link _added"
				title="<?php echo esc_attr($browse_wishlist_text); ?>"
			>
				<span class="add-to-wishlist__icon _added">
					<span class="icon-heart"></span>
				</span>
				<span class="add-to-wishlist__text">
					<?php echo esc_html($product_added_text); ?>
					<?php echo esc_html($browse_wishlist_text); ?>
				</span>
			</a>
		</div>

		<div
			class="
				yith-wcwl-wishlistexistsbrowse
				<?php echo ($exists && !$available_multi_wishlist) ? 'show' : 'hide'; ?>
			"
			style="display:<?php echo ($exists && !$available_multi_wishlist) ? 'block' : 'none'; ?>"
		>
			<a
				href="<?php echo esc_url($wishlist_url); ?>"
				class="add-to-wishlist__link _added"
				title="<?php echo esc_attr($browse_wishlist_text); ?>"
			>
				<span class="add-to-wishlist__icon _added">
					<span class="icon-heart"></span>
				</span>
				<span class="add-to-wishlist__text">
					<?php echo esc_html($already_in_wishslist_text); ?>
					<?php echo esc_html($browse_wishlist_text); ?>
				</span>
			</a>
		</div>

	<?php } else { ?>

		<a
			href="<?php echo esc_url(add_query_arg(
				array(
					'wishlist_notice' => 'true',
					'add_to_wishlist' => $product_id
				),
				get_permalink(wc_get_page_id('myaccount'))
			)); ?>"
			rel="nofollow"
			class="<?php echo esc_attr(str_replace('add_to_wishlist', '', $link_classes)); ?>"
			title="<?php echo esc_attr($label); ?>"
		>
			<span class="add-to-wishlist__icon">
				<?php echo empty($icon) ? '<span class="icon-heart"></span>' : wp_kses_post($icon); ?>
			</span>
			<span class="add-to-wishlist__text"><?php echo esc_html($label); ?></span>
		</a>

	<?php } ?>

</div>
