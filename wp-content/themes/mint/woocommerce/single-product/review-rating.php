<?php
/**
 * The template to display the reviewers star rating in reviews
 *
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     3.1.0
 */

if (!defined('ABSPATH')) {
	exit;
}

global $comment;
$rating = intval(get_comment_meta($comment->comment_ID, 'rating', true));

if ($rating && get_option('woocommerce_enable_review_rating') === 'yes') { ?>

	<div class="theme-comment__rating">
		<div class="product-rating">
			<?php echo wc_get_rating_html($rating); ?>
		</div>
	</div>

<?php }
