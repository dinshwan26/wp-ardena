<?php
/**
 * Single Product title
 *
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     1.6.4
 */

if (!defined('ABSPATH')) {
	exit;
}

the_title('<h1 class="product-page__title">', '</h1>');
