<?php
/**
 * Single Product tabs
 *
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     2.4.0
 */

if (!defined('ABSPATH')) {
	exit;
}

/**
 * Filter tabs and allow third parties to add their own
 *
 * Each tab is an array containing title, callback and priority.
 * @see woocommerce_default_product_tabs()
 */
$tabs = apply_filters('woocommerce_product_tabs', array());

if (!empty($tabs)) { ?>

	<div class="product-page-tabs" id="product-page-tabs">

		<ul class="product-page-tabs__nav">

			<?php foreach ($tabs as $key => $tab) { ?>
				<li class="product-page-tabs__nav-item <?php echo esc_attr($key); ?>_tab">
					<a href="#product-page-tabs-<?php echo esc_attr($key); ?>" class="product-page-tabs__nav-link">
						<?php echo apply_filters('woocommerce_product_' . $key . '_tab_title', $tab['title'], $key); ?>
					</a>
				</li>
			<?php } ?>

			<?php if (MintOptions::get('single_product--extra_tab')) { ?>
				<li class="product-page-tabs__nav-item">
					<a href="#product-page-tabs-extra" class="product-page-tabs__nav-link">
						<?php echo wp_kses_post(MintOptions::get('single_product--extra_tab_title')); ?>
					</a>
				</li>
			<?php } ?>

		</ul>

		<div class="product-page-tabs__content-wrapper">

			<?php foreach ($tabs as $key => $tab) { ?>
				<div class="product-page-tabs__content" id="product-page-tabs-<?php echo esc_attr($key); ?>">
					<?php call_user_func($tab['callback'], $key, $tab); ?>
				</div>
			<?php } ?>

			<?php if (MintOptions::get('single_product--extra_tab')) { ?>
				<div class="product-page-tabs__content" id="product-page-tabs-extra">
					<?php echo wp_kses_post(MintOptions::get('single_product--extra_tab_content')); ?>
				</div>
			<?php } ?>

		</div>

	</div>

<?php } ?>
