<?php
/**
 * Single Product Rating
 *
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     3.1.0
 */

if (!defined('ABSPATH')) {
	exit;
}

global $product;

if (get_option('woocommerce_enable_review_rating') === 'no') {
	return;
}

$rating_count = $product->get_rating_count();
$review_count = $product->get_review_count();
$average      = $product->get_average_rating();

if ($rating_count > 0) { ?>

	<div class="product-page__rating">
		<div class="product-rating">

			<?php echo wc_get_rating_html($average, $rating_count); ?>

			<?php if (comments_open()) { ?>
				<a href="<?php the_permalink(); ?>#product-page-tabs-reviews" class="product-rating__link" rel="nofollow">
					<?php printf(
						_n('%s customer review', '%s customer reviews', $review_count, 'woocommerce'),
						'<span>' . esc_html($review_count) . '</span>'
					); ?>
				</a>
			<?php } ?>

		</div>
	</div>

<?php } ?>
