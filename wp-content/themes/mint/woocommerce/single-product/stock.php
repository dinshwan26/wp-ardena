<?php
/**
 * Single Product stock.
 *
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     3.0.0
 */

if (!defined('ABSPATH')) {
	exit;
}

?>

<div class="product-page__availability">
	<div class="product-label _<?php echo esc_attr($class); ?>">
		<?php echo wp_kses_post($availability); ?>
	</div>
</div>
