<?php
/**
 * The template to display the reviewers meta data (name, verified owner, review date)
 *
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     3.0.0
 */

if (!defined('ABSPATH')) {
	exit;
}

global $comment;
$verified = wc_review_is_from_verified_owner($comment->comment_ID);

if ('0' === $comment->comment_approved) { ?>

	<p><em><?php esc_attr_e('Your review is awaiting approval', 'woocommerce'); ?></em></p>

<?php } else { ?>

	<h6 itemprop="author" class="theme-comment__author"><?php comment_author(); ?></h6>

	<div class="theme-comment__meta">
		<time itemprop="datePublished" datetime="<?php echo get_comment_date('c'); ?>" class="theme-comment__date">
			<?php echo get_comment_date(wc_date_format()); ?>
		</time>

		<?php
		if ('yes' === get_option('woocommerce_review_rating_verification_label') && $verified) {
			echo '<em class="theme-comment__label verified">(' . esc_attr__('verified owner', 'woocommerce') . ')</em> ';
		}
		?>
	</div>

<?php }
