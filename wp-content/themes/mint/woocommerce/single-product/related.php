<?php
/**
 * Related Products
 *
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     3.0.0
 */

if (!defined('ABSPATH')) {
	exit;
}

if ($related_products) { ?>

	<div class="related-products">
		<div class="container">
			<h5 class="related-products__title"><?php esc_html_e('Related products', 'woocommerce'); ?></h5>

			<?php woocommerce_product_loop_start(); ?>

				<?php foreach ($related_products as $related_product) { ?>

					<?php
					$post_object = get_post($related_product->get_id());

					setup_postdata($GLOBALS['post'] =& $post_object);

					wc_get_template_part('content', 'product');
					?>

				<?php } ?>

			<?php woocommerce_product_loop_end(); ?>
		</div>
	</div>

<?php }

wp_reset_postdata();
