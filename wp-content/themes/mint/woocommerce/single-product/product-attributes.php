<?php
/**
 * Product attributes
 *
 * Used by list_attributes() in the products class.
 *
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     3.1.0
 */

if (!defined('ABSPATH')) {
	exit;
}

?>

<dl class="product-page-attributes shop_attributes">

	<?php if ($display_dimensions && $product->has_weight()) { ?>

		<dt class="product-page-attributes__title">
			<?php esc_html_e('Weight', 'woocommerce') ?>
		</dt>

		<dd class="product-page-attributes__desc product_weight">
			<?php echo esc_html(wc_format_weight($product->get_weight())); ?>
		</dd>

	<?php } ?>

	<?php if ($display_dimensions && $product->has_dimensions()) { ?>

		<dt class="product-page-attributes__title">
			<?php esc_html_e('Dimensions', 'woocommerce') ?>
		</dt>

		<dd class="product-page-attributes__desc product_dimensions">
			<?php echo esc_html(wc_format_dimensions($product->get_dimensions(false))); ?>
		</dd>

	<?php } ?>

	<?php foreach ($attributes as $attribute) { ?>

		<dt class="product-page-attributes__title">
			<?php echo wc_attribute_label($attribute->get_name()); ?>
		</dt>

		<dd class="product-page-attributes__desc">
			<?php
			$values = array();

			if ($attribute->is_taxonomy()) {
				$attribute_taxonomy = $attribute->get_taxonomy_object();
				$attribute_values = wc_get_product_terms(
					$product->get_id(),
					$attribute->get_name(),
					array('fields' => 'all')
				);

				foreach ($attribute_values as $attribute_value) {
					$value_name = esc_html($attribute_value->name);

					if ($attribute_taxonomy->attribute_public) {
						$values[] =
							'<a
								href="' . esc_url(get_term_link($attribute_value->term_id, $attribute->get_name())) . '"
								rel="tag"
							>'
								. $value_name .
							'</a>';
					} else {
						$values[] = $value_name;
					}
				}
			} else {
				$values = $attribute->get_options();

				foreach ($values as &$value) {
					$value = make_clickable(esc_html($value));
				}
			}

			echo apply_filters(
				'woocommerce_attribute',
				wpautop(wptexturize(implode(', ', $values))),
				$attribute,
				$values
			);
			?>
		</dd>

	<?php } ?>

</dl>
