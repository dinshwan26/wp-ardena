<?php
/**
 * Review Comments Template
 *
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     2.6.0
 */

if (!defined('ABSPATH')) {
	exit;
}

?>

<li
	id="li-comment-<?php comment_ID(); ?>"
	<?php comment_class('theme-comment _dark'); ?>
>
	<article id="comment-<?php comment_ID(); ?>" class="theme-comment__inner comment_container">

		<div class="theme-comment__content">
			<?php
			/**
			 * The woocommerce_review_before hook
			 *
			 * @hooked woocommerce_review_display_gravatar - 10
			 */
			do_action('woocommerce_review_before', $comment);

			/**
			 * The woocommerce_review_before_comment_meta hook.
			 */
			do_action('woocommerce_review_before_comment_meta', $comment);

			/**
			 * The woocommerce_review_meta hook.
			 *
			 * @hooked woocommerce_review_display_meta - 10
			 * @hooked WC_Structured_Data::generate_review_data() - 20
			 */
			do_action('woocommerce_review_meta', $comment);

			/**
			 * The woocommerce_review_before_comment_text hook.
			 */
			do_action('woocommerce_review_before_comment_text', $comment);
			?>

			<div class="theme-comment__desc">
				<?php
				/**
				 * The woocommerce_review_comment_text hook
				 *
				 * @hooked woocommerce_review_display_comment_text - 10
				 */
				do_action('woocommerce_review_comment_text', $comment);
				?>
			</div>

			<?php
			/**
			 * The woocommerce_review_after_comment_text hook
			 *
			 * @hooked woocommerce_review_display_rating - 25
			 */
			do_action('woocommerce_review_after_comment_text', $comment);
			?>
		</div>

	</article>
</li>
