<?php
/**
 * Single Product Meta
 *
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     3.0.0
 */

if (!defined('ABSPATH')) {
	exit;
}

global $product;

?>

<div class="product-page-meta">

	<?php do_action('woocommerce_product_meta_start'); ?>

	<?php if (wc_product_sku_enabled() && ($product->get_sku() || $product->is_type('variable'))) { ?>

		<span class="product-page-meta__item sku_wrapper">
			<span class="product-page-meta__item-title">
				<?php esc_html_e('SKU:', 'woocommerce'); ?>
			</span>
			<span class="product-page-meta__item-desc sku">
				<?php echo ($sku = $product->get_sku()) ? $sku : esc_html__('N/A', 'woocommerce'); ?>
			</span>
		</span>

	<?php } ?>

	<?php echo wc_get_product_category_list(
		$product->get_id(),
		', ',
		'<span class="product-page-meta__item posted_in">
			<span class="product-page-meta__item-title">'
				. _n('Category:', 'Categories:', count($product->get_category_ids()), 'woocommerce') .
			'</span>
			<span class="product-page-meta__item-desc">',
			'</span>
		</span>'
	); ?>

	<?php echo wc_get_product_tag_list(
		$product->get_id(),
		', ',
		'<span class="product-page-meta__item tagged_as">
			<span class="product-page-meta__item-title">'
				. _n('Tag:', 'Tags:', count($product->get_tag_ids()), 'woocommerce') .
			'</span>
			<span class="product-page-meta__item-desc">',
			'</span>
		</span>'
	); ?>

	<?php do_action('woocommerce_product_meta_end'); ?>

</div>
