<?php
/**
 * Variable product add to cart
 *
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     3.0.0
 */

if (!defined('ABSPATH')) {
	exit;
}

global $product;

$attribute_keys = array_keys($attributes);

do_action('woocommerce_before_add_to_cart_form');

?>

<form
	class="variations_form cart product-page__add-to-cart"
	method="post"
	enctype='multipart/form-data'
	data-product_id="<?php echo absint($product->get_id()); ?>"
	data-product_variations="<?php echo htmlspecialchars(wp_json_encode($available_variations)) ?>"
>

	<?php do_action('woocommerce_before_variations_form'); ?>

	<?php if (empty($available_variations) && false !== $available_variations) { ?>

		<div class="product-page__availability">
			<div class="product-label">
				<?php esc_html_e('This product is currently out of stock and unavailable.', 'woocommerce'); ?>
			</div>
		</div>

	<?php } else { ?>

		<div class="product-variations">
			<table class="product-variations__table variations" cellspacing="0">
				<?php foreach ($attributes as $attribute_name => $options) { ?>
					<tr>
						<td class="product-variations__td label">
							<label for="<?php echo sanitize_title($attribute_name); ?>">
								<?php echo wc_attribute_label($attribute_name); ?>:
							</label>
						</td>
						<td class="product-variations__td value">
							<?php
							$selected = isset($_REQUEST[ 'attribute_' . sanitize_title($attribute_name) ]) ?
								wc_clean(stripslashes(urldecode(
									$_REQUEST['attribute_' . sanitize_title($attribute_name)]
								))) :
								$product->get_variation_default_attribute($attribute_name);
							wc_dropdown_variation_attribute_options(array(
								'options' => $options,
								'attribute' => $attribute_name,
								'product' => $product,
								'selected' => $selected,
								'class' => 'product-variations__select',
								'show_option_none' => false,
							));
							?>
						</td>
					</tr>
				<?php } ?>
			</table>
		</div>

		<?php do_action('woocommerce_before_add_to_cart_button'); ?>

		<div class="single_variation_wrap">
			<?php
			/**
			 * woocommerce_before_single_variation Hook
			 */
			do_action('woocommerce_before_single_variation');

			/**
			 * woocommerce_single_variation Hook
			 * Used to output the cart button and placeholder for variation data
			 * @since 2.4.0
			 * @hooked woocommerce_single_variation - 10 Empty div for variation data
			 * @hooked woocommerce_single_variation_add_to_cart_button - 20 Qty and cart button
			 */
			do_action('woocommerce_single_variation');

			/**
			 * woocommerce_after_single_variation Hook
			 */
			do_action('woocommerce_after_single_variation');
			?>
		</div>

		<?php do_action('woocommerce_after_add_to_cart_button'); ?>

	<?php } ?>

	<?php do_action('woocommerce_after_variations_form'); ?>

</form>

<?php do_action('woocommerce_after_add_to_cart_form'); ?>
