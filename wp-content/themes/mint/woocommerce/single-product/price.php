<?php
/**
 * Single Product Price, including microdata for SEO
 *
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     3.0.0
 */

if (!defined('ABSPATH')) {
	exit;
}

global $product;

?>

<div class="product-page__price price">
	<?php echo wp_kses_post($product->get_price_html()); ?>
</div>
