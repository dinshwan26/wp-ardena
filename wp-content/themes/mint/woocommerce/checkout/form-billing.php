<?php
/**
 * Checkout billing information form
 *
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     3.0.9
 */

if (!defined('ABSPATH')) {
	exit;
}

/** @global WC_Checkout $checkout */

?>

<div class="checkout-billing">


	<?php if (wc_ship_to_billing_address_only() && WC()->cart->needs_shipping()) { ?>
		<h3 class="checkout-billing__title"><?php esc_html_e('Billing &amp; Shipping', 'woocommerce'); ?></h3>
	<?php } else { ?>
		<h3 class="checkout-billing__title"><?php esc_html_e('Billing details', 'woocommerce'); ?></h3>
	<?php } ?>


	<?php do_action('woocommerce_before_checkout_billing_form', $checkout); ?>

	<?php
	$fields = $checkout->get_checkout_fields('billing');

	foreach ($fields as $key => $field) {
		if (isset($field['country_field'], $fields[$field['country_field']])) {
			$field['country'] = $checkout->get_value($field['country_field']);
		}
		woocommerce_form_field($key, $field, $checkout->get_value($key));
	}
	?>

	<?php do_action('woocommerce_after_checkout_billing_form', $checkout); ?>


	<?php if (!is_user_logged_in() && $checkout->is_registration_enabled()) { ?>


		<?php if (!$checkout->is_registration_required()) { ?>

			<div class="checkout-create-account">
				<input
					class="input-checkbox"
					id="createaccount"
					type="checkbox"
					name="createaccount"
					value="1"
					<?php checked((true === $checkout->get_value('createaccount') || (true === apply_filters('woocommerce_create_account_default_checked', false))), true); ?>
				>
				<label for="createaccount" class="checkbox checkout-create-account__label">
					<?php esc_html_e('Create an account?', 'woocommerce'); ?>
				</label>

		<?php } ?>


			<?php do_action('woocommerce_before_checkout_registration_form', $checkout); ?>

			<?php if ($checkout->get_checkout_fields('account')) { ?>

				<div class="checkout-create-account__inner">
					<p><?php esc_html_e('Create an account by entering the information below. If you are a returning customer please login at the top of the page.', 'woocommerce'); ?></p>

					<?php foreach ($checkout->get_checkout_fields('account') as $key => $field) { ?>
						<?php woocommerce_form_field($key, $field, $checkout->get_value($key)); ?>
					<?php } ?>

					<div class="clear"></div>
				</div>

			<?php } ?>

			<?php do_action('woocommerce_after_checkout_registration_form', $checkout); ?>


		<?php if (!$checkout->is_registration_required()) { ?>

			</div>

		<?php } ?>


	<?php } ?>


</div>
