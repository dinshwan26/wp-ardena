<?php
/**
 * Thankyou page
 *
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     3.0.0
 */

if (!defined('ABSPATH')) {
	exit;
}

if ($order) { ?>

	<div class="wc-box">

		<?php if ($order->has_status('failed')) { ?>

			<p class="wc-box__title"><?php esc_attr_e('Unfortunately your order cannot be processed as the originating bank/merchant has declined your transaction. Please attempt your purchase again.', 'woocommerce'); ?></p>

			<p class="woocommerce-thankyou-order-failed-actions">
				<a href="<?php echo esc_url($order->get_checkout_payment_url()); ?>" class="button pay"><?php esc_attr_e('Pay', 'woocommerce') ?></a>
				<?php if (is_user_logged_in()) { ?>
					<a href="<?php echo esc_url(wc_get_page_permalink('myaccount')); ?>" class="button pay"><?php esc_attr_e('My Account', 'woocommerce'); ?></a>
				<?php } ?>
			</p>

		<?php } else { ?>

			<p class="wc-box__title"><?php echo apply_filters('woocommerce_thankyou_order_received_text', esc_attr__('Thank you. Your order has been received.', 'woocommerce'), $order); ?></p>

			<ul class="woocommerce-thankyou-order-details">
				<li class="order">
					<?php esc_attr_e('Order number:', 'woocommerce'); ?>
					<strong><?php echo wp_kses_post($order->get_order_number()); ?></strong>
				</li>
				<li class="date">
					<?php esc_attr_e('Date:', 'woocommerce'); ?>
					<strong><?php echo wc_format_datetime($order->get_date_created()); ?></strong>
				</li>
				<li class="total">
					<?php esc_attr_e('Total:', 'woocommerce'); ?>
					<strong><?php echo wp_kses_post($order->get_formatted_order_total()); ?></strong>
				</li>
				<?php if ($order->get_payment_method_title()) { ?>
				<li class="method">
					<?php esc_attr_e('Payment method:', 'woocommerce'); ?>
					<strong><?php echo wp_kses_post($order->get_payment_method_title()); ?></strong>
				</li>
				<?php } ?>
			</ul>
			<div class="clear"></div>

		<?php } ?>

		<?php do_action('woocommerce_thankyou_' . $order->get_payment_method(), $order->get_id()); ?>


	</div>

	<?php do_action('woocommerce_thankyou', $order->get_id()); ?>

<?php } else { ?>

	<p class="wc-lead"><?php echo apply_filters('woocommerce_thankyou_order_received_text', esc_attr__('Thank you. Your order has been received.', 'woocommerce'), null); ?></p>

<?php } ?>
