<?php
/**
 * Product quantity inputs
 *
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     2.5.0
 */

if (!defined('ABSPATH')) {
	exit;
}

if ($max_value && $min_value === $max_value) { ?>

	<input
		type="hidden"
		class="qty"
		name="<?php echo esc_attr($input_name); ?>"
		value="<?php echo esc_attr($min_value); ?>"
	>

<?php } else { ?>

	<input
		type="number"
		step="<?php echo esc_attr($step); ?>"
		min="<?php echo esc_attr($min_value); ?>"
		max="<?php echo esc_attr($max_value > 0 ? $max_value : ''); ?>"
		name="<?php echo esc_attr($input_name); ?>"
		value="<?php echo esc_attr($input_value); ?>"
		title="<?php echo esc_attr_x('Qty', 'Product quantity input tooltip', 'woocommerce') ?>"
		class="<?php echo (empty($class) ? '' : esc_attr($class)); ?>"
		pattern="<?php echo esc_attr($pattern); ?>"
		inputmode="<?php echo esc_attr($inputmode); ?>"
	>

<?php }
