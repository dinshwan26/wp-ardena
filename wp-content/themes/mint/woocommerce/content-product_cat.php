<?php
/**
 * The template for displaying product category thumbnails within loops
 *
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     2.6.1
 */

if (!defined('ABSPATH')) {
	exit;
}

if (MintHelpers::is_title_wrapper() && (is_shop() || is_product_taxonomy())) {

	?><div <?php wc_product_cat_class('products-category', $category); ?>>
		<?php
		/**
		 * woocommerce_before_subcategory hook.
		 */
		do_action('woocommerce_before_subcategory', $category);
		?>
		<a
			href="<?php echo get_term_link($category->slug, 'product_cat'); ?>"
			class="products-category__link"
		>
			<?php echo esc_html($category->name); ?>
		</a>
		<?php
		/**
		 * woocommerce_after_subcategory hook.
		 */
		do_action('woocommerce_after_subcategory', $category);
		?>
	</div><?php

} else {

	global $woocommerce_loop;

	// Store loop count we're currently on
	if (empty($woocommerce_loop['loop'])) {
		$woocommerce_loop['loop'] = 0;
	}

	// Store column count for displaying the grid
	if (empty($woocommerce_loop['columns'])) {
		$woocommerce_loop['columns'] = apply_filters('loop_shop_columns', 4);
	}

	$column_classes = MintHelpers::get_responsive_column_classes($woocommerce_loop['columns'], $woocommerce_loop['loop']);

	?>

	<div class="<?php echo esc_attr(join(' ', $column_classes)); ?>">
		<div class="animate-on-screen js-animate-on-screen">
			<div <?php wc_product_cat_class('products-category-card', $category); ?>>
				<?php
				/**
				 * woocommerce_before_subcategory hook.
				 */
				do_action('woocommerce_before_subcategory', $category);
				?>

				<a
					href="<?php echo get_term_link($category->slug, 'product_cat'); ?>"
					class="products-category-card__link"
					title="<?php echo esc_attr($category->name); ?>"
				>
					<?php
					/**
					 * woocommerce_before_subcategory_title hook
					 *
					 * @hooked woocommerce_subcategory_thumbnail - 10
					 */
					do_action('woocommerce_before_subcategory_title', $category);
					?>

					<div class="products-category-card__bottom">
						<h5 class="products-category-card__title"><?php echo esc_html($category->name); ?></h5>
					</div>

					<?php
					/**
					 * woocommerce_after_subcategory_title hook
					 */
					do_action('woocommerce_after_subcategory_title', $category);
					?>
				</a>

				<?php
				/**
				 * woocommerce_after_subcategory hook.
				 */
				do_action('woocommerce_after_subcategory', $category);
				?>
			</div>
		</div>
	</div>

<?php } ?>
