<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     3.0.0
 */

if (!defined('ABSPATH')) {
	exit;
}

?>

<div class="container">
	<?php
	/**
	 * woocommerce_before_single_product hook
	 *
	 * @hooked wc_print_notices - 10
	 */
	do_action('woocommerce_before_single_product');
	?>
</div>

<?php
if (post_password_required()) {
	?><div class="container"><?php
		echo get_the_password_form();
	?></div><?php
	return;
}
?>

<div
	<?php post_class('js-product product-page'); ?>
	id="product-<?php the_ID(); ?>"
>

	<?php MintShop::breadcrumb(); ?>

	<div class="product-page__inner-top">
		<div class="container">
			<div class="row">
				<div class="col-md-6 relative">
					<?php
					/**
					 * woocommerce_before_single_product_summary hook
					 *
					 * @hooked woocommerce_show_product_sale_flash - 10
					 * @hooked woocommerce_show_product_images - 20
					 */
					do_action('woocommerce_before_single_product_summary');
					?>
				</div>
				<div class="col-md-6">
					<div class="product-page__summary">
						<?php
						/**
						 * woocommerce_single_product_summary hook
						 *
						 * @hooked woocommerce_template_single_title - 5
						 * @hooked woocommerce_template_single_price - 10
						 * @hooked woocommerce_template_single_rating - 15
						 * @hooked woocommerce_template_single_excerpt - 20
						 * @hooked woocommerce_template_single_add_to_cart - 30
						 * @hooked woocommerce_template_single_sharing - 50
						 * @hooked WC_Structured_Data::generate_product_data() - 60
						 */
						do_action('woocommerce_single_product_summary');
						?>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="product-page__inner-bottom">
		<?php
		/**
		 * woocommerce_after_single_product_summary hook
		 *
		 * @hooked woocommerce_output_product_data_tabs - 10
		 * @hooked woocommerce_template_single_meta - 10
		 * @hooked woocommerce_upsell_display - 15
		 * @hooked woocommerce_output_related_products - 20
		 */
		do_action('woocommerce_after_single_product_summary');
		?>
	</div>

</div>

<?php do_action('woocommerce_after_single_product'); ?>
