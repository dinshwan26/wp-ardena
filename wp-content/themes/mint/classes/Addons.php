<?php if (!defined('ABSPATH')) die('-1');


class MintAddons extends MintTheme {


	private static $_instance = null;


	private function __construct() {
		// We safely integrate with VC with this hook
		if (!defined('WPB_VC_VERSION')) {
			return;
		}

		if (function_exists('visual_composer')) {
			add_action('init', array($this, 'disable_vc_meta'), 100);
		}

		add_action('vc_before_init', array($this, 'settings'));

		if (class_exists('woocommerce')) {
			remove_action( 'wp_enqueue_scripts', 'vc_woocommerce_add_to_cart_script' );
			add_action('wp_enqueue_scripts', array($this, 'vc_woocommerce_add_to_cart_script'));
		}
	}


	public static function init() {
		if(is_null(self::$_instance)) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}


	public function disable_vc_meta() {
		remove_action('wp_head', array(visual_composer(), 'addMetaData'));
	}


	public function settings() {
		vc_set_as_theme(true);
		vc_set_default_editor_post_types(array(
			'page',
			'product',
			'project',
		));
	}


	public function vc_woocommerce_add_to_cart_script() {
		wp_enqueue_script( 'vc_woocommerce-add-to-cart-js', vc_asset_url( 'js/vendors/woocommerce-add-to-cart.js' ), array( 'wc-add-to-cart' ), WPB_VC_VERSION, true );
	}


}

MintAddons::init();
