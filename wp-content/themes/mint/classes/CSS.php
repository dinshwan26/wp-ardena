<?php if (!defined('ABSPATH')) die('-1');


class MintCSS extends MintTheme {


	public static function font($option) {
		$font = self::get_option($option);
		if (!empty($font)) {
			if (!empty($font['font-family']) && !empty($font['font-backup'])) {
				echo 'font-family:' . $font['font-family'] . ', ' . $font['font-backup'] . ';';
			}
			elseif (!empty($font['font-family'])) {
				echo 'font-family:' . $font['font-family'] . ';';
			}
			if (!empty($font['font-weight'])) {
				echo 'font-weight:' . $font['font-weight'] . ';';
			}
			if (!empty($font['font-style'])) {
				echo 'font-style:' . $font['font-style'] . ';';
			}
			if (!empty($font['text-transform'])) {
				echo 'text-transform:' . $font['text-transform'] . ';';
			}
			if (!empty($font['font-size'])) {
				echo 'font-size:' . $font['font-size'] . ';';
			}
			if (!empty($font['line-height'])) {
				echo 'line-height:' . $font['line-height'] . ';';
			}
			if (!empty($font['letter-spacing'])) {
				echo 'letter-spacing:' . $font['letter-spacing'] . ';';
			}
			if (!empty($font['color'])) {
				echo 'color:' . $font['color'] . ';';
			}
		}
		$font_family = self::get_option($option . '__custom_family');
		if ($font_family) {
			echo 'font-family:' . $font_family . ';';
		}
	}


	public static function bg($option = false) {
		$bg = self::get_option($option);
		if (!empty($bg)) {
			if (!empty($bg['background-color'])) {
				echo 'background-color:' . $bg['background-color'] . ';';
			}
			if (!empty($bg['background-repeat'])) {
				echo 'background-repeat:' . $bg['background-repeat'] . ';';
			}
			if (!empty($bg['background-size'])) {
				echo 'background-size:' . $bg['background-size'] . ';';
			}
			if (!empty($bg['background-attachment'])) {
				echo 'background-attachment:' . $bg['background-attachment'] . ';';
			}
			if (!empty($bg['background-position'])) {
				echo 'background-position:' . $bg['background-position'] . ';';
			}
			if (!empty($bg['background-image'])) {
				echo 'background-image:url(' . $bg['background-image'] . ');';
			}
		}
	}


	public static function border($option, $top = true, $right = true, $bottom = true, $left = true) {
		$border = self::get_option($option);
		$border_color = self::get_option($option . '_color');
		if (!empty($border)) {
			echo 'border-top-width:' . (
				$top &&
				!empty($border['border-top']) &&
				$border['border-top'] !== 'px'
				? $border['border-top'] : 0
			) . ';';
			echo 'border-right-width:' . (
				$right &&
				!empty($border['border-right']) &&
				$border['border-right'] !== 'px'
				? $border['border-right'] : 0
			) . ';';
			echo 'border-bottom-width:' . (
				$bottom &&
				!empty($border['border-bottom']) &&
				$border['border-bottom'] !== 'px'
				? $border['border-bottom'] : 0
			) . ';';
			echo 'border-left-width:' . (
				$left &&
				!empty($border['border-left']) &&
				$border['border-left'] !== 'px'
				? $border['border-left'] : 0
			) . ';';
			echo 'border-style:' . (!empty($border['border-style']) ? $border['border-style'] : 'solid') . ';';
			if (!empty($border_color['rgba'])) {
				echo 'border-color:' . $border_color['rgba'] . ';';
			} else {
				echo 'border-color:' . ($border['border-color'] ? $border['border-color'] : 'inherit') . ';';
			}
		}
	}


	public static function padding($option) {
		$padding = self::get_option($option);
		if (
			isset($padding['padding-top']) &&
			$padding['padding-top'] !== 'px' &&
			$padding['padding-top'] !== ''
		) {
			echo 'padding-top:' . $padding['padding-top'] . ';';
		}
		if (
			isset($padding['padding-right']) &&
			$padding['padding-right'] !== 'px' &&
			$padding['padding-right'] !== ''
		) {
			echo 'padding-right:' . $padding['padding-right'] . ';';
		}
		if (
			isset($padding['padding-bottom']) &&
			$padding['padding-bottom'] !== 'px' &&
			$padding['padding-bottom'] !== ''
		) {
			echo 'padding-bottom:' . $padding['padding-bottom'] . ';';
		}
		if (
			isset($padding['padding-left']) &&
			$padding['padding-left'] !== 'px' &&
			$padding['padding-left'] !== ''
		) {
			echo 'padding-left:' . $padding['padding-left'] . ';';
		}
	}


	public static function color($option) {
		$color = self::get_option($option);
		if (!empty($color)) {
			echo 'color:' . $color . ';';
		}
	}


	public static function adjustBrightness($hex, $steps) {
		// Steps should be between -255 and 255. Negative = darker, positive = lighter
		$steps = max(-255, min(255, $steps));

		// Normalize into a six character long hex string
		$hex = str_replace('#', '', $hex);
		if (strlen($hex) == 3) {
			$hex = str_repeat(substr($hex,0,1), 2).str_repeat(substr($hex,1,1), 2).str_repeat(substr($hex,2,1), 2);
		}

		// Split into three parts: R, G and B
		$color_parts = str_split($hex, 2);
		$return = '#';

		foreach ($color_parts as $color) {
			$color   = hexdec($color); // Convert to decimal
			$color   = max(0,min(255,$color + $steps)); // Adjust color
			$return .= str_pad(dechex($color), 2, '0', STR_PAD_LEFT); // Make two char hex code
		}

		return $return;
	}


	public static function hex2rgb($hex) {
		$hex = str_replace("#", "", $hex);

		if(strlen($hex) == 3) {
			$r = hexdec(substr($hex,0,1).substr($hex,0,1));
			$g = hexdec(substr($hex,1,1).substr($hex,1,1));
			$b = hexdec(substr($hex,2,1).substr($hex,2,1));
		} else {
			$r = hexdec(substr($hex,0,2));
			$g = hexdec(substr($hex,2,2));
			$b = hexdec(substr($hex,4,2));
		}
		$rgb = array($r, $g, $b);
		return implode(",", $rgb);
	}


}

// MintCSS::init();
