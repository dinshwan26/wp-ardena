<?php if (!defined('ABSPATH')) die('-1');


class MintHelpers extends MintTheme {


	public static function get_image_sizes($size = '') {
		return parent::get_image_sizes($size);
	}


	public static function get_image_size_names() {
		return parent::get_image_size_names();
	}


	public static function get_social_icons() {
		return self::$social_icons;
	}


	public static function is_negative_header() {
		// DO NOT USE IN LOOP OR AFTER LOOP
		if (
			(get_post_type() == 'product' && (is_singular() || (!is_singular() && !self::is_title_wrapper()))) ||
			(function_exists('is_account_page') && is_account_page() && !is_user_logged_in()) ||
			(!is_home() && get_post_type() == 'post' && !self::is_title_wrapper()) ||
			!self::get_option('header--negative_height') ||
			is_404() ||
			!have_posts()
		) {
			return false;
		}
		return true;
	}


	public static function is_title_wrapper() {
		if (
			is_404() ||
			is_singular(array('product')) ||
			(function_exists('is_account_page') && is_account_page() && !is_user_logged_in())
		) {
			return false;
		}
		return self::get_option('title_wrapper');
	}


	public static function is_categorized_blog() {
		if (false === ($all_the_cool_cats = get_transient(self::$theme_prefix . '_categories'))) {
			// Create an array of all the categories that are attached to posts.
			$all_the_cool_cats = get_categories(array(
				'fields' => 'ids',
				'hide_empty' => 1,

				// We only need to know if there is more than one category.
				'number' => 2,
			));

			// Count the number of categories that are attached to the posts.
			$all_the_cool_cats = count($all_the_cool_cats);

			set_transient(self::$theme_prefix . '_categories', $all_the_cool_cats);
		}

		if (
			$all_the_cool_cats > 1 &&
			(
				(self::get_option('posts--categories') && !is_single()) ||
				(self::get_option('single_post--categories') && is_single())
			)
		) {
			// This blog has more than 1 category so is_categorized_blog() should return true.
			return true;
		} else {
			// This blog has only 1 category so is_categorized_blog() should return false.
			return false;
		}
	}


	public static function get_sidebar_location() {
		$sidebar_location = self::get_option('layout--sidebars');
		if (
			is_404() ||
			$sidebar_location == 'without' ||
			($sidebar_location != 'right' && !is_active_sidebar('sidebar_left')) ||
			($sidebar_location != 'left' && !is_active_sidebar('sidebar_right'))
		) {
			return false;
		}
		return $sidebar_location;
	}


	public static function get_responsive_column_classes($columns = 1, $loop = false) {

		$classes = array();

		if (self::get_option('layout--content_width') == 'expanded' && !MintShop::is_product()) {

			if ($columns == 1) {
				$classes[] = 'col-xs-12';
			} elseif ($columns == 2) {
				$classes[] = 'col-xs-12 col-md-6 col-xxl-4';
				if ($loop !== false && $loop % 2 == 0) { $classes[] = 'first-md first-lg first-xl'; }
				if ($loop !== false && $loop % 3 == 0) { $classes[] = 'first-xxl'; }
			} elseif ($columns == 3) {
				$classes[] = 'col-xs-12 col-sm-6 col-md-4 col-xxl-3';
				if ($loop !== false && $loop % 2 == 0) { $classes[] = 'first-sm'; }
				if ($loop !== false && $loop % 3 == 0) { $classes[] = 'first-md first-lg first-xl'; }
				if ($loop !== false && $loop % 4 == 0) { $classes[] = 'first-xxl'; }
			} elseif ($columns == 4) {
				$classes[] = 'col-xs-12 col-sm-4 col-md-3 col-xxl-2';
				if ($loop !== false && $loop % 3 == 0) { $classes[] = 'first-sm'; }
				if ($loop !== false && $loop % 4 == 0) { $classes[] = 'first-md first-lg first-xl'; }
				if ($loop !== false && $loop % 6 == 0) { $classes[] = 'first-xxl'; }
			} else {
				$classes[] = 'col-xs-6 col-sm-3 col-md-2';
				if ($loop !== false && $loop % 4 == 0) { $classes[] = 'first-sm'; }
				if ($loop !== false && $loop % 6 == 0) { $classes[] = 'first-md first-lg first-xl first-xxl'; }
			}

		} else {

			if ($columns == 1) {
				$classes[] = 'col-xs-12';
			} elseif ($columns == 2) {
				$classes[] = 'col-xs-12 col-md-6';
				if ($loop !== false && $loop % 2 == 0) { $classes[] = 'first-md first-lg first-xl first-xxl'; }
			} elseif ($columns == 3) {
				$classes[] = 'col-xs-12 col-sm-6 col-md-4';
				if ($loop !== false && $loop % 2 == 0) { $classes[] = 'first-sm'; }
				if ($loop !== false && $loop % 3 == 0) { $classes[] = 'first-md first-lg first-xl first-xxl'; }
			} elseif ($columns == 4) {
				$classes[] = 'col-xs-12 col-sm-4 col-md-3';
				if ($loop !== false && $loop % 3 == 0) { $classes[] = 'first-sm'; }
				if ($loop !== false && $loop % 4 == 0) { $classes[] = 'first-md first-lg first-xl first-xxl'; }
			} else {
				$classes[] = 'col-xs-6 col-sm-3 col-md-2';
				if ($loop !== false && $loop % 4 == 0) { $classes[] = 'first-sm'; }
				if ($loop !== false && $loop % 6 == 0) { $classes[] = 'first-md first-lg first-xl first-xxl'; }
			}

		}

		return $classes;

	}


	public static function get_dynamic_area($id) {
		if($id) {
			global $post;
			$old_post = $post;
			$post = get_post($id);
			if ($post) {
				echo '<div class="container">';
				echo apply_filters('the_content', $post->post_content);
				echo '</div>';

				// Display custom row CSS by VC
				$vc_styles = get_post_meta($id , '_wpb_shortcodes_custom_css', true);
				if($vc_styles !== '') {
					echo '<style>' . $vc_styles .'</style>';
				}
			}
			$post = $old_post;
		}
	}


	public static function comment($comment, $args, $depth) {
		$GLOBALS['comment'] = $comment;

		if ('pingback' == $comment->comment_type || 'trackback' == $comment->comment_type) { ?>

			<li <?php comment_class('theme-comment'); ?>>
				<div id="comment-<?php comment_ID(); ?>" class="theme-comment__inner">
					<?php esc_html_e('Pingback:', 'mint'); ?>
					<?php comment_author_link(); ?>
					<?php edit_comment_link(esc_html__('Edit', 'mint'), '<span class="edit-link">', '</span>'); ?>
				</div>

		<?php } else { ?>

			<li
				id="comment-<?php comment_ID(); ?>"
				<?php comment_class(empty($args['has_children']) ? 'theme-comment' : 'theme-comment _parent'); ?>
			>
				<article id="div-comment-<?php comment_ID(); ?>" class="theme-comment__inner">

					<?php
					$avatar = get_avatar(
						$comment,
						140,
						'',
						false,
						array('class' => 'theme-comment__avatar')
					);
					if ($avatar) {
					?>
						<div class="theme-comment__aside">
							<div class="theme-comment__avatar-wrapper">
								<?php echo ($avatar); ?>
							</div>
						</div>
					<?php } ?>

					<div class="theme-comment__content">
						<?php if ('0' === $comment->comment_approved) { ?>

							<p><em><?php esc_html_e('Your comment is awaiting moderation.', 'mint'); ?></em></p>

						<?php } else { ?>

							<h6 class="theme-comment__author"><?php echo get_comment_author_link(); ?></h6>

							<div class="theme-comment__meta">
								<a href="<?php echo esc_url(get_comment_link($comment->comment_ID)); ?>">
									<time datetime="<?php comment_time('c'); ?>" class="theme-comment__date">
										<?php printf(_x('%1$s at %2$s', '1: date, 2: time', 'mint'), get_comment_date(), get_comment_time()); ?>
									</time>
								</a>

								<?php edit_comment_link(esc_html__('Edit', 'mint')); ?>
							</div>

						<?php } ?>

						<div class="theme-comment__desc">
							<?php comment_text(); ?>
						</div>

						<?php
						comment_reply_link(array_merge($args, array(
							'add_below'  => 'div-comment',
							'depth'      => $depth,
							'max_depth'  => $args['max_depth'],
							'before'     => '<div class="theme-comment__reply">',
							'after'      => '</div>',
							'reply_text' => esc_html__('Leave reply', 'mint'),
						)));
						?>
					</div>

				</article>

		<?php
		}
	}


	public static function post_author() {
		?>
		<a href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>" class="post-author">
			<?php
			$avatar = get_avatar(
				get_the_author_meta('email'),
				100,
				'',
				false,
				array('class' => 'post-author__img')
			);
			if ($avatar) {
			?>
				<span class="post-author__img-wrapper">
					<?php echo ($avatar); ?>
				</span>
			<?php } ?>
			<span class="post-author__title-wrapper">
				<span class="post-author__subtitle"><?php esc_html_e('Posted by', 'mint'); ?></span>
				<span class="post-author__title"><?php the_author(); ?></span>
			</span>
		</a>
		<?php
	}


	public static function post_navigation() {
		$prev = get_previous_post_link(
			'<li class="post-nav__list-item _prev">%link</li>',
			'<span class="post-nav__icon _prev"><span class="arrow-left"></span></span>
			<span class="post-nav__subtitle _prev">
			' . esc_html__('Previous reading', 'mint') . '
			</span>
			<span class="post-nav__title _prev">%title</span>'
		);

		$next = get_next_post_link(
			'<li class="post-nav__list-item _next">%link</li>',
			'<span class="post-nav__icon _next"><span class="arrow-right"></span></span>
			<span class="post-nav__subtitle _next">
				' . esc_html__('Next reading', 'mint') . '
			</span>
			<span class="post-nav__title _next">%title</span>'
		);

		if (!$next && !$prev) {
			return;
		}
		?>
		<nav class="navigation post-nav">
			<ul class="post-nav__list">
				<?php echo wp_kses_post($prev); ?>
				<?php echo wp_kses_post($next); ?>
			</ul>
		</nav>
		<?php
	}


	public static function posts_navigation($post_type = 'posts') {
		if ($GLOBALS['wp_query']->max_num_pages < 2 || !self::get_option($post_type . '--nav')) {
			return;
		}
		?>
		<nav class="navigation posts-nav">
			<ul class="posts-nav__list">

				<?php if (get_next_posts_link()) { ?>
					<li class="posts-nav__list-item _prev">
						<span class="posts-nav__icon _prev"><span class="arrow-left"></span></span>
						<?php next_posts_link(esc_html__('Older posts', 'mint')); ?>
					</li>
				<?php } ?>

				<?php if (get_previous_posts_link()) { ?>
					<li class="posts-nav__list-item _next">
						<span class="posts-nav__icon _next"><span class="arrow-right"></span></span>
						<?php previous_posts_link(esc_html__('Newer posts', 'mint')); ?>
					</li>
				<?php } ?>

			</ul>
		</nav>
		<?php
	}


}

// MintHelpers::init();
