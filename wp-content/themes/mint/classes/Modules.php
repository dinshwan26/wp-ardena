<?php if (!defined('ABSPATH')) die('-1');


class MintModules extends MintTheme {

	private static $_header_mods_counter = null;

	private static function get_header_mods_count() {
		if(is_null(self::$_header_mods_counter)) {
			self::$_header_mods_counter = 0;
			if (self::get_option('header--text')) self::$_header_mods_counter++;
			if (self::get_option('header--social')) self::$_header_mods_counter++;
			if (self::get_option('header--wpml_currency')) self::$_header_mods_counter++;
			if (self::get_option('header--wpml_lang')) self::$_header_mods_counter++;
			if (self::$_header_mods_counter && self::get_option('header--wishlist')) self::$_header_mods_counter++;
		}

		return self::$_header_mods_counter;
	}


	public static function text($section = 'header', $large_text = true, $hide_on_screen_xs = true) {
		if (self::get_option($section . '--text')) { ?>

			<div class="
				inline-module
				<?php
				if ($hide_on_screen_xs) { ?>hidden-xs<?php }
				elseif ($section == 'header') { ?>_block<?php }
				?>
			">
				<div class="
					inline-module__text
					<?php if (!$large_text) { ?>small<?php } ?>
				">
					<?php echo wp_kses_post(self::get_option($section . '--text_content')); ?>
				</div>
			</div>

			<?php if ($hide_on_screen_xs) { ?><span class="inline-module _separator hidden-xs"></span><?php }

		}
	}


	public static function icon_social($section = 'header', $large_icon = true, $hide_on_screen_xs = true) {
		if (self::get_option($section . '--social')) {
			$social_links = self::get_option($section . '--social_links');
			foreach ($social_links as $key => $social_link) {
				if ($social_link) { ?>

					<div class="
						inline-module
						<?php if ($hide_on_screen_xs) { ?>hidden-xs<?php } ?>
					">
						<a
							href="<?php echo esc_url(self::get_option('social--' . $key)); ?>"
							target="_blank"
							class="inline-module__link"
						>
							<i class="
								fa
								<?php if ($large_icon) echo 'fa-lg'; ?>
								fa-<?php echo esc_attr($key); ?>
							"></i>
						</a>
					</div>

				<?php }
			}

			if ($hide_on_screen_xs) { ?><span class="inline-module _separator hidden-xs"></span><?php }

		}
	}


	public static function icon_lwa($section = 'header', $large_icon = true) {
		if (self::get_option($section . '--login_ajax') && function_exists('login_with_ajax')) {

			if (class_exists('woocommerce')) {
				$profile_link = get_permalink( get_option('woocommerce_myaccount_page_id') );
			} else if (function_exists('bp_loggedin_user_link')) {
				$profile_link = bp_loggedin_user_link();
			} else {
				$profile_link = trailingslashit(get_admin_url()) . 'profile.php';
			}
			?>

			<div class="inline-module">
				<a
					href="<?php echo esc_url($profile_link); ?>"
					class="inline-module__link js-toggle-next"
				>
					<span class="icon-head <?php if ($large_icon) echo 'xbig'; ?>"></span>
				</a>
				<div class="popup _small js-popup">
					<?php login_with_ajax( array( 'profile_link' => 1 ) ); ?>
				</div>
			</div>

			<?php
		}
	}


	public static function icon_wishlist($section = 'header', $large_icon = true, $hide_on_screen_xs = true) {
		if (self::get_option($section . '--wishlist') && function_exists('YITH_WCWL')) { ?>

			<div class="
				inline-module
				<?php
				if (
					!($section === 'header' && !self::get_header_mods_count()) &&
					$hide_on_screen_xs
				) {
					?>hidden-xs<?php
				}
				?>
			">
				<a
					href="<?php echo esc_url(YITH_WCWL()->get_wishlist_url()); ?>"
					class="inline-module__link"
				>
					<span class="icon-heart <?php if ($large_icon) echo 'xbig'; ?>"></span>
				</a>
			</div>

		<?php }
	}


	public static function icon_minicart($section = 'header', $large_icon = true) {
		if (
			self::get_option($section . '--woo_cart') &&
			class_exists('woocommerce') &&
			!MintShop::is_cart() &&
			!MintShop::is_checkout()
		) {
			$count = WC()->cart->cart_contents_count;
			?>

			<div class="inline-module">
				<a
					href="<?php echo esc_url(WC()->cart->get_cart_url()); ?>"
					title="<?php esc_html_e('View your shopping cart', 'mint'); ?>"
					class="inline-module__link js-toggle-next"
				>
					<span class="icon-bag <?php if ($large_icon) echo 'xbig'; ?>"></span>
					<span class="
						inline-module__label
						<?php if (!$count) echo 'hide'; ?>
						js-minicart-counter
					"><?php echo absint($count); ?></span>
				</a>
				<div class="popup _small js-popup">
					<div class="widget_shopping_cart_content"></div>
				</div>
			</div>

			<?php
		}
	}


	public static function icon_search($section = 'header', $large_icon = true) {
		if (self::get_option($section . '--search')) { ?>

			<div class="inline-module">
				<a href="#" class="js-toggle-next inline-module__link">
					<span class="icon-search <?php if ($large_icon) echo 'xbig'; ?>"></span>
				</a>
				<div class="popup _fullscreen _accent js-popup">
					<a href="#" class="popup__close _fullscreen js-hide-popups">
						<span class="hamburger _close _light pull-right"></span>
					</a>
					<span class="vertical-helper"></span><div class="popup__middle">
						<?php get_search_form(); ?>
					</div>
				</div>
			</div>

		<?php }
	}


	public static function icon_currency($section = 'header', $hide_on_screen_xs = true) {
		if (self::get_option($section . '--wpml_currency')) {

			if ($hide_on_screen_xs) { ?><span class="inline-module _separator hidden-xs"></span><?php } ?>

			<div class="
				inline-module
				<?php if ($hide_on_screen_xs) { ?>hidden-xs<?php } ?>
			">
				<?php do_action(
					'currency_switcher',
					array(
						'format' => '%symbol%',
						'switcher_style' => 'list',
						'orientation' => 'horizontal',
					)
				); ?>
			</div>

			<?php
		}
	}


	public static function icon_language_flags($section = 'header', $hide_on_screen_xs = true) {
		if (self::get_option($section . '--wpml_lang') && function_exists('icl_get_languages')) {
			$languages = icl_get_languages('skip_missing=0&orderby=code');
			if (!empty($languages)) {

				if ($hide_on_screen_xs) { ?>
					<span class="inline-module _separator hidden-xs"></span>
				<?php }

				foreach ($languages as $language) { ?>

					<div class="
						inline-module
						<?php if ($hide_on_screen_xs) { ?>hidden-xs<?php } ?>
					">
						<?php if (!$language['active']) echo '<a href="' . esc_url($language['url']) . '">'; ?>
						<img
							src="<?php echo esc_url($language['country_flag_url']); ?>"
							alt="<?php echo esc_attr($language['language_code']); ?>"
							width="18"
							height="12"
						>
						<?php if (!$language['active']) echo '</a>';?>
					</div>

				<?php }
			}
		}
	}


	public static function popup_menu() {
		if (has_nav_menu('popup')) {
			wp_nav_menu( array(
				'theme_location' => 'popup',
				'menu' => self::get_option('menu--popup'),
				'menu_class' =>
					'popup-menu js-popup-menu js-scroll-nav' .
					' _' . self::get_option('popup_menu--color_scheme') .
					' _' . self::get_option('popup_menu_styles--align'),
				'container' => 'nav',
			) );
		} else {
			esc_html_e('Assign a menu at Appearance > Menus', 'mint');
		}
	}


	public static function icon_mobile_menu($hide_on_screen_lg = true) {
		?>

		<span class="
			inline-module
			_separator
			hidden-xs
			<?php if ($hide_on_screen_lg) { ?>hidden-lg<?php } ?>
		"></span>

		<div class="
			inline-module
			<?php if ($hide_on_screen_lg) { ?>hidden-lg<?php } ?>
		">
			<a href="#" class="js-toggle-next inline-module__link">
				<span class="icon-menu xbig"></span>
			</a>
			<div class="popup _mobile-menu _fullscreen js-popup">
				<a href="#" class="popup__close js-hide-popups">
					<span class="hamburger _close"></span>
				</a>
				<span class="vertical-helper"></span><div class="popup__middle">
					<?php self::popup_menu(); ?>
				</div>
			</div>
		</div>

		<?php
	}


	public static function icon_popup_menu() {
		?>

		<div class="inline-module">
			<a href="#" class="js-toggle-next inline-module__link _popup">
				<span class="js-toggle-next-icon hamburger"></span>
			</a>
			<div class="popup _popup-menu _fullscreen _double js-popup">
				<div class="popup__half _left">
					<span class="vertical-helper"></span><div class="popup__middle">
						<?php self::popup_menu(); ?>
					</div>
				</div>

				<div class="popup__half _right">
					<span class="vertical-helper"></span><div class="popup__middle _text_light text-left">
						<?php if (is_active_sidebar('popup_menu')) { ?>
							<div class="row">
								<?php dynamic_sidebar('popup_menu'); ?>
							</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>

		<?php
	}


	public static function icon_mobile() {
		if (self::get_header_mods_count()) { ?>

			<div class="inline-module hidden-sm hidden-md hidden-lg">
				<a href="#" class="inline-module__link js-toggle-next">
					<span class="icon-plus xbig"></span>
				</a>
				<div class="popup _small js-popup">
					<div class="inline-modules">
						<?php self::text('header', true, false); ?>
						<?php self::icon_social('header', true, false); ?>
						<?php self::icon_wishlist('header', true, false); ?>
						<?php self::icon_currency('header', false); ?>
						<?php self::icon_language_flags('header', false); ?>
					</div>
				</div>
			</div>

		<?php }
	}


	public static function share($classes = '') {
		global $post;

		$image = '';
		if (has_post_thumbnail($post->ID)) {
			$image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'shop_single');
			$image = urlencode($image[0]);
		}

		$link = urlencode(get_permalink());
		$title = urlencode(get_the_title());
		?>

			<ul class="share <?php echo esc_attr($classes); ?>">
				<li class="share__icon">
					<a
						class="share__icon-link _facebook"
						target="_blank"
						href="<?php
							echo esc_url(
								'https://www.facebook.com/sharer.php?s=100' .
								'&p[url]=' . $link
							);
						?>"
						title="<?php esc_attr_e('Facebook', 'mint'); ?>"
					>
						<i class="fa fa-facebook"></i>
						<span class="share__icon-text"><?php esc_html_e('Share', 'mint'); ?></span>
					</a>
				</li>
				<li class="share__icon">
					<a
						class="share__icon-link _twitter"
						target="_blank"
						href="<?php
							echo esc_url(
								'https://twitter.com/share?' .
								'url=' . $link .
								'&text=' . $title
							);
						?>"
						title="<?php esc_attr_e('Twitter', 'mint'); ?>"
					>
						<i class="fa fa-twitter"></i>
						<span class="share__icon-text"><?php esc_html_e('Tweet', 'mint'); ?></span>
					</a>
				</li>
				<li class="share__icon">
					<a
						class="share__icon-link _pinterest"
						target="_blank"
						href="<?php
							echo esc_url(
								'http://pinterest.com/pin/create/button/?' .
								'url=' . $link .
								'&description=' . $title .
								'&media=' . $image
							);
						?>"
						title="<?php esc_attr_e('Pinterest', 'mint'); ?>"
					>
						<i class="fa fa-pinterest"></i>
						<span class="share__icon-text"><?php esc_html_e('Pin It', 'mint'); ?></span>
					</a>
				</li>
				<li class="share__icon">
					<a
						class="share__icon-link _google-plus"
						target="_blank"
						href="<?php
							echo esc_url(
								'https://plus.google.com/share?' .
								'url=' . $link .
								'&title=' . $title
							);
						?>"
						title="<?php esc_attr_e('Google+', 'mint'); ?>"
					>
						<i class="fa fa-google-plus"></i>
						<span class="share__icon-text"><?php esc_html_e('Share', 'mint'); ?></span>
					</a>
				</li>
				<li class="share__icon">
					<a
						class="share__icon-link _envelope"
						href="<?php
							echo esc_url(
								'mailto:?subject=I wanted you to see this site' .
								'&body=' . $link .
								'&title=' . $title
							);
						?>"
						title="<?php esc_attr_e('Email', 'mint'); ?>"
					>
						<i class="fa fa-envelope"></i>
						<span class="share__icon-text"><?php esc_html_e('Send', 'mint'); ?></span>
					</a>
				</li>
			</ul>

		<?php
	}


	public static function share_tooltip() {
		?>

		<div class="tooltip">
			<a class="tooltip__title">
				<span class="tooltip__title-icon">
					<span class="icon-share"></span>
				</span>
				<span class="tooltip__title-text"><?php esc_html_e('Share', 'mint'); ?></span>
			</a>
			<div class="tooltip__content">
				<?php self::share('_big-icons'); ?>
			</div>
		</div>

		<?php
	}


	public static function share_buttons() {

		self::share('brand-colors _buttons');

	}


}

// MintModules::init();
