<?php
Redux::setSection( $theme_options, array(
	'id' => 'sec_single_project',
	'title' => esc_html__('Single project', 'mint'),
	'desc' => esc_html__('Change single project templates.', 'mint'),
	'icon' => 'el el-idea-alt',
	'fields' => array(
		array(
			'id' => 'single_project--layout--sidebars',
			'type' => 'image_select',
			'title' => esc_html__('Sidebars in single project', 'mint'),
			'subtitle' => esc_html__('Select the layout to be used in  single project.', 'mint'),
			'options' => array(
				'' => array(
					'alt' => 'default',
					'img' => self::$theme_uri . '/assets/images/sidebars/def.png'
				),
				'without' => array(
					'alt' => 'without sidebar',
					'img' => self::$theme_uri . '/assets/images/sidebars/1c.png'
				),
				'left' => array(
					'alt' => 'sidebar at left',
					'img' => self::$theme_uri . '/assets/images/sidebars/2cl.png'
				),
				'right' => array(
					'alt' => 'sidebar at right',
					'img' => self::$theme_uri . '/assets/images/sidebars/2cr.png'
				),
				'both' => array(
					'alt' => 'both sidebars',
					'img' => self::$theme_uri . '/assets/images/sidebars/3cm.png'
				),
				'both_left' => array(
					'alt' => 'both sidebars at left',
					'img' => self::$theme_uri . '/assets/images/sidebars/3cl.png'
				),
				'both_right' => array(
					'alt' => 'both sidebars at right',
					'img' => self::$theme_uri . '/assets/images/sidebars/3cr.png'
				)
			),
			'default' => '',
		),

		array(
			'id' => 'single_project--layout--content_width',
			'type' => 'select',
			'title' => esc_html__('Content container type', 'mint'),
			'subtitle' => esc_html__('Define container configuration to be used, it can be normal, expanded or compact.', 'mint'),
			'options' => array(
				'expanded' => esc_html__('Expanded', 'mint'),
				'normal' => esc_html__('Normal', 'mint'),
				'compact' => esc_html__('Compact', 'mint'),
			),
			'default' => '',
		),

		array(
			'id' => 'single_project--nav',
			'type' => 'switch',
			'title' => esc_html__('Single project navigation', 'mint'),
			'subtitle' => esc_html__('If on, navigation will be displayed below content.', 'mint'),
			'default' => 1,
		),
	)
) );
