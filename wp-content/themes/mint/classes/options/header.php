<?php
Redux::setSection( $theme_options, array(
	'id' => 'main_sec_header',
	'title' => esc_html__('Header', 'mint'),
	'icon' => 'el el-caret-up',
) );

Redux::setSection( $theme_options, array(
	'id' => 'sec_header',
	'title' => esc_html__('Header settings', 'mint'),
	'subsection' => true,
	'fields' => array(
		array(
			'id' => 'header',
			'type' => 'switch',
			'title' => esc_html__('Enable header?', 'mint'),
			'subtitle' => esc_html__('If on, this layout part will be displayed in website.', 'mint'),
			'default' => 1,
		),

		array(
			'id' => 'header--fixed',
			'type' => 'switch',
			'title' => esc_html__('Fixed header', 'mint'),
			'subtitle' => esc_html__('If on, the header will be fixed at screen top on page scroll.', 'mint'),
			'default' => 1,
		),

		array(
			'id' => 'header--negative_height',
			'type' => 'switch',
			'title' => esc_html__('Negative height', 'mint'),
			'subtitle' => esc_html__('If on, header and top header will not have height and background and title wrapper or content will be showed behind it.', 'mint'),
			'default' => 0,
		),

		array(
			'id' => 'header--boxed',
			'type' => 'switch',
			'title' => esc_html__('Boxed', 'mint'),
			'default' => 0,
		),

		array(
			'id' => 'header--template',
			'type' => 'image_select',
			'title' => esc_html__('Header layout', 'mint'),
			'options' => array(
				'1' => array(
					'alt' => 'Logo - Menu and Modules',
					'img' => self::$theme_uri . '/assets/images/header-layouts/1.png'
				),
				'2' => array(
					'alt' => 'Additional Menu - Logo - Menu and Modules',
					'img' => self::$theme_uri . '/assets/images/header-layouts/2.png'
				),
				'3' => array(
					'alt' => 'Logo / Menu and Modules',
					'img' => self::$theme_uri . '/assets/images/header-layouts/3.png'
				),
				'4' => array(
					'alt' => 'Logo - Menu',
					'img' => self::$theme_uri . '/assets/images/header-layouts/4.png'
				),
				'5' => array(
					'alt' => 'Additional Menu - Logo - Menu',
					'img' => self::$theme_uri . '/assets/images/header-layouts/5.png'
				),
				'6' => array(
					'alt' => 'Logo / Menu',
					'img' => self::$theme_uri . '/assets/images/header-layouts/6.png'
				),
				'7' => array(
					'alt' => 'Logo - Modules and Popup Menu',
					'img' => self::$theme_uri . '/assets/images/header-layouts/7.png'
				),
				'8' => array(
					'alt' => 'Logo - Popup Menu',
					'img' => self::$theme_uri . '/assets/images/header-layouts/8.png'
				),
			),
			'default' => '1',
		),

		array(
			'id' => 'header--logo_dark',
			'type' => 'media',
			'title' => esc_html__('Logo (dark)', 'mint'),
			'subtitle' => esc_html__('Upload a dark version of logo used in light backgrounds in the header.', 'mint'),
			'url' => true,
		),

			array(
				'id' => 'header--logo_dark_retina',
				'type' => 'media',
				'title' => esc_html__('Logo retina (dark)', 'mint'),
				'subtitle' => esc_html__('Optional retina version displayed in devices with retina display (high resolution display).', 'mint'),
				'desc' => esc_html__('The same logo image but with twice dimensions, e.g. your logo is 100x100, then your retina logo must be 200x200.', 'mint'),
				'url' => true,
				'required' => array('header--logo_dark', '!=', null),
			),

		array(
			'id' => 'header--logo_light',
			'type' => 'media',
			'title' => esc_html__('Logo (light)', 'mint'),
			'subtitle' => esc_html__('Upload a light version of logo used in dark backgrounds in the header.', 'mint'),
			'url' => true,
		),

			array(
				'id' => 'header--logo_light_retina',
				'type' => 'media',
				'title' => esc_html__('Logo retina (light)', 'mint'),
				'subtitle' => esc_html__('Optional retina version displayed in devices with retina display (high resolution display).', 'mint'),
				'desc' => esc_html__('The same logo image but with twice dimensions, e.g. your logo is 100x100, then your retina logo must be 200x200.', 'mint'),
				'url' => true,
				'required' => array('header--logo_light', '!=', null),
			),

		array(
			'id' => 'header--color_scheme',
			'type' => 'select',
			'title' => esc_html__('Color scheme for header', 'mint'),
			'options' => array(
				'light' => esc_html__('Light text', 'mint'),
				'dark' => esc_html__('Dark text', 'mint'),
			),
			'default' => 'dark',
			'validate' => 'not_empty',
		),

		array(
			'id' => 'header--separator',
			'type' => 'switch',
			'title' => esc_html__('Separator between menu and modules', 'mint'),
			'default' => 0,
		),

		array(
			'id' => 'header--mobile_menu',
			'type' => 'switch',
			'title' => esc_html__('Mobile menu', 'mint'),
			'subtitle' => esc_html__('If on, a mobile menu link will be displayed on mobile devices (smaller than 768px) and main menu will be hided.', 'mint'),
			'default' => 1,
		),

		array(
			'id' => 'header--text',
			'type' => 'switch',
			'title' => esc_html__('Text module', 'mint'),
			'subtitle' => esc_html__('If on, a rich text module will be displayed.', 'mint'),
			'default' => 0,
		),

			array(
				'id' => 'header--text_content',
				'type' => 'editor',
				'title' => esc_html__('Text module content', 'mint'),
				'subtitle' => esc_html__('Place any text or shortcode to be displayed in header. Use &lt;i class="text-separator"&gt;&lt;/i&gt; to add a separator in the text. Use &lt;i class="fa fa-home"&gt;&lt;/i&gt; to display Font Awesome icons.', 'mint'),
				'default' => '9854-888-021, New York, NY',
				'required' => array('header--text', '=', 1),
			),

		array(
			'id' => 'header--login_ajax',
			'type' => 'switch',
			'title' => esc_html__('Login With Ajax module', 'mint'),
			'subtitle' => esc_html__('If on, a Login With Ajax module will be displayed. Requires Login With Ajax plugin activated.', 'mint'),
			'default' => 1,
		),

		array(
			'id' => 'header--wishlist',
			'type' => 'switch',
			'title' => esc_html__('Wishlist module', 'mint'),
			'subtitle' => esc_html__('If on, a wishlist link will be displayed. Requires YITH Woocommerce Wishlist plugin activated.', 'mint'),
			'default' => 1,
		),

		array(
			'id' => 'header--woo_cart',
			'type' => 'switch',
			'title' => esc_html__('Minicart', 'mint'),
			'subtitle' => esc_html__('If on, a WooCommerce minicart will be displayed. Requires WooCommerce plugin activated.', 'mint'),
			'default' => 0,
		),

		array(
			'id' => 'header--search',
			'type' => 'switch',
			'title' => esc_html__('Search module', 'mint'),
			'subtitle' => esc_html__('If on, a search module will be displayed.', 'mint'),
			'default' => 1,
		),

		array(
			'id' => 'header--social',
			'type' => 'switch',
			'title' => esc_html__('Social module', 'mint'),
			'subtitle' => esc_html__('If on, a social icon module will be displayed.', 'mint'),
			'default' => 0,
		),

			array(
				'id' => 'header--social_links',
				'type' => 'sortable',
				'mode' => 'checkbox',
				'title' => esc_html__('Social links', 'mint'),
				'subtitle' => esc_html__('Enable social links to be displayed.', 'mint'),
				'options' => self::$social_icons,
				'default' => self::$social_icons_default,
				'required' => array('header--social', '=', 1),
			),

		array(
			'id' => 'header--wpml_mods_section',
			'type' => 'section',
			'title' => esc_html__('WPML modules', 'mint'),
			'indent' => true,
		),

			array(
				'id' => 'header--wpml_lang',
				'type' => 'switch',
				'title' => esc_html__('WPML language flags', 'mint'),
				'subtitle' => esc_html__('If on, the avaliable languages flags will be displayed. Only works with WPML activated.', 'mint'),
				'default' => 0,
			),

			array(
				'id' => 'header--wpml_currency',
				'type' => 'switch',
				'title' => esc_html__('WPML shop currencies', 'mint'),
				'subtitle' => esc_html__('If on, the avaliable currencies flags will be displayed. Only works with WPML + WooCommerce Multilingual activated.', 'mint'),
				'default' => 0,
			),

		array(
			'id' => 'header--wpml_mods_section__end',
			'type' => 'section',
			'indent' => false,
		),
	)
) );

Redux::setSection( $theme_options, array(
	'id' => 'sec_header_styles',
	'title' => esc_html__('Header styles', 'mint'),
	'subsection' => true,
	'fields' => array(
		array(
			'id' => 'header_styles--gradient',
			'type' => 'switch',
			'title' => esc_html__('Dark gradient on background', 'mint'),
			'subtitle' => esc_html__('Only for negative header.', 'mint'),
			'default' => 0,
		),

		array(
			'id' => 'header_styles--border',
			'type' => 'border',
			'title' => esc_html__('Header border', 'mint'),
			'subtitle' => esc_html__('Select a custom border to be applied in the header.', 'mint'),
			'all' => false,
			'left' => false,
			'right' => false,
			'color' => false,
		),

		array(
			'id' => 'header_styles--border_color',
			'type' => 'color_rgba',
			'title' => esc_html__('Header border color', 'mint'),
			'subtitle' => esc_html__('Select a custom border color to be applied in the header.', 'mint'),
			'default' => array(
				'alpha' => 0,
			),
		),

		array(
			'id' => 'header_styles--padding',
			'type' => 'spacing',
			'mode' => 'padding',
			'title' => esc_html__('Header padding', 'mint'),
			'left' => false,
			'right' => false,
			'units' => 'px',
		),

		array(
			'id' => 'header_styles--logo_padding',
			'type' => 'spacing',
			'mode' => 'padding',
			'title' => esc_html__('Logo padding', 'mint'),
			'subtitle' => esc_html__('Select a custom padding to be applied in the logo.', 'mint'),
			'left' => false,
			'right' => false,
			'units' => 'px',
		),

		array(
			'id' => 'header_styles--main_menu_padding',
			'type' => 'spacing',
			'mode' => 'padding',
			'title' => esc_html__('Main menu padding', 'mint'),
			'subtitle' => esc_html__('Select a custom padding to be applied in the main menu.', 'mint'),
			'left' => false,
			'right' => false,
			'units' => 'px',
		),

		array(
			'id' => 'header_styles--modules_padding',
			'type' => 'spacing',
			'mode' => 'padding',
			'title' => esc_html__('Modules padding', 'mint'),
			'subtitle' => esc_html__('Select a custom padding to be applied in the modules.', 'mint'),
			'left' => false,
			'right' => false,
			'units' => 'px',
		),

		array(
			'id' => 'header_styles--additional_menu_padding',
			'type' => 'spacing',
			'mode' => 'padding',
			'title' => esc_html__('Additional menu padding', 'mint'),
			'subtitle' => esc_html__('Select a custom padding to be applied in the additional menu.', 'mint'),
			'left' => false,
			'right' => false,
			'units' => 'px',
		),
	)
) );

Redux::setSection( $theme_options, array(
	'id' => 'sec_popup_menu',
	'title' => esc_html__('Popup/Mobile menu', 'mint'),
	'subsection' => true,
	'fields' => array(
		array(
			'id' => 'popup_menu--color_scheme',
			'type' => 'select',
			'title' => esc_html__('Color scheme for popup/mobile menu', 'mint'),
			'options' => array(
				'light' => esc_html__('Light text', 'mint'),
				'dark' => esc_html__('Dark text', 'mint'),
			),
			'default' => 'dark',
			'validate' => 'not_empty',
		),

		array(
			'id' => 'popup_menu_styles--align',
			'type' => 'button_set',
			'title' => esc_html__('Text align', 'mint'),
			'options' => array(
				'left' => esc_html__('Left', 'mint'),
				'center' => esc_html__('Center', 'mint'),
				'right' => esc_html__('Right', 'mint'),
			),
			'default' => 'left',
		),

		array(
			'id' => 'popup_menu_styles--bg',
			'type' => 'background',
			'title' => esc_html__('Popup/Mobile menu background', 'mint'),
		),
	)
) );
