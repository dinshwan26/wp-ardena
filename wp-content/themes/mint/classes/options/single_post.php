<?php
Redux::setSection( $theme_options, array(
	'id' => 'sec_single_post',
	'title' => esc_html__('Single post', 'mint'),
	'desc' => esc_html__('Change single post templates.', 'mint'),
	'icon' => 'el el-pencil-alt',
	'fields' => array(
		array(
			'id' => 'single_post--layout--sidebars',
			'type' => 'image_select',
			'title' => esc_html__( 'Sidebars in single post', 'mint' ),
			'subtitle' => esc_html__( 'Select the layout to be used in single post.', 'mint' ),
			'options' => array(
				'' => array(
					'alt' => 'default',
					'img' => self::$theme_uri . '/assets/images/sidebars/def.png'
				),
				'without' => array(
					'alt' => 'without sidebar',
					'img' => self::$theme_uri . '/assets/images/sidebars/1c.png'
				),
				'left' => array(
					'alt' => 'sidebar at left',
					'img' => self::$theme_uri . '/assets/images/sidebars/2cl.png'
				),
				'right' => array(
					'alt' => 'sidebar at right',
					'img' => self::$theme_uri . '/assets/images/sidebars/2cr.png'
				),
				'both' => array(
					'alt' => 'both sidebars',
					'img' => self::$theme_uri . '/assets/images/sidebars/3cm.png'
				),
				'both_left' => array(
					'alt' => 'both sidebars at left',
					'img' => self::$theme_uri . '/assets/images/sidebars/3cl.png'
				),
				'both_right' => array(
					'alt' => 'both sidebars at right',
					'img' => self::$theme_uri . '/assets/images/sidebars/3cr.png'
				)
			),
			'default' => '',
		),

		array(
			'id' => 'single_post--layout--content_width',
			'type' => 'select',
			'title' => esc_html__('Content container type', 'mint'),
			'subtitle' => esc_html__('Define container configuration to be used, it can be normal, expanded or compact.', 'mint'),
			'options' => array(
				'expanded' => esc_html__('Expanded', 'mint'),
				'normal' => esc_html__('Normal', 'mint'),
				'compact' => esc_html__('Compact', 'mint'),
			),
			'default' => '',
		),

		array(
			'id' => 'single_post--featured_image',
			'type' => 'switch',
			'title' => esc_html__('Featured image', 'mint'),
			'subtitle' => esc_html__('If on, featured image will be displayed at single post before content.', 'mint'),
			'default' => 1,
		),

		array(
			'id' => 'single_post--date',
			'type' => 'switch',
			'title' => esc_html__('Date', 'mint'),
			'subtitle' => esc_html__('If on, date will be displayed.', 'mint'),
			'default' => 1,
		),

		array(
			'id' => 'single_post--comments_counter',
			'type' => 'switch',
			'title' => esc_html__('Comments counter', 'mint'),
			'subtitle' => esc_html__('If on, comments counter will be displayed.', 'mint'),
			'default' => 1,
		),

		array(
			'id' => 'single_post--categories',
			'type' => 'switch',
			'title' => esc_html__('Categories', 'mint'),
			'subtitle' => esc_html__('If on, categories will be displayed.', 'mint'),
			'default' => 1,
		),

		array(
			'id' => 'single_post--author',
			'type' => 'switch',
			'title' => esc_html__('Author', 'mint'),
			'subtitle' => esc_html__('If on, the author will be displayed.', 'mint'),
			'default' => 0,
		),

		array(
			'id' => 'single_post--share',
			'type' => 'switch',
			'title' => esc_html__('Social share', 'mint'),
			'subtitle' => esc_html__('If on, social share icons will be displayed.', 'mint'),
			'default' => 1,
		),

		array(
			'id' => 'single_post--tags',
			'type' => 'switch',
			'title' => esc_html__('Tags after content', 'mint'),
			'subtitle' => esc_html__('If on, the tags will be displayed after post content.', 'mint'),
			'default' => 1,
		),

		array(
			'id' => 'single_post--nav',
			'type' => 'switch',
			'title' => esc_html__('Post navigation', 'mint'),
			'subtitle' => esc_html__('If on, navigation will be displayed below content.', 'mint'),
			'default' => 1,
		),

		array(
			'id' => 'single_post--header_section',
			'type' => 'section',
			'title' => esc_html__('Header settings at single post', 'mint'),
			'indent' => true,
		),

			array(
				'id' => 'single_post--header--negative_height',
				'type' => 'button_set',
				'title' => esc_html__('Negative height', 'mint'),
				'subtitle' => esc_html__('If on, header and top header will not have height and background and title wrapper or content will be showed behind it.', 'mint'),
				'options' => array(
					'1' => esc_html__('On', 'mint'),
					'' => esc_html__('Default', 'mint'),
					'0' => esc_html__('Off', 'mint'),
				),
				'default' => '',
			),

			array(
				'id' => 'single_post--header--color_scheme',
				'type' => 'select',
				'title' => esc_html__('Color scheme for header', 'mint'),
				'options' => array(
					'light' => esc_html__('Light text', 'mint'),
					'dark' => esc_html__('Dark text', 'mint'),
				),
				'default' => '',
			),

		array(
			'id' => 'single_post--header_section__end',
			'type' => 'section',
			'indent' => false,
		),

		array(
			'id' => 'single_post--title_wrapper_section',
			'type' => 'section',
			'title' => esc_html__('Title wrapper settings at single post', 'mint'),
			'indent' => true,
		),

			array(
				'id' => 'single_post--title_wrapper',
				'type' => 'button_set',
				'title' => esc_html__('Title wrapper at single post', 'mint'),
				'subtitle' => esc_html__('If on, title wrapper will be displayed at single post.', 'mint'),
				'options' => array(
					'1' => esc_html__('On', 'mint'),
					'' => esc_html__('Default', 'mint'),
					'0' => esc_html__('Off', 'mint'),
				),
				'default' => '',
			),

			array(
				'id' => 'single_post--title_wrapper--full_height',
				'type' => 'button_set',
				'title' => esc_html__('Full viewport height at single post', 'mint'),
				'subtitle' => esc_html__('If on, title wrapper will have same height than viewport/window at single post.', 'mint'),
				'options' => array(
					'1' => esc_html__('On', 'mint'),
					'' => esc_html__('Default', 'mint'),
					'0' => esc_html__('Off', 'mint'),
				),
				'default' => '',
			),

			array(
				'id' => 'single_post--title_wrapper--parallax',
				'type' => 'button_set',
				'title' => esc_html__('Title wrapper parallax at single post', 'mint'),
				'options' => array(
					'1' => esc_html__('On', 'mint'),
					'' => esc_html__('Default', 'mint'),
					'0' => esc_html__('Off', 'mint'),
				),
				'default' => '',
			),

			array(
				'id' => 'single_post--title_wrapper--featured_image_on_bg',
				'type' => 'switch',
				'title' => esc_html__('Featured image as title wrapper background', 'mint'),
				'default' => 0,
			),

			array(
				'id' => 'single_post--title_wrapper_styles--align',
				'type' => 'button_set',
				'title' => esc_html__('Text align', 'mint'),
				'options' => array(
					'' => esc_html__('Default', 'mint'),
					'left' => esc_html__('Left', 'mint'),
					'center' => esc_html__('Center', 'mint'),
					'right' => esc_html__('Right', 'mint'),
				),
				'default' => '',
			),

			array(
				'id' => 'single_post--title_wrapper_styles--font_breadcrumb',
				'type' => 'typography',
				'title' => esc_html__('Breadcrumb typography', 'mint'),
				'google' => true,
				'font-backup' => true,
				'letter-spacing' => true,
				'text-transform' => true,
				'subsets' => true,
				'text-align' => false,
				'all_styles' => true,
			),

			array(
				'id' => 'single_post--title_wrapper_styles--font_subtitle',
				'type' => 'typography',
				'title' => esc_html__('Subtitle typography', 'mint'),
				'google' => true,
				'font-backup' => true,
				'letter-spacing' => true,
				'text-transform' => true,
				'subsets' => true,
				'text-align' => false,
				'all_styles' => true,
			),

			array(
				'id' => 'single_post--title_wrapper_styles--font_title',
				'type' => 'typography',
				'title' => esc_html__('Title typography', 'mint'),
				'google' => true,
				'font-backup' => true,
				'letter-spacing' => true,
				'text-transform' => true,
				'subsets' => true,
				'text-align' => false,
				'all_styles' => true,
			),

			array(
				'id' => 'single_post--title_wrapper_styles--font_desc',
				'type' => 'typography',
				'title' => esc_html__('Description typography', 'mint'),
				'subtitle' => esc_html__('Typography to optional description used in pages.', 'mint'),
				'google' => true,
				'font-backup' => true,
				'letter-spacing' => true,
				'text-transform' => true,
				'subsets' => true,
				'text-align' => false,
				'all_styles' => true,
			),

			array(
				'id' => 'single_post--title_wrapper_styles--bg_overlay',
				'type' => 'color_rgba',
				'title' => esc_html__('Title wrapper background overlay', 'mint'),
				'default' => array(
					'alpha' => 0,
				),
			),

		array(
			'id' => 'single_post--title_wrapper_section__end',
			'type' => 'section',
			'indent' => false,
		),
	)
) );
