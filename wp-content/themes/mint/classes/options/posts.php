<?php
Redux::setSection( $theme_options, array(
	'id' => 'sec_posts',
	'title' => esc_html__('Blog', 'mint'),
	'desc' => esc_html__('Change blog and archives templates.', 'mint'),
	'icon' => 'el el-pencil',
	'fields' => array(
		array(
			'id' => 'posts--layout--sidebars',
			'type' => 'image_select',
			'title' => esc_html__( 'Sidebars in blog', 'mint' ),
			'subtitle' => esc_html__( 'Select the layout to be used in blog.', 'mint' ),
			'options' => array(
				'' => array(
					'alt' => 'default',
					'img' => self::$theme_uri . '/assets/images/sidebars/def.png'
				),
				'without' => array(
					'alt' => 'without sidebar',
					'img' => self::$theme_uri . '/assets/images/sidebars/1c.png'
				),
				'left' => array(
					'alt' => 'sidebar at left',
					'img' => self::$theme_uri . '/assets/images/sidebars/2cl.png'
				),
				'right' => array(
					'alt' => 'sidebar at right',
					'img' => self::$theme_uri . '/assets/images/sidebars/2cr.png'
				),
				'both' => array(
					'alt' => 'both sidebars',
					'img' => self::$theme_uri . '/assets/images/sidebars/3cm.png'
				),
				'both_left' => array(
					'alt' => 'both sidebars at left',
					'img' => self::$theme_uri . '/assets/images/sidebars/3cl.png'
				),
				'both_right' => array(
					'alt' => 'both sidebars at right',
					'img' => self::$theme_uri . '/assets/images/sidebars/3cr.png'
				)
			),
			'default' => '',
		),

		array(
			'id' => 'posts--layout--content_width',
			'type' => 'select',
			'title' => esc_html__('Content container type', 'mint'),
			'subtitle' => esc_html__('Define container configuration to be used, it can be normal, expanded or compact.', 'mint'),
			'options' => array(
				'expanded' => esc_html__('Expanded', 'mint'),
				'normal' => esc_html__('Normal', 'mint'),
				'compact' => esc_html__('Compact', 'mint'),
			),
			'default' => '',
		),

		array(
			'id' => 'posts--template',
			'type' => 'select',
			'title' => esc_html__('Posts layout', 'mint'),
			'subtitle' => esc_html__('Select the layout to be used by posts.', 'mint'),
			'options' => array(
				'standard' => 'Standard',
				'boxed' => 'Boxed',
				'grid' => 'Grid',
				'metro' => 'Metro',
				'masonry' => 'Masonry',
			),
			'default' => 'standard',
			'validate' => 'not_empty',
		),

		array(
			'id' => 'posts--img',
			'type' => 'switch',
			'title' => esc_html__('Thumbnails', 'mint'),
			'subtitle' => esc_html__('If on, thumbnails will be displayed in the blog.', 'mint'),
			'default' => 1,
			'required' => array('posts--template', '!=', 'metro'),
		),

		array(
			'id' => 'posts--img_size',
			'type' => 'select',
			'title' => esc_html__('Thumbnails size', 'mint'),
			'options' => self::get_image_size_names(),
			'default' => self::$theme_prefix . '_rectangle_medium__crop',
			'validate' => 'not_empty',
		),

		array(
			'id' => 'posts--desc',
			'type' => 'textarea',
			'title' => esc_html__('Description', 'mint'),
			'subtitle' => esc_html__('You can use a, strong, br, em and strong HTML tags. Use this field to display an optional text on title wrapper layout part.', 'mint'),
			'validate' => 'html_custom',
			'allowed_html' => array(
				'a' => array(
					'href' => array(),
					'title' => array(),
					'target' => array(),
				),
				'br' => array(),
				'em' => array(),
				'strong' => array()
			),
		),

		array(
			'id' => 'posts--categories',
			'type' => 'switch',
			'title' => esc_html__('Categories', 'mint'),
			'subtitle' => esc_html__('If on, categories will be displayed.', 'mint'),
			'default' => 1,
		),

		array(
			'id' => 'posts--nav',
			'type' => 'switch',
			'title' => esc_html__('Posts navigation', 'mint'),
			'subtitle' => esc_html__('If on, navigation will be displayed below posts.', 'mint'),
			'default' => 1,
		),

		array(
			'id' => 'posts--header_section',
			'type' => 'section',
			'title' => esc_html__('Header settings at blog', 'mint'),
			'indent' => true,
		),

			array(
				'id' => 'posts--header--color_scheme',
				'type' => 'select',
				'title' => esc_html__('Color scheme for header', 'mint'),
				'options' => array(
					'light' => esc_html__('Light text', 'mint'),
					'dark' => esc_html__('Dark text', 'mint'),
				),
				'default' => '',
			),

		array(
			'id' => 'posts--header_section__end',
			'type' => 'section',
			'indent' => false,
		),

		array(
			'id' => 'posts--title_wrapper_section',
			'type' => 'section',
			'title' => esc_html__('Title wrapper settings at blog', 'mint'),
			'indent' => true,
		),

			array(
				'id' => 'posts--title_wrapper',
				'type' => 'button_set',
				'title' => esc_html__('Enable this layout part?', 'mint'),
				'subtitle' => esc_html__('If on, this layout part will be displayed.', 'mint'),
				'options' => array(
					'1' => esc_html__('On', 'mint'),
					'' => esc_html__('Default', 'mint'),
					'0' => esc_html__('Off', 'mint'),
				),
				'default' => '',
			),

			array(
				'id' => 'posts--title_wrapper--subtitle',
				'type' => 'text',
				'title' => esc_html__('Subtitle', 'mint'),
				'subtitle' => esc_html__('Subtitle will be displayed above the title.', 'mint'),
			),

			array(
				'id' => 'posts--title_wrapper_styles--font_breadcrumb',
				'type' => 'typography',
				'title' => esc_html__('Breadcrumb typography', 'mint'),
				'google' => true,
				'font-backup' => true,
				'letter-spacing' => true,
				'text-transform' => true,
				'subsets' => true,
				'text-align' => false,
				'all_styles' => true,
			),

			array(
				'id' => 'posts--title_wrapper_styles--font_subtitle',
				'type' => 'typography',
				'title' => esc_html__('Subtitle typography', 'mint'),
				'google' => true,
				'font-backup' => true,
				'letter-spacing' => true,
				'text-transform' => true,
				'subsets' => true,
				'text-align' => false,
				'all_styles' => true,
			),

			array(
				'id' => 'posts--title_wrapper_styles--font_title',
				'type' => 'typography',
				'title' => esc_html__('Title typography', 'mint'),
				'google' => true,
				'font-backup' => true,
				'letter-spacing' => true,
				'text-transform' => true,
				'subsets' => true,
				'text-align' => false,
				'all_styles' => true,
			),

			array(
				'id' => 'posts--title_wrapper_styles--font_desc',
				'type' => 'typography',
				'title' => esc_html__('Description typography', 'mint'),
				'subtitle' => esc_html__('Typography to optional description used in pages.', 'mint'),
				'google' => true,
				'font-backup' => true,
				'letter-spacing' => true,
				'text-transform' => true,
				'subsets' => true,
				'text-align' => false,
				'all_styles' => true,
			),

			array(
				'id' => 'posts--title_wrapper_styles--bg',
				'type' => 'background',
				'title' => esc_html__('Title wrapper background', 'mint'),
				'subtitle' => esc_html__('Overwrite title wrapper at blog and archives.', 'mint'),
			),

			array(
				'id' => 'posts--title_wrapper_styles--bg_overlay',
				'type' => 'color_rgba',
				'title' => esc_html__('Title wrapper background overlay', 'mint'),
				'default' => array(
					'alpha' => 0,
				),
			),

		array(
			'id' => 'posts--title_wrapper_section__end',
			'type' => 'section',
			'indent' => false,
		),

		array(
			'id' => 'posts--content_section',
			'type' => 'section',
			'title' => esc_html__('Content settings at blog', 'mint'),
			'indent' => true,
		),

			array(
				'id'=>'posts--content--dynamic_area__before',
				'type' => 'select',
				'title' => esc_html__('Dynamic area before posts', 'mint'),
				'subtitle' => esc_html__('Select the page which content will be loaded and displayed before posts.', 'mint'),
				'data' => 'pages',
				'default' => '',
			),

			array(
				'id'=>'posts--content--dynamic_area__after',
				'type' => 'select',
				'title' => esc_html__('Dynamic area after posts', 'mint'),
				'subtitle' => esc_html__('Select the page which content will be loaded and displayed after posts.', 'mint'),
				'data' => 'pages',
				'default' => '',
			),

			array(
				'id' => 'posts--content_styles--padding',
				'type' => 'spacing',
				'mode' => 'padding',
				'title' => esc_html__('Content padding', 'mint'),
				'right' => false,
				'left' => false,
				'units' => 'px',
			),

		array(
			'id' => 'posts--content_section__end',
			'type' => 'section',
			'indent' => false,
		),
	)
) );
