<?php
$boxSections[] = array(
	'title' => esc_html__('Title wrapper', 'mint'),
	'desc' => esc_html__('Change the title wrapper section configuration.', 'mint'),
	'fields' => array(
		array(
			'id' => 'local_title_wrapper',
			'type' => 'button_set',
			'title' => esc_html__('Enable this layout part?', 'mint'),
			'subtitle' => esc_html__('If on, this layout part will be displayed.', 'mint'),
			'options' => array(
				'1' => esc_html__('On', 'mint'),
				'' => esc_html__('Default', 'mint'),
				'0' => esc_html__('Off', 'mint'),
			),
			'default' => '',
		),

		array(
			'id' => 'local_title_wrapper--full_height',
			'type' => 'button_set',
			'title' => esc_html__('Full viewport height', 'mint'),
			'subtitle' => esc_html__('If on, title wrapper will have same height than viewport/window.', 'mint'),
			'options' => array(
				'1' => esc_html__('On', 'mint'),
				'' => esc_html__('Default', 'mint'),
				'0' => esc_html__('Off', 'mint'),
			),
			'default' => '',
		),

		array(
			'id' => 'local_title_wrapper--parallax',
			'type' => 'button_set',
			'title' => esc_html__('Parallax', 'mint'),
			'options' => array(
				'1' => esc_html__('On', 'mint'),
				'' => esc_html__('Default', 'mint'),
				'0' => esc_html__('Off', 'mint'),
			),
			'default' => '',
		),

		array(
			'id' => 'local_title_wrapper--breadcrumb',
			'type' => 'button_set',
			'title' => esc_html__('Breadcrumb', 'mint'),
			'options' => array(
				'1' => esc_html__('On', 'mint'),
				'' => esc_html__('Default', 'mint'),
				'0' => esc_html__('Off', 'mint'),
			),
			'default' => '',
		),

		array(
			'id' => 'local_title_wrapper--desc',
			'type' => 'button_set',
			'title' => esc_html__('Description after title', 'mint'),
			'options' => array(
				'1' => esc_html__('On', 'mint'),
				'' => esc_html__('Default', 'mint'),
				'0' => esc_html__('Off', 'mint'),
			),
			'default' => '',
		),

		array(
			'id' => 'local_title_wrapper--subtitle',
			'type' => 'text',
			'title' => esc_html__('Subtitle', 'mint'),
			'subtitle' => esc_html__('Subtitle will be displayed above the title (not for posts).', 'mint'),
		),

		array(
			'id' => 'local_title_wrapper--desc_text',
			'type' => 'textarea',
			'title' => esc_html__('Description', 'mint'),
			'subtitle' => esc_html__('You can use a, strong, br, em and strong HTML tags. Use this field to display an optional text below main page title.', 'mint'),
			'validate' => 'html_custom',
			'allowed_html' => array(
				'a' => array(
					'href' => array(),
					'title' => array(),
					'target' => array(),
				),
				'br' => array(),
				'em' => array(),
				'strong' => array()
			),
		),

		array(
			'id' => 'local_title_wrapper_styles--padding',
			'type' => 'spacing',
			'mode' => 'padding',
			'title' => esc_html__('Title wrapper padding', 'mint'),
			'right' => false,
			'left' => false,
			'units' => 'px',
		),

		array(
			'id' => 'local_title_wrapper_styles--align',
			'type' => 'button_set',
			'title' => esc_html__('Text align', 'mint'),
			'options' => array(
				'' => esc_html__('Default', 'mint'),
				'left' => esc_html__('Left', 'mint'),
				'center' => esc_html__('Center', 'mint'),
				'right' => esc_html__('Right', 'mint'),
			),
			'default' => '',
		),

		array(
			'id' => 'local_title_wrapper_styles--font_breadcrumb',
			'type' => 'typography',
			'title' => esc_html__('Breadcrumb typography', 'mint'),
			'google' => true,
			'font-backup' => true,
			'letter-spacing' => true,
			'text-transform' => true,
			'subsets' => true,
			'text-align' => false,
			'all_styles' => true,
		),

		array(
			'id' => 'local_title_wrapper_styles--font_breadcrumb__custom_family',
			'type' => 'text',
			'title' => esc_html__('Breadcrumb typography: custom font family', 'mint'),
			'subtitle' => esc_html__('You can use here your Typekit fonts.', 'mint'),
			'default' => '',
			'placeholder' => '"proxima-nova", Arial, Helvetica, sans-serif',
			'validate' => 'no_html',
		),

		array(
			'id' => 'local_title_wrapper_styles--font_subtitle',
			'type' => 'typography',
			'title' => esc_html__('Subtitle typography', 'mint'),
			'google' => true,
			'font-backup' => true,
			'letter-spacing' => true,
			'text-transform' => true,
			'subsets' => true,
			'text-align' => false,
			'all_styles' => true,
		),

		array(
			'id' => 'local_title_wrapper_styles--font_subtitle__custom_family',
			'type' => 'text',
			'title' => esc_html__('Subtitle typography: custom font family', 'mint'),
			'subtitle' => esc_html__('You can use here your Typekit fonts.', 'mint'),
			'default' => '',
			'placeholder' => '"proxima-nova", Arial, Helvetica, sans-serif',
			'validate' => 'no_html',
		),

		array(
			'id' => 'local_title_wrapper_styles--font_title',
			'type' => 'typography',
			'title' => esc_html__('Title typography', 'mint'),
			'google' => true,
			'font-backup' => true,
			'letter-spacing' => true,
			'text-transform' => true,
			'subsets' => true,
			'text-align' => false,
			'all_styles' => true,
		),

		array(
			'id' => 'local_title_wrapper_styles--font_title__custom_family',
			'type' => 'text',
			'title' => esc_html__('Title typography: custom font family', 'mint'),
			'subtitle' => esc_html__('You can use here your Typekit fonts.', 'mint'),
			'default' => '',
			'placeholder' => '"proxima-nova", Arial, Helvetica, sans-serif',
			'validate' => 'no_html',
		),

		array(
			'id' => 'local_title_wrapper_styles--font_desc',
			'type' => 'typography',
			'title' => esc_html__('Description typography', 'mint'),
			'subtitle' => esc_html__('Typography to optional description used in pages.', 'mint'),
			'google' => true,
			'font-backup' => true,
			'letter-spacing' => true,
			'text-transform' => true,
			'subsets' => true,
			'text-align' => false,
			'all_styles' => true,
		),

		array(
			'id' => 'local_title_wrapper_styles--font_desc__custom_family',
			'type' => 'text',
			'title' => esc_html__('Description typography: custom font family', 'mint'),
			'subtitle' => esc_html__('You can use here your Typekit fonts.', 'mint'),
			'default' => '',
			'placeholder' => '"proxima-nova", Arial, Helvetica, sans-serif',
			'validate' => 'no_html',
		),

		array(
			'id' => 'local_title_wrapper_styles--bg',
			'type' => 'background',
			'title' => esc_html__('Title wrapper background', 'mint'),
		),

		array(
			'id' => 'local_title_wrapper_styles--bg_overlay',
			'type' => 'color_rgba',
			'title' => esc_html__('Title wrapper background ovarlay', 'mint'),
			'default' => array(
				'alpha' => 0,
			),
		),

		array(
			'id' => 'local_title_wrapper_styles--bg_overlay_pattern',
			'type' => 'button_set',
			'title' => esc_html__('Enable dotted overlay pattern?', 'mint'),
			'options' => array(
				'1' => esc_html__('On', 'mint'),
				'0' => esc_html__('Off', 'mint'),
			),
			'default' => '0',
		),
	),
);
