<?php
$boxSections[] = array(
	'title' => esc_html__('Posts', 'mint'),
	'desc' => esc_html__('Change posts templates and configurations.', 'mint'),
	'fields' => array(
		array(
			'id' => 'local_single_post--double_width',
			'type' => 'switch',
			'title' => esc_html__('Double width on posts list', 'mint'),
			'default' => 0,
		),

		array(
			'id' => 'local_single_post--featured_image',
			'type' => 'button_set',
			'title' => esc_html__('Featured image', 'mint'),
			'subtitle' => esc_html__('If on, featured image will be displayed at single post before content.', 'mint'),
			'options' => array(
				'1' => esc_html__('On', 'mint'),
				'' => esc_html__('Default', 'mint'),
				'0' => esc_html__('Off', 'mint'),
			),
			'default' => '',
		),

		array(
			'id' => 'local_single_post--date',
			'type' => 'button_set',
			'title' => esc_html__('Date', 'mint'),
			'subtitle' => esc_html__('If on, date will be displayed.', 'mint'),
			'options' => array(
				'1' => esc_html__('On', 'mint'),
				'' => esc_html__('Default', 'mint'),
				'0' => esc_html__('Off', 'mint'),
			),
			'default' => '',
		),

		array(
			'id' => 'local_single_post--comments_counter',
			'type' => 'button_set',
			'title' => esc_html__('Comments counter', 'mint'),
			'subtitle' => esc_html__('If on, comments counter will be displayed.', 'mint'),
			'options' => array(
				'1' => esc_html__('On', 'mint'),
				'' => esc_html__('Default', 'mint'),
				'0' => esc_html__('Off', 'mint'),
			),
			'default' => '',
		),

		array(
			'id' => 'local_single_post--categories',
			'type' => 'button_set',
			'title' => esc_html__('Categories', 'mint'),
			'subtitle' => esc_html__('If on, categories will be displayed.', 'mint'),
			'options' => array(
				'1' => esc_html__('On', 'mint'),
				'' => esc_html__('Default', 'mint'),
				'0' => esc_html__('Off', 'mint'),
			),
			'default' => '',
		),

		array(
			'id' => 'local_single_post--author',
			'type' => 'button_set',
			'title' => esc_html__('Author', 'mint'),
			'subtitle' => esc_html__('If on, the author will be displayed in title wrapper.', 'mint'),
			'options' => array(
				'1' => esc_html__('On', 'mint'),
				'' => esc_html__('Default', 'mint'),
				'0' => esc_html__('Off', 'mint'),
			),
			'default' => '',
		),

		array(
			'id' => 'local_single_post--share',
			'type' => 'button_set',
			'title' => esc_html__('Social share', 'mint'),
			'subtitle' => esc_html__('If on, social share icons will be displayed in title wrapper.', 'mint'),
			'options' => array(
				'1' => esc_html__('On', 'mint'),
				'' => esc_html__('Default', 'mint'),
				'0' => esc_html__('Off', 'mint'),
			),
			'default' => '',
		),

		array(
			'id' => 'local_single_post--tags',
			'type' => 'button_set',
			'title' => esc_html__('Tags after content', 'mint'),
			'subtitle' => esc_html__('If on, the tags will be displayed after post content.', 'mint'),
			'options' => array(
				'1' => esc_html__('On', 'mint'),
				'' => esc_html__('Default', 'mint'),
				'0' => esc_html__('Off', 'mint'),
			),
			'default' => '',
		),

		array(
			'id' => 'local_single_post--nav',
			'type' => 'button_set',
			'title' => esc_html__('Post navigation', 'mint'),
			'subtitle' => esc_html__('If on, navigation will be displayed below content.', 'mint'),
			'options' => array(
				'1' => esc_html__('On', 'mint'),
				'' => esc_html__('Default', 'mint'),
				'0' => esc_html__('Off', 'mint'),
			),
			'default' => '',
		),
	)
);
