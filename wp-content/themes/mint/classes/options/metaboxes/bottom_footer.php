<?php
$boxSections[] = array(
	'title' => esc_html__('Bottom footer', 'mint'),
	'desc' => esc_html__('Change the bottom footer section configuration.', 'mint'),
	'fields' => array(
		array(
			'id' => 'local_bottom_footer',
			'type' => 'button_set',
			'title' => esc_html__('Enable this layout part?', 'mint'),
			'subtitle' => esc_html__('If on, this layout part will be displayed.', 'mint'),
			'options' => array(
				'1' => esc_html__('On', 'mint'),
				'' => esc_html__('Default', 'mint'),
				'0' => esc_html__('Off', 'mint'),
			),
			'default' => '',
		),
	),
);
