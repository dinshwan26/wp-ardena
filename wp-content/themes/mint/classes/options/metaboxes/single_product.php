<?php
$boxSections[] = array(
	'title' => esc_html__('Single product', 'mint'),
	'desc' => esc_html__('Change single product templates and configurations.', 'mint'),
	'fields' => array(
		array(
			'id' => 'local_products--catalog_mode',
			'type' => 'button_set',
			'title' => esc_html__('Enable catalog mode?', 'mint'),
			'subtitle' => esc_html__('If on, Add to Cart buttons will not be displayed to users.', 'mint'),
			'options' => array(
				'1' => esc_html__('On', 'mint'),
				'' => esc_html__('Default', 'mint'),
				'0' => esc_html__('Off', 'mint'),
			),
			'default' => '',
		),

		array(
			'id' => 'local_single_product--extra_tab',
			'type' => 'button_set',
			'title' => esc_html__('Enable extra tab?', 'mint'),
			'subtitle' => esc_html__('If on, an additional global tab will be displayed in products tabs.', 'mint'),
			'options' => array(
				'1' => esc_html__('On', 'mint'),
				'' => esc_html__('Default', 'mint'),
				'0' => esc_html__('Off', 'mint'),
			),
			'default' => '',
		),

		array(
			'id' => 'local_single_product--share',
			'type' => 'button_set',
			'title' => esc_html__('Enable share?', 'mint'),
			'subtitle' => esc_html__('If on, share icons below product details will be displayed.', 'mint'),
			'options' => array(
				'1' => esc_html__('On', 'mint'),
				'' => esc_html__('Default', 'mint'),
				'0' => esc_html__('Off', 'mint'),
			),
			'default' => '',
		),

		array(
			'id' => 'local_single_product--related_products',
			'type' => 'button_set',
			'title' => esc_html__('Enable related products?', 'mint'),
			'subtitle' => esc_html__('If on, related products will be displayed.', 'mint'),
			'options' => array(
				'1' => esc_html__('On', 'mint'),
				'' => esc_html__('Default', 'mint'),
				'0' => esc_html__('Off', 'mint'),
			),
			'default' => '',
		),
	)
);
