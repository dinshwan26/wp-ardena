<?php
$boxSections[] = array(
	'title' => esc_html__('Header', 'mint'),
	'desc' => esc_html__('Change the header section configuration.', 'mint'),
	'fields' => array(
		array(
			'id' => 'local_header',
			'type' => 'button_set',
			'title' => esc_html__('Enable header?', 'mint'),
			'subtitle' => esc_html__('If on, this layout part will be displayed in website.', 'mint'),
			'options' => array(
				'1' => esc_html__('On', 'mint'),
				'' => esc_html__('Default', 'mint'),
				'0' => esc_html__('Off', 'mint'),
			),
			'default' => '',
		),

		array(
			'id' => 'local_header--fixed',
			'type' => 'button_set',
			'title' => esc_html__('Fixed header', 'mint'),
			'subtitle' => esc_html__('If on, the header will be fixed at screen top on page scroll.', 'mint'),
			'options' => array(
				'1' => esc_html__('On', 'mint'),
				'' => esc_html__('Default', 'mint'),
				'0' => esc_html__('Off', 'mint'),
			),
			'default' => '',
		),

		array(
			'id' => 'local_header--negative_height',
			'type' => 'button_set',
			'title' => esc_html__('Negative height', 'mint'),
			'subtitle' => esc_html__('If on, header and top header will not have height and background and title wrapper or content will be showed behind it.', 'mint'),
			'options' => array(
				'1' => esc_html__('On', 'mint'),
				'' => esc_html__('Default', 'mint'),
				'0' => esc_html__('Off', 'mint'),
			),
			'default' => '',
		),

		array(
			'id' => 'local_header--boxed',
			'type' => 'button_set',
			'title' => esc_html__('Boxed', 'mint'),
			'options' => array(
				'1' => esc_html__('On', 'mint'),
				'' => esc_html__('Default', 'mint'),
				'0' => esc_html__('Off', 'mint'),
			),
			'default' => '',
		),

		array(
			'id' => 'local_header--template',
			'type' => 'image_select',
			'title' => esc_html__('Header layout', 'mint'),
			'options' => array(
				'' => array(
					'alt' => 'default',
					'img' => self::$theme_uri . '/assets/images/sidebars/def.png'
				),
				'1' => array(
					'alt' => 'Logo - Menu and Modules',
					'img' => self::$theme_uri . '/assets/images/header-layouts/1.png'
				),
				'2' => array(
					'alt' => 'Additional Menu - Logo - Menu and Modules',
					'img' => self::$theme_uri . '/assets/images/header-layouts/2.png'
				),
				'3' => array(
					'alt' => 'Logo / Menu and Modules',
					'img' => self::$theme_uri . '/assets/images/header-layouts/3.png'
				),
				'4' => array(
					'alt' => 'Logo - Menu',
					'img' => self::$theme_uri . '/assets/images/header-layouts/4.png'
				),
				'5' => array(
					'alt' => 'Additional Menu - Logo - Menu',
					'img' => self::$theme_uri . '/assets/images/header-layouts/5.png'
				),
				'6' => array(
					'alt' => 'Logo / Menu',
					'img' => self::$theme_uri . '/assets/images/header-layouts/6.png'
				),
				'7' => array(
					'alt' => 'Logo - Modules and Popup Menu',
					'img' => self::$theme_uri . '/assets/images/header-layouts/7.png'
				),
				'8' => array(
					'alt' => 'Logo - Popup Menu',
					'img' => self::$theme_uri . '/assets/images/header-layouts/8.png'
				),
			),
			'default' => '',
		),

		array(
			'id' => 'local_header--color_scheme',
			'type' => 'select',
			'title' => esc_html__('Color scheme for header', 'mint'),
			'options' => array(
				'light' => esc_html__('Light text', 'mint'),
				'dark' => esc_html__('Dark text', 'mint'),
			),
			'default' => '',
		),

		array(
			'id' => 'local_header--separator',
			'type' => 'button_set',
			'title' => esc_html__('Separator between menu and modules', 'mint'),
			'options' => array(
				'1' => esc_html__('On', 'mint'),
				'' => esc_html__('Default', 'mint'),
				'0' => esc_html__('Off', 'mint'),
			),
			'default' => '',
		),

		array(
			'id' => 'local_header--mobile_menu',
			'type' => 'button_set',
			'title' => esc_html__('Mobile menu', 'mint'),
			'subtitle' => esc_html__('If on, a mobile menu link will be displayed on mobile devices (smaller than 768px) and main menu will be hided.', 'mint'),
			'options' => array(
				'1' => esc_html__('On', 'mint'),
				'' => esc_html__('Default', 'mint'),
				'0' => esc_html__('Off', 'mint'),
			),
			'default' => '',
		),

		array(
			'id' => 'local_header--text',
			'type' => 'button_set',
			'title' => esc_html__('Text module', 'mint'),
			'subtitle' => esc_html__('If on, a rich text module will be displayed.', 'mint'),
			'options' => array(
				'1' => esc_html__('On', 'mint'),
				'' => esc_html__('Default', 'mint'),
				'0' => esc_html__('Off', 'mint'),
			),
			'default' => '',
		),

			array(
				'id' => 'local_header--text_content',
				'type' => 'editor',
				'title' => esc_html__('Text module content', 'mint'),
				'subtitle' => esc_html__('Place any text or shortcode to be displayed in header. Use &lt;i class="text-separator"&gt;&lt;/i&gt; to add a separator in the text. Use &lt;i class="fa fa-home"&gt;&lt;/i&gt; to display Font Awesome icons.', 'mint'),
				'default' => '',
				'required' => array('local_header--text', '=', 1),
			),

		array(
			'id' => 'local_header--login_ajax',
			'type' => 'button_set',
			'title' => esc_html__('Login With Ajax', 'mint'),
			'subtitle' => esc_html__('If on, a Login With Ajax module will be displayed. Requires Login With Ajax plugin activated.', 'mint'),
			'options' => array(
				'1' => esc_html__('On', 'mint'),
				'' => esc_html__('Default', 'mint'),
				'0' => esc_html__('Off', 'mint'),
			),
			'default' => '',
		),

		array(
			'id' => 'local_header--wishlist',
			'type' => 'button_set',
			'title' => esc_html__('Wishlist', 'mint'),
			'subtitle' => esc_html__('If on, a wishlist link will be displayed. Requires YITH Woocommerce Wishlist plugin activated.', 'mint'),
			'options' => array(
				'1' => esc_html__('On', 'mint'),
				'' => esc_html__('Default', 'mint'),
				'0' => esc_html__('Off', 'mint'),
			),
			'default' => '',
		),

		array(
			'id' => 'local_header--woo_cart',
			'type' => 'button_set',
			'title' => esc_html__('Woo minicart', 'mint'),
			'subtitle' => esc_html__('If on, a WooCommerce minicart will be displayed. Requires WooCommerce plugin activated.', 'mint'),
			'options' => array(
				'1' => esc_html__('On', 'mint'),
				'' => esc_html__('Default', 'mint'),
				'0' => esc_html__('Off', 'mint'),
			),
			'default' => '',
		),

		array(
			'id' => 'local_header--search',
			'type' => 'button_set',
			'title' => esc_html__('Search', 'mint'),
			'subtitle' => esc_html__('If on, a search module will be displayed.', 'mint'),
			'options' => array(
				'1' => esc_html__('On', 'mint'),
				'' => esc_html__('Default', 'mint'),
				'0' => esc_html__('Off', 'mint'),
			),
			'default' => '',
		),

		array(
			'id' => 'local_header--social',
			'type' => 'button_set',
			'title' => esc_html__('Social module', 'mint'),
			'subtitle' => esc_html__('If on, a social icon module will be displayed.', 'mint'),
			'options' => array(
				'1' => esc_html__('On', 'mint'),
				'' => esc_html__('Default', 'mint'),
				'0' => esc_html__('Off', 'mint'),
			),
			'default' => '',
		),

		array(
			'id' => 'local_header_styles--gradient',
			'type' => 'button_set',
			'title' => esc_html__('Dark gradient on background', 'mint'),
			'subtitle' => esc_html__('Only for negative header.', 'mint'),
			'options' => array(
				'1' => esc_html__('On', 'mint'),
				'' => esc_html__('Default', 'mint'),
				'0' => esc_html__('Off', 'mint'),
			),
			'default' => '',
		),

		array(
			'id' => 'local_header_styles--border',
			'type' => 'border',
			'title' => esc_html__('Header border', 'mint'),
			'subtitle' => esc_html__('Select a custom border to be applied in the header.', 'mint'),
			'all' => false,
			'left' => false,
			'right' => false,
			'style' => false,
			'color' => false,
		),

		array(
			'id' => 'local_header_styles--padding',
			'type' => 'spacing',
			'mode' => 'padding',
			'title' => esc_html__('Header padding', 'mint'),
			'left' => false,
			'right' => false,
			'units' => 'px',
		),

		array(
			'id' => 'local_header_styles--logo_padding',
			'type' => 'spacing',
			'mode' => 'padding',
			'title' => esc_html__('Logo padding', 'mint'),
			'subtitle' => esc_html__('Select a custom padding to be applied in the logo.', 'mint'),
			'left' => false,
			'right' => false,
			'units' => 'px',
		),

		array(
			'id' => 'local_header_styles--main_menu_padding',
			'type' => 'spacing',
			'mode' => 'padding',
			'title' => esc_html__('Main menu padding', 'mint'),
			'subtitle' => esc_html__('Select a custom padding to be applied in the main menu.', 'mint'),
			'left' => false,
			'right' => false,
			'units' => 'px',
		),

		array(
			'id' => 'local_header_styles--modules_padding',
			'type' => 'spacing',
			'mode' => 'padding',
			'title' => esc_html__('Modules padding', 'mint'),
			'subtitle' => esc_html__('Select a custom padding to be applied in the modules.', 'mint'),
			'left' => false,
			'right' => false,
			'units' => 'px',
		),

		array(
			'id' => 'local_header_styles--additional_menu_padding',
			'type' => 'spacing',
			'mode' => 'padding',
			'title' => esc_html__('Additional menu padding', 'mint'),
			'subtitle' => esc_html__('Select a custom padding to be applied in the additional menu.', 'mint'),
			'left' => false,
			'right' => false,
			'units' => 'px',
		),

		array(
			'id' => 'local_popup_menu--color_scheme',
			'type' => 'select',
			'title' => esc_html__('Color scheme for popup menu', 'mint'),
			'options' => array(
				'light' => esc_html__('Light text', 'mint'),
				'dark' => esc_html__('Dark text', 'mint'),
			),
			'default' => '',
		),

		array(
			'id' => 'local_popup_menu_styles--bg',
			'type' => 'background',
			'title' => esc_html__('Popup menu background', 'mint'),
		),
	),
);
