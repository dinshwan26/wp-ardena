<?php
$boxSections[] = array(
	'title' => esc_html__('Layout', 'mint'),
	'desc' => esc_html__('Change the main theme\'s layout and configure it.', 'mint'),
	'fields' => array(
		array(
			'id' => 'local_general_styles--accent',
			'type' => 'color',
			'title' => esc_html__('Accent color', 'mint'),
			'subtitle' => esc_html__('Pick an accent color to overwrite the default from the theme. Usually used for links and buttons.', 'mint'),
			'transparent' => false,
			'validate' => 'color',
		),

		array(
			'id' => 'local_layout',
			'type' => 'select',
			'title' => esc_html__('Normal or boxed', 'mint'),
			'options' => array(
				'normal' => esc_html__('Normal', 'mint'),
				'boxed' => esc_html__('Boxed', 'mint'),
				'boxed_laterals' => esc_html__('Boxed only lateral margins', 'mint'),
				'bordered' => esc_html__('Bordered', 'mint'),
			),
			'default' => '',
		),

			array(
				'id' => 'local_layout--border',
				'type' => 'border',
				'title' => esc_html__('Layout border', 'mint'),
				'subtitle' => esc_html__('Select a custom border to be applied in the viewport/window.', 'mint'),
				'all' => false,
				'style' => false,
			),

		array(
			'id' => 'local_layout--sidebars',
			'type' => 'image_select',
			'title' => esc_html__( 'Sidebars', 'mint' ),
			'options' => array(
				'' => array(
					'alt' => 'default',
					'img' => self::$theme_uri . '/assets/images/sidebars/def.png'
				),
				'without' => array(
					'alt' => 'without sidebar',
					'img' => self::$theme_uri . '/assets/images/sidebars/1c.png'
				),
				'left' => array(
					'alt' => 'sidebar at left',
					'img' => self::$theme_uri . '/assets/images/sidebars/2cl.png'
				),
				'right' => array(
					'alt' => 'sidebar at right',
					'img' => self::$theme_uri . '/assets/images/sidebars/2cr.png'
				),
				'both' => array(
					'alt' => 'both sidebars',
					'img' => self::$theme_uri . '/assets/images/sidebars/3cm.png'
				),
				'both_left' => array(
					'alt' => 'both sidebars at left',
					'img' => self::$theme_uri . '/assets/images/sidebars/3cl.png'
				),
				'both_right' => array(
					'alt' => 'both sidebars at right',
					'img' => self::$theme_uri . '/assets/images/sidebars/3cr.png'
				)
			),
			'default' => '',
		),

		array(
			'id' => 'local_layout--header_width',
			'type' => 'select',
			'title' => esc_html__('Header container type', 'mint'),
			'subtitle' => esc_html__('Define container configuration to be used, it can be normal, expanded or compact.', 'mint'),
			'options' => array(
				'expanded' => esc_html__('Expanded', 'mint'),
				'normal' => esc_html__('Normal', 'mint'),
				'compact' => esc_html__('Compact', 'mint'),
			),
			'default' => '',
		),

		array(
			'id' => 'local_layout--content_width',
			'type' => 'select',
			'title' => esc_html__('Content container type', 'mint'),
			'subtitle' => esc_html__('Define container configuration to be used, it can be normal, expanded or compact.', 'mint'),
			'options' => array(
				'expanded' => esc_html__('Expanded', 'mint'),
				'normal' => esc_html__('Normal', 'mint'),
				'compact' => esc_html__('Compact', 'mint'),
			),
			'default' => '',
		),

		array(
			'id' => 'local_layout--footer_width',
			'type' => 'select',
			'title' => esc_html__('Footer container type', 'mint'),
			'subtitle' => esc_html__('Define container configuration to be used, it can be normal, expanded or compact.', 'mint'),
			'options' => array(
				'expanded' => esc_html__('Expanded', 'mint'),
				'normal' => esc_html__('Normal', 'mint'),
				'compact' => esc_html__('Compact', 'mint'),
			),
			'default' => '',
		),

		array(
			'id' => 'local_general_styles--bg',
			'type' => 'background',
			'title' => esc_html__('Body background', 'mint'),
			'subtitle' => esc_html__('Body background with image, color and other options. Usually visible only when using boxed layout.', 'mint'),
		),
	),
);
