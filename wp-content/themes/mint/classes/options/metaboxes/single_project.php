<?php
$boxSections[] = array(
	'title' => esc_html__('Projects', 'mint'),
	'desc' => esc_html__('Change projects templates and configurations.', 'mint'),
	'fields' => array(
		array(
			'id' => 'local_single_project--double_width',
			'type' => 'switch',
			'title' => esc_html__('Double width on projects list', 'mint'),
			'default' => 0,
		),

		array(
			'id' => 'local_single_project--double_height',
			'type' => 'switch',
			'title' => esc_html__('Double height on projects list', 'mint'),
			'default' => 0,
		),

		array(
			'id' => 'local_single_project--nav',
			'type' => 'button_set',
			'title' => esc_html__('Single project navigation', 'mint'),
			'subtitle' => esc_html__('If on, navigation will be displayed below content.', 'mint'),
			'options' => array(
				'1' => esc_html__('On', 'mint'),
				'' => esc_html__('Default', 'mint'),
				'0' => esc_html__('Off', 'mint'),
			),
			'default' => '',
		),
	)
);
