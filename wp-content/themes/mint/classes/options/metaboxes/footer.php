<?php
$boxSections[] = array(
	'title' => esc_html__('Footer', 'mint'),
	'desc' => esc_html__('Change the footer section configuration.', 'mint'),
	'fields' => array(
		array(
			'id' => 'local_footer',
			'type' => 'button_set',
			'title' => esc_html__('Enable this layout part?', 'mint'),
			'subtitle' => esc_html__('If on, this layout part will be displayed.', 'mint'),
			'options' => array(
				'1' => esc_html__('On', 'mint'),
				'' => esc_html__('Default', 'mint'),
				'0' => esc_html__('Off', 'mint'),
			),
			'default' => '',
		),

		array(
			'id' => 'local_footer--fixed',
			'type' => 'button_set',
			'title' => esc_html__('Fixed footer', 'mint'),
			'subtitle' => esc_html__('If on, footer and bottom footer will be fixed at screen bottom on page scroll.', 'mint'),
			'options' => array(
				'1' => esc_html__('On', 'mint'),
				'' => esc_html__('Default', 'mint'),
				'0' => esc_html__('Off', 'mint'),
			),
			'default' => '',
		),
	),
);
