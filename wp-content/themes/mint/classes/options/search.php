<?php
Redux::setSection( $theme_options, array(
	'id' => 'sec_search',
	'title' => esc_html__('Search', 'mint'),
	'desc' => esc_html__('Change search results template and configurations.', 'mint'),
	'icon' => 'el el-search',
	'fields' => array(
		array(
			'id' => 'search--post_type',
			'type' => 'button_set',
			'title' => esc_html__('Search in post types', 'mint'),
			'options' => array(
				'all' => esc_html__('All', 'mint'),
				'post' => esc_html__('Only in posts', 'mint'),
				'product' => esc_html__('Only in products', 'mint'),
				'project' => esc_html__('Only in projects', 'mint'),
			),
			'default' => 'all',
		),

		array(
			'id' => 'search--layout--sidebars',
			'type' => 'image_select',
			'title' => esc_html__( 'Sidebars in search results', 'mint' ),
			'subtitle' => esc_html__( 'Select the layout to be used in search results.', 'mint' ),
			'options' => array(
				'' => array(
					'alt' => 'default',
					'img' => self::$theme_uri . '/assets/images/sidebars/def.png'
				),
				'without' => array(
					'alt' => 'without sidebar',
					'img' => self::$theme_uri . '/assets/images/sidebars/1c.png'
				),
				'left' => array(
					'alt' => 'sidebar at left',
					'img' => self::$theme_uri . '/assets/images/sidebars/2cl.png'
				),
				'right' => array(
					'alt' => 'sidebar at right',
					'img' => self::$theme_uri . '/assets/images/sidebars/2cr.png'
				),
				'both' => array(
					'alt' => 'both sidebars',
					'img' => self::$theme_uri . '/assets/images/sidebars/3cm.png'
				),
				'both_left' => array(
					'alt' => 'both sidebars at left',
					'img' => self::$theme_uri . '/assets/images/sidebars/3cl.png'
				),
				'both_right' => array(
					'alt' => 'both sidebars at right',
					'img' => self::$theme_uri . '/assets/images/sidebars/3cr.png'
				)
			),
			'default' => '',
		),

		array(
			'id' => 'search--layout--content_width',
			'type' => 'select',
			'title' => esc_html__('Content container type', 'mint'),
			'subtitle' => esc_html__('Define container configuration to be used, it can be normal, expanded or compact.', 'mint'),
			'options' => array(
				'expanded' => esc_html__('Expanded', 'mint'),
				'normal' => esc_html__('Normal', 'mint'),
				'compact' => esc_html__('Compact', 'mint'),
			),
			'default' => '',
		),

		array(
			'id' => 'search--columns',
			'type' => 'slider',
			'title' => esc_html__('Columns', 'mint'),
			'subtitle' => esc_html__('Define columns number at shop.', 'mint'),
			'default' => '3',
			'min' => '1',
			'step' => '1',
			'max' => '4',
		),

		array(
			'id' => 'search--title_wrapper_section',
			'type' => 'section',
			'title' => esc_html__('Title wrapper settings at search results', 'mint'),
			'indent' => true,
		),


			array(
				'id' => 'search--title_wrapper_styles--padding',
				'type' => 'spacing',
				'mode' => 'padding',
				'title' => esc_html__('Title wrapper padding', 'mint'),
				'right' => false,
				'left' => false,
				'units' => 'px',
			),

			array(
				'id' => 'search--title_wrapper_styles--align',
				'type' => 'button_set',
				'title' => esc_html__('Text align', 'mint'),
				'options' => array(
					'' => esc_html__('Default', 'mint'),
					'left' => esc_html__('Left', 'mint'),
					'center' => esc_html__('Center', 'mint'),
					'right' => esc_html__('Right', 'mint'),
				),
				'default' => '',
			),

		array(
			'id' => 'search--title_wrapper_section__end',
			'type' => 'section',
			'indent' => false,
		),
	)
) );
