<?php
Redux::setSection( $theme_options, array(
	'id' => 'sec_shop',
	'title' => esc_html__('Shop', 'mint'),
	'desc' => esc_html__('Change shop templates and configurations.', 'mint'),
	'icon' => 'el el-shopping-cart',
	'fields' => array(
		array(
			'id' => 'products--layout--sidebars',
			'type' => 'image_select',
			'title' => esc_html__( 'Sidebars in shop', 'mint' ),
			'subtitle' => esc_html__( 'Select the layout to be used in shop.', 'mint' ),
			'options' => array(
				'' => array(
					'alt' => 'default',
					'img' => self::$theme_uri . '/assets/images/sidebars/def.png'
				),
				'without' => array(
					'alt' => 'without sidebar',
					'img' => self::$theme_uri . '/assets/images/sidebars/1c.png'
				),
				'left' => array(
					'alt' => 'sidebar at left',
					'img' => self::$theme_uri . '/assets/images/sidebars/2cl.png'
				),
				'right' => array(
					'alt' => 'sidebar at right',
					'img' => self::$theme_uri . '/assets/images/sidebars/2cr.png'
				),
				'both' => array(
					'alt' => 'both sidebars',
					'img' => self::$theme_uri . '/assets/images/sidebars/3cm.png'
				),
				'both_left' => array(
					'alt' => 'both sidebars at left',
					'img' => self::$theme_uri . '/assets/images/sidebars/3cl.png'
				),
				'both_right' => array(
					'alt' => 'both sidebars at right',
					'img' => self::$theme_uri . '/assets/images/sidebars/3cr.png'
				)
			),
			'default' => '',
		),

		array(
			'id' => 'products--layout--content_width',
			'type' => 'select',
			'title' => esc_html__('Content container type', 'mint'),
			'subtitle' => esc_html__('Define container configuration to be used, it can be normal, expanded or compact.', 'mint'),
			'options' => array(
				'expanded' => esc_html__('Expanded', 'mint'),
				'normal' => esc_html__('Normal', 'mint'),
				'compact' => esc_html__('Compact', 'mint'),
			),
			'default' => '',
		),

		array(
			'id' => 'products--template',
			'type' => 'select',
			'title' => esc_html__('Products layout', 'mint'),
			'subtitle' => esc_html__('Select the layout to be used by products.', 'mint'),
			'options' => array(
				'card' => esc_html__('Cards', 'mint'),
				'tile' => esc_html__('Tiles', 'mint'),
			),
			'default' => 'card',
		),

			array(
				'id' => 'products--columns',
				'type' => 'slider',
				'title' => esc_html__('Columns', 'mint'),
				'subtitle' => esc_html__('Define columns number at shop.', 'mint'),
				'default' => '3',
				'min' => '1',
				'step' => '1',
				'max' => '4',
				'required' => array('products--template', '=', 'card'),
			),

		array(
			'id' => 'products--img_size',
			'type' => 'select',
			'title' => esc_html__('Thumbnails size', 'mint'),
			'options' => self::get_image_size_names(),
			'default' => 'shop_catalog',
			'validate' => 'not_empty',
		),

		array(
			'id' => 'products--per_page',
			'type' => 'text',
			'title' => esc_html__('Products per page', 'mint'),
			'subtitle' => esc_html__('Define the number of products displayed per page.', 'mint'),
			'default' => '8',
			'validate' => 'numeric',
		),

		array(
			'id' => 'products--catalog_mode',
			'type' => 'switch',
			'title' => esc_html__('Catalog mode', 'mint'),
			'subtitle' => esc_html__('If on, Add to Cart buttons will not be displayed to users.', 'mint'),
			'default' => 0,
		),

			array(
				'id' => 'products--catalog_mode_text',
				'type' => 'textarea',
				'title' => esc_html__('Catalog mode text', 'mint'),
				'subtitle' => esc_html__('What will be displayed instead default Add to Cart button.', 'mint'),
				'default' => 'Get in <a href="#">touch</a> to more details.',
				'required' => array('products--catalog_mode', '=', 1),
			),

		array(
			'id' => 'products--sorting',
			'type' => 'switch',
			'title' => esc_html__('Sorting options', 'mint'),
			'subtitle' => esc_html__('If on, the sorting options will be displayed in the shop, this way users can order by price or others.', 'mint'),
			'default' => 1,
		),

		array(
			'id' => 'products--quick_view',
			'type' => 'switch',
			'title' => esc_html__('Quick view feature', 'mint'),
			'subtitle' => esc_html__('If on, the quick view feature will be avaliable in shop listing.', 'mint'),
			'default' => 1,
		),

		array(
			'id' => 'products--top_header_section',
			'type' => 'section',
			'title' => esc_html__('Top header settings at shop', 'mint'),
			'indent' => true,
		),

			array(
				'id' => 'products--top_header',
				'type' => 'button_set',
				'title' => esc_html__('Enable this layout part?', 'mint'),
				'subtitle' => esc_html__('If on, this layout part will be displayed.', 'mint'),
				'options' => array(
					'1' => esc_html__('On', 'mint'),
					'' => esc_html__('Default', 'mint'),
					'0' => esc_html__('Off', 'mint'),
				),
				'default' => '',
			),

		array(
			'id' => 'products--top_header_section__end',
			'type' => 'section',
			'indent' => false,
		),

		array(
			'id' => 'products--header_section',
			'type' => 'section',
			'title' => esc_html__('Header settings at shop', 'mint'),
			'indent' => true,
		),

			array(
				'id' => 'products--header--negative_height',
				'type' => 'button_set',
				'title' => esc_html__('Negative height', 'mint'),
				'subtitle' => esc_html__('If on, header and top header will not have height and background and title wrapper or content will be showed behind it.', 'mint'),
				'options' => array(
					'1' => esc_html__('On', 'mint'),
					'' => esc_html__('Default', 'mint'),
					'0' => esc_html__('Off', 'mint'),
				),
				'default' => '',
			),

			array(
				'id' => 'products--header--color_scheme',
				'type' => 'select',
				'title' => esc_html__('Color scheme for header', 'mint'),
				'options' => array(
					'light' => esc_html__('Light text', 'mint'),
					'dark' => esc_html__('Dark text', 'mint'),
				),
				'default' => '',
			),

		array(
			'id' => 'products--header_section__end',
			'type' => 'section',
			'indent' => false,
		),

		array(
			'id' => 'products--title_wrapper_section',
			'type' => 'section',
			'title' => esc_html__('Title wrapper settings at shop', 'mint'),
			'indent' => true,
		),

			array(
				'id' => 'products--title_wrapper',
				'type' => 'button_set',
				'title' => esc_html__('Enable this layout part?', 'mint'),
				'subtitle' => esc_html__('If on, this layout part will be displayed.', 'mint'),
				'options' => array(
					'1' => esc_html__('On', 'mint'),
					'' => esc_html__('Default', 'mint'),
					'0' => esc_html__('Off', 'mint'),
				),
				'default' => '',
			),

			array(
				'id' => 'products--title_wrapper--desc',
				'type' => 'button_set',
				'title' => esc_html__('Enable description after title?', 'mint'),
				'options' => array(
					'1' => esc_html__('On', 'mint'),
					'' => esc_html__('Default', 'mint'),
					'0' => esc_html__('Off', 'mint'),
				),
				'default' => '',
			),

			array(
				'id' => 'products--title_wrapper--subtitle',
				'type' => 'text',
				'title' => esc_html__('Subtitle', 'mint'),
				'subtitle' => esc_html__('Subtitle will be displayed above the title.', 'mint'),
			),

			array(
				'id' => 'products--title_wrapper_styles--align',
				'type' => 'button_set',
				'title' => esc_html__('Text align', 'mint'),
				'options' => array(
					'' => esc_html__('Default', 'mint'),
					'left' => esc_html__('Left', 'mint'),
					'center' => esc_html__('Center', 'mint'),
					'right' => esc_html__('Right', 'mint'),
				),
				'default' => '',
			),

			array(
				'id' => 'products--title_wrapper_styles--font_breadcrumb',
				'type' => 'typography',
				'title' => esc_html__('Breadcrumb typography', 'mint'),
				'google' => true,
				'font-backup' => true,
				'letter-spacing' => true,
				'text-transform' => true,
				'subsets' => true,
				'text-align' => false,
				'all_styles' => true,
			),

			array(
				'id' => 'products--title_wrapper_styles--font_subtitle',
				'type' => 'typography',
				'title' => esc_html__('Subtitle typography', 'mint'),
				'google' => true,
				'font-backup' => true,
				'letter-spacing' => true,
				'text-transform' => true,
				'subsets' => true,
				'text-align' => false,
				'all_styles' => true,
			),

			array(
				'id' => 'products--title_wrapper_styles--font_title',
				'type' => 'typography',
				'title' => esc_html__('Title typography', 'mint'),
				'google' => true,
				'font-backup' => true,
				'letter-spacing' => true,
				'text-transform' => true,
				'subsets' => true,
				'text-align' => false,
				'all_styles' => true,
			),

			array(
				'id' => 'products--title_wrapper_styles--font_desc',
				'type' => 'typography',
				'title' => esc_html__('Description typography', 'mint'),
				'subtitle' => esc_html__('Typography to optional description used in pages.', 'mint'),
				'google' => true,
				'font-backup' => true,
				'letter-spacing' => true,
				'text-transform' => true,
				'subsets' => true,
				'text-align' => false,
				'all_styles' => true,
			),

			array(
				'id' => 'products--title_wrapper_styles--bg',
				'type' => 'background',
				'title' => esc_html__('Title wrapper background', 'mint'),
				'subtitle' => esc_html__('Overwrite title wrapper at shop.', 'mint'),
			),

			array(
				'id' => 'products--title_wrapper_styles--bg_overlay',
				'type' => 'color_rgba',
				'title' => esc_html__('Title wrapper background overlay', 'mint'),
				'default' => array(
					'alpha' => 0,
				),
			),

			array(
				'id' => 'products--title_wrapper--category_image_on_bg',
				'type' => 'switch',
				'title' => esc_html__('Category image as title wrapper background', 'mint'),
				'default' => 0,
			),

		array(
			'id' => 'products--title_wrapper_section__end',
			'type' => 'section',
			'indent' => false,
		),

		array(
			'id' => 'products--content_section',
			'type' => 'section',
			'title' => esc_html__('Content settings at shop', 'mint'),
			'indent' => true,
		),

			array(
				'id'=>'products--content--dynamic_area__before',
				'type' => 'select',
				'title' => esc_html__('Dynamic area before products', 'mint'),
				'subtitle' => esc_html__('Select the page which content will be loaded and displayed before products.', 'mint'),
				'data' => 'pages',
				'default' => '',
			),

			array(
				'id'=>'products--content--dynamic_area__after',
				'type' => 'select',
				'title' => esc_html__('Dynamic area after products', 'mint'),
				'subtitle' => esc_html__('Select the page which content will be loaded and displayed after products.', 'mint'),
				'data' => 'pages',
				'default' => '',
			),

			array(
				'id' => 'products--content_styles--padding',
				'type' => 'spacing',
				'mode' => 'padding',
				'title' => esc_html__('Content padding', 'mint'),
				'right' => false,
				'left' => false,
				'units' => 'px',
			),

		array(
			'id' => 'products--content_section__end',
			'type' => 'section',
			'indent' => false,
		),
	)
) );
