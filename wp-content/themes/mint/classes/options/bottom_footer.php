<?php
Redux::setSection( $theme_options, array(
	'id' => 'main_sec_bottom_footer',
	'title' => esc_html__('Bottom footer', 'mint'),
	'icon' => 'el el-chevron-down',
) );

Redux::setSection( $theme_options, array(
	'id' => 'sec_bottom_footer',
	'title' => esc_html__('Bottom footer settings', 'mint'),
	'subsection' => true,
	'fields' => array(
		array(
			'id' => 'bottom_footer',
			'type' => 'switch',
			'title' => esc_html__('Enable this layout part?', 'mint'),
			'subtitle' => esc_html__('If on, this layout part will be displayed.', 'mint'),
			'default' => 1,
		),

		array(
			'id' => 'bottom_footer--left_cols_sm',
			'type' => 'slider',
			'title' => esc_html__('Left area columns', 'mint'),
			'subtitle' => esc_html__('Define columns number of bottom footer left area.', 'mint'),
			'default' => '6',
			'min' => '0',
			'step' => '1',
			'max' => '12',
		),

		array(
			'id' => 'bottom_footer--right_cols_sm',
			'type' => 'slider',
			'title' => esc_html__('Right area columns', 'mint'),
			'subtitle' => esc_html__('Define columns number of bottom footer right area.', 'mint'),
			'default' => '6',
			'min' => '0',
			'step' => '1',
			'max' => '12',
		),

		array(
			'id' => 'bottom_footer--menu',
			'type' => 'switch',
			'title' => esc_html__('Menu', 'mint'),
			'subtitle' => esc_html__('If on, menu will be displayed.', 'mint'),
			'default' => 0,
		),

			array(
				'id' => 'bottom_footer--menu_left',
				'type' => 'switch',
				'title' => esc_html__('Menu at left area', 'mint'),
				'subtitle' => esc_html__('If on, menu will display at left area of bottom footer.', 'mint'),
				'default' => 0,
				'required' => array('bottom_footer--menu', '=', 1),
			),

		array(
			'id' => 'bottom_footer--text',
			'type' => 'switch',
			'title' => esc_html__('Text module', 'mint'),
			'subtitle' => esc_html__('If on, a rich text module will be displayed.', 'mint'),
			'default' => 1,
		),

			array(
				'id' => 'bottom_footer--text_content',
				'type' => 'editor',
				'title' => esc_html__('Text module content', 'mint'),
				'subtitle' => esc_html__('Place any text to be displayed in bottom footer.', 'mint'),
				'default' => 'All rights reserved. Powered by <a href="http://mint.themes.tvda.pw/">Mint Theme</a>.',
				'required' => array('bottom_footer--text', '=', 1),
			),

		array(
			'id' => 'bottom_footer--social',
			'type' => 'switch',
			'title' => esc_html__('Social module', 'mint'),
			'subtitle' => esc_html__('If on, a social icon module will be displayed.', 'mint'),
			'default' => 0,
		),

			array(
				'id' => 'bottom_footer--social_links',
				'type' => 'sortable',
				'mode' => 'checkbox',
				'title' => esc_html__('Social links', 'mint'),
				'subtitle' => esc_html__('Enable social links to be displayed.', 'mint'),
				'options' => self::$social_icons,
				'default' => self::$social_icons_default,
				'required' => array('bottom_footer--social', '=', 1),
			),

		array(
			'id' => 'bottom_footer--wpml_modules_section',
			'type' => 'section',
			'title' => esc_html__('WPML modules', 'mint'),
			'indent' => true,
		),

			array(
				'id' => 'bottom_footer--wpml_lang',
				'type' => 'switch',
				'title' => esc_html__('WPML language flags', 'mint'),
				'subtitle' => esc_html__('If on, the avaliable languages flags will be displayed. Only works with WPML activated.', 'mint'),
				'default' => 0,
			),

			array(
				'id' => 'bottom_footer--wpml_currency',
				'type' => 'switch',
				'title' => esc_html__('WPML shop currencies', 'mint'),
				'subtitle' => esc_html__('If on, the avaliable currencies flags will be displayed. Only works with WPML + WooCommerce Multilingual activated.', 'mint'),
				'default' => 0,
			),

		array(
			'id' => 'bottom_footer--wpml_modules_section__end',
			'type' => 'section',
			'indent' => false,
		),
	)
) );

Redux::setSection( $theme_options, array(
	'id' => 'sec_bottom_footer_styles',
	'title' => esc_html__('Bottom footer styles', 'mint'),
	'subsection' => true,
	'fields' => array(
		array(
			'id' => 'bottom_footer_styles--first_align',
			'type' => 'button_set',
			'title' => esc_html__('First area align', 'mint'),
			'options' => array(
				'left' => esc_html__('Left', 'mint'),
				'center' => esc_html__('Center', 'mint'),
				'right' => esc_html__('Right', 'mint'),
			),
			'default' => 'left',
		),

		array(
			'id' => 'bottom_footer_styles--second_align',
			'type' => 'button_set',
			'title' => esc_html__('Second area align', 'mint'),
			'options' => array(
				'left' => esc_html__('Left', 'mint'),
				'center' => esc_html__('Center', 'mint'),
				'right' => esc_html__('Right', 'mint'),
			),
			'default' => 'right',
		),

		array(
			'id' => 'bottom_footer_styles--border',
			'type' => 'border',
			'title' => esc_html__('Bottom footer border', 'mint'),
			'subtitle' => esc_html__('Select a custom border to be applied in the bottom footer.', 'mint'),
			'all' => false,
			'left' => false,
			'right' => false,
		),

		array(
			'id' => 'bottom_footer_styles--padding',
			'type' => 'spacing',
			'mode' => 'padding',
			'title' => esc_html__('Bottom footer padding', 'mint'),
			'right' => false,
			'left' => false,
			'units' => 'px',
		),

		array(
			'id' => 'bottom_footer_styles--bg',
			'type' => 'background',
			'title' => esc_html__('Bottom footer background', 'mint'),
		),

		array(
			'id' => 'bottom_footer_styles--font',
			'type' => 'typography',
			'title' => esc_html__('Bottom footer typography', 'mint'),
			'google' => true,
			'font-backup' => true,
			'letter-spacing' => true,
			'text-transform' => true,
			'subsets' => true,
			'text-align' => false,
			'all_styles' => true,
		),

		array(
			'id' => 'bottom_footer_styles--font__custom_family',
			'type' => 'text',
			'title' => esc_html__('Bottom footer typography: custom font family', 'mint'),
			'subtitle' => esc_html__('You can use here your Typekit fonts.', 'mint'),
			'default' => '',
			'placeholder' => '"proxima-nova", Arial, Helvetica, sans-serif',
			'validate' => 'no_html',
		),
	)
) );
