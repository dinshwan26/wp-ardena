<?php
Redux::setSection( $theme_options, array(
	'id' => 'main_sec_general',
	'title' => esc_html__('General', 'mint'),
	'icon' => 'el el-dashboard',
) );

Redux::setSection( $theme_options, array(
	'id' => 'sec_general',
	'title' => esc_html__('General settings', 'mint'),
	'desc' => esc_html__('Configure easily the basic theme\'s settings.', 'mint'),
	'subsection' => true,
	'fields' => array(
		array(
			'id' => 'general--page_comments',
			'type' => 'switch',
			'title' => esc_html__('Comments in pages', 'mint'),
			'subtitle' => esc_html__('If on, the comment form will be avaliable in all pages.', 'mint'),
			'default' => 1,
		),

		array(
			'id' => 'general--responsiveness',
			'type' => 'switch',
			'title' => esc_html__('Responsive layout', 'mint'),
			'subtitle' => esc_html__('If on, the website will adapt in smaller devices like tablets or smartphones.', 'mint'),
			'default' => 1,
		),

		array(
			'id' => 'general--wp_version',
			'type' => 'switch',
			'title' => esc_html__('Meta tags in head about wordpress version', 'mint'),
			'subtitle' => esc_html__('If on, meta tags will be show in all pages.', 'mint'),
			'default' => 0,
		),

		array(
			'id' => 'general--wlwmanifest',
			'type' => 'switch',
			'title' => esc_html__('WLW manifest link in head', 'mint'),
			'subtitle' => esc_html__('WLW manifest link is the resource file needed to enable tagging support for Windows Live Writer.', 'mint'),
			'default' => 0,
		),

		array(
			'id' => 'general--rsd',
			'type' => 'switch',
			'title' => esc_html__('RSD link in head', 'mint'),
			'subtitle' => esc_html__('RSD link is used by blog clients, enable if you use blog clients for wordpress.', 'mint'),
			'default' => 0,
		),

		array(
			'id' => 'general--preloader',
			'type' => 'switch',
			'title' => esc_html__('Loader effect', 'mint'),
			'subtitle' => esc_html__('If on, a loader will appear before loading the page.', 'mint'),
			'default' => 0,
		),

		array(
			'id' => 'general--go_to_top',
			'type' => 'switch',
			'title' => esc_html__('"Go to top" button', 'mint'),
			'subtitle' => esc_html__('If on, "Go to top" button will be visible at bottom right corner of viewport/window.', 'mint'),
			'default' => 0,
		),

		array(
			'id' => 'general--typekit_kit_id',
			'type' => 'text',
			'title' => esc_html__('Typekit Kit ID', 'mint'),
			'subtitle' => esc_html__('Define Kit ID, if you want use Typekit fonts.', 'mint'),
			'default' => '',
			'validate' => 'no_html',
		),
	)
) );

Redux::setSection( $theme_options, array(
	'id' => 'sec_general_styles',
	'title' => esc_html__('General styles', 'mint'),
	'subsection' => true,
	'fields' => array(
		array(
			'id' => 'general_styles--accent',
			'type' => 'color',
			'title' => esc_html__('Accent color', 'mint'),
			'subtitle' => esc_html__('Pick an accent color to overwrite the default from the theme. Usually used for links and buttons.', 'mint'),
			'transparent' => false,
			'validate' => 'color',
		),

		array(
			'id' => 'general_styles--font',
			'type' => 'typography',
			'title' => esc_html__('Base font', 'mint'),
			'subtitle' => esc_html__('Font used in the content in general, usually overwrite by local layout fonts, but used in paragraphs, lists and others.', 'mint'),
			'google' => true,
			'font-backup' => true,
			'letter-spacing' => true,
			'text-transform' => true,
			'subsets' => true,
			'text-align' => false,
			'all_styles' => true,
		),

		array(
			'id' => 'general_styles--font__custom_family',
			'type' => 'text',
			'title' => esc_html__('Base font: custom font family', 'mint'),
			'subtitle' => esc_html__('You can use here your Typekit fonts.', 'mint'),
			'default' => '',
			'placeholder' => '"proxima-nova", Arial, Helvetica, sans-serif',
			'validate' => 'no_html',
		),

		array(
			'id' => 'general_styles--font_second',
			'type' => 'typography',
			'title' => esc_html__('Second font', 'mint'),
			'subtitle' => esc_html__('For menus, inputs, buttons, titles and heading elements.', 'mint'),
			'google' => true,
			'font-backup' => true,
			'letter-spacing' => true,
			'text-transform' => true,
			'subsets' => true,
			'text-align' => false,
			'all_styles' => true,
		),

		array(
			'id' => 'general_styles--font_second__custom_family',
			'type' => 'text',
			'title' => esc_html__('Second font: custom font family', 'mint'),
			'subtitle' => esc_html__('You can use here your Typekit fonts.', 'mint'),
			'default' => '',
			'placeholder' => '"proxima-nova", Arial, Helvetica, sans-serif',
			'validate' => 'no_html',
		),

		array(
			'id' => 'general_styles--bg',
			'type' => 'background',
			'title' => esc_html__('Body background', 'mint'),
			'subtitle' => esc_html__('Body background with image, color and other options. Usually visible only when using boxed layout.', 'mint'),
		),
	)
) );
