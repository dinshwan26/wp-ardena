<?php
Redux::setSection( $theme_options, array(
	'id' => 'main_sec_title_wrapper',
	'title' => esc_html__('Title wrapper', 'mint'),
	'icon' => 'el el-star',
) );

Redux::setSection( $theme_options, array(
	'id' => 'sec_title_wrapper',
	'title' => esc_html__('Title wrapper settings', 'mint'),
	'subsection' => true,
	'fields' => array(
		array(
			'id' => 'title_wrapper',
			'type' => 'switch',
			'title' => esc_html__('Enable this layout part?', 'mint'),
			'subtitle' => esc_html__('If on, this layout part will be displayed.', 'mint'),
			'default' => 1,
		),

		array(
			'id' => 'title_wrapper--full_height',
			'type' => 'switch',
			'title' => esc_html__('Full viewport height', 'mint'),
			'subtitle' => esc_html__('If on, title wrapper will have same height than viewport/window.', 'mint'),
			'default' => 0,
		),

		array(
			'id' => 'title_wrapper--parallax',
			'type' => 'switch',
			'title' => esc_html__('Parallax', 'mint'),
			'default' => 1,
		),

		array(
			'id' => 'title_wrapper--desc',
			'type' => 'switch',
			'title' => esc_html__('Description after title', 'mint'),
			'default' => 1,
			'required' => array('title_wrapper--title_and_desc_cols_sm', '!=', 0),
		),

		array(
			'id' => 'title_wrapper--breadcrumb',
			'type' => 'switch',
			'title' => esc_html__('Breadcrumb', 'mint'),
			'default' => 1,
		),
	)
) );

Redux::setSection( $theme_options, array(
	'id' => 'sec_title_wrapper_styles',
	'title' => esc_html__('Title wrapper styles', 'mint'),
	'subsection' => true,
	'fields' => array(
		array(
			'id' => 'title_wrapper_styles--border',
			'type' => 'border',
			'title' => esc_html__('Title wrapper border', 'mint'),
			'all' => false,
			'left' => false,
			'right' => false,
		),

		array(
			'id' => 'title_wrapper_styles--padding',
			'type' => 'spacing',
			'mode' => 'padding',
			'title' => esc_html__('Title wrapper padding', 'mint'),
			'right' => false,
			'left' => false,
			'units' => 'px',
		),

		array(
			'id' => 'title_wrapper_styles--align',
			'type' => 'button_set',
			'title' => esc_html__('Text align', 'mint'),
			'options' => array(
				'left' => esc_html__('Left', 'mint'),
				'center' => esc_html__('Center', 'mint'),
				'right' => esc_html__('Right', 'mint'),
			),
			'default' => 'center',
		),

		array(
			'id' => 'title_wrapper_styles--font_breadcrumb',
			'type' => 'typography',
			'title' => esc_html__('Breadcrumb typography', 'mint'),
			'google' => true,
			'font-backup' => true,
			'letter-spacing' => true,
			'text-transform' => true,
			'subsets' => true,
			'text-align' => false,
			'all_styles' => true,
		),

		array(
			'id' => 'title_wrapper_styles--font_breadcrumb__custom_family',
			'type' => 'text',
			'title' => esc_html__('Breadcrumb typography: custom font family', 'mint'),
			'subtitle' => esc_html__('You can use here your Typekit fonts.', 'mint'),
			'default' => '',
			'placeholder' => '"proxima-nova", Arial, Helvetica, sans-serif',
			'validate' => 'no_html',
		),

		array(
			'id' => 'title_wrapper_styles--font_subtitle',
			'type' => 'typography',
			'title' => esc_html__('Subtitle typography', 'mint'),
			'google' => true,
			'font-backup' => true,
			'letter-spacing' => true,
			'text-transform' => true,
			'subsets' => true,
			'text-align' => false,
			'all_styles' => true,
		),

		array(
			'id' => 'title_wrapper_styles--font_subtitle__custom_family',
			'type' => 'text',
			'title' => esc_html__('Subtitle typography: custom font family', 'mint'),
			'subtitle' => esc_html__('You can use here your Typekit fonts.', 'mint'),
			'default' => '',
			'placeholder' => '"proxima-nova", Arial, Helvetica, sans-serif',
			'validate' => 'no_html',
		),

		array(
			'id' => 'title_wrapper_styles--font_title',
			'type' => 'typography',
			'title' => esc_html__('Title typography', 'mint'),
			'google' => true,
			'font-backup' => true,
			'letter-spacing' => true,
			'text-transform' => true,
			'subsets' => true,
			'text-align' => false,
			'all_styles' => true,
		),

		array(
			'id' => 'title_wrapper_styles--font_title__custom_family',
			'type' => 'text',
			'title' => esc_html__('Title typography: custom font family', 'mint'),
			'subtitle' => esc_html__('You can use here your Typekit fonts.', 'mint'),
			'default' => '',
			'placeholder' => '"proxima-nova", Arial, Helvetica, sans-serif',
			'validate' => 'no_html',
		),

		array(
			'id' => 'title_wrapper_styles--font_desc',
			'type' => 'typography',
			'title' => esc_html__('Description typography', 'mint'),
			'subtitle' => esc_html__('Typography to optional description used in pages.', 'mint'),
			'google' => true,
			'font-backup' => true,
			'letter-spacing' => true,
			'text-transform' => true,
			'subsets' => true,
			'text-align' => false,
			'all_styles' => true,
		),

		array(
			'id' => 'title_wrapper_styles--font_desc__custom_family',
			'type' => 'text',
			'title' => esc_html__('Description typography: custom font family', 'mint'),
			'subtitle' => esc_html__('You can use here your Typekit fonts.', 'mint'),
			'default' => '',
			'placeholder' => '"proxima-nova", Arial, Helvetica, sans-serif',
			'validate' => 'no_html',
		),

		array(
			'id' => 'title_wrapper_styles--bg',
			'type' => 'background',
			'title' => esc_html__('Title wrapper background', 'mint'),
		),
	)
) );
