<?php
Redux::setSection( $theme_options, array(
	'id' => 'sec_projects',
	'title' => esc_html__('Projects', 'mint'),
	'desc' => esc_html__('Change projects templates.', 'mint'),
	'icon' => 'el el-idea',
	'fields' => array(
		array(
			'id' => 'projects--title',
			'type' => 'text',
			'title' => esc_html__('Projects title', 'mint'),
			'default' => 'Projects',
		),

		array(
			'id' => 'projects--layout--sidebars',
			'type' => 'image_select',
			'title' => esc_html__('Sidebars in projects', 'mint'),
			'subtitle' => esc_html__('Select the layout to be used in projects.', 'mint'),
			'options' => array(
				'' => array(
					'alt' => 'default',
					'img' => self::$theme_uri . '/assets/images/sidebars/def.png'
				),
				'without' => array(
					'alt' => 'without sidebar',
					'img' => self::$theme_uri . '/assets/images/sidebars/1c.png'
				),
				'left' => array(
					'alt' => 'sidebar at left',
					'img' => self::$theme_uri . '/assets/images/sidebars/2cl.png'
				),
				'right' => array(
					'alt' => 'sidebar at right',
					'img' => self::$theme_uri . '/assets/images/sidebars/2cr.png'
				),
				'both' => array(
					'alt' => 'both sidebars',
					'img' => self::$theme_uri . '/assets/images/sidebars/3cm.png'
				),
				'both_left' => array(
					'alt' => 'both sidebars at left',
					'img' => self::$theme_uri . '/assets/images/sidebars/3cl.png'
				),
				'both_right' => array(
					'alt' => 'both sidebars at right',
					'img' => self::$theme_uri . '/assets/images/sidebars/3cr.png'
				)
			),
			'default' => '',
		),

		array(
			'id' => 'projects--layout--content_width',
			'type' => 'select',
			'title' => esc_html__('Content container type', 'mint'),
			'subtitle' => esc_html__('Define container configuration to be used, it can be normal, expanded or compact.', 'mint'),
			'options' => array(
				'expanded' => esc_html__('Expanded', 'mint'),
				'normal' => esc_html__('Normal', 'mint'),
				'compact' => esc_html__('Compact', 'mint'),
			),
			'default' => '',
		),

		array(
			'id' => 'projects--categories_align',
			'type' => 'button_set',
			'title' => esc_html__('Categories filter align', 'mint'),
			'options' => array(
				'left' => esc_html__('Left', 'mint'),
				'center' => esc_html__('Center', 'mint'),
				'right' => esc_html__('Right', 'mint'),
			),
			'default' => 'center',
		),

		array(
			'id' => 'projects--columns',
			'type' => 'slider',
			'title' => esc_html__('Columns', 'mint'),
			'subtitle' => esc_html__('Define the columns numbers to be used in the projects.', 'mint'),
			'default' => '3',
			'min' => '1',
			'step' => '1',
			'max' => '4',
		),

		array(
			'id' => 'projects--gap',
			'type' => 'slider',
			'title' => esc_html__('Gap', 'mint'),
			'subtitle' => esc_html__('Define gap between columns.', 'mint'),
			'default' => '30',
			'min' => '0',
			'step' => '2',
			'max' => '30',
		),

		array(
			'id' => 'projects--img_size',
			'type' => 'select',
			'title' => esc_html__('Image size', 'mint'),
			'options' => self::get_image_size_names(),
			'default' => 'rectangle_medium__crop',
			'validate' => 'not_empty',
		),

		array(
			'id' => 'projects--font_size',
			'type' => 'button_set',
			'title' => esc_html__('Font size', 'mint'),
			'options' => array(
				'medium' => esc_html__('Medium', 'mint'),
				'large' => esc_html__('Large', 'mint'),
			),
			'default' => 'medium',
		),

		array(
			'id' => 'projects--bg_overlay',
			'type' => 'color_rgba',
			'title' => esc_html__('Color to overlay image on hover', 'mint'),
			'default' => array(
				'alpha' => 0,
			),
		),

		array(
			'id' => 'projects--align',
			'type' => 'button_set',
			'title' => esc_html__('Title align', 'mint'),
			'options' => array(
				'left' => esc_html__('Left', 'mint'),
				'center' => esc_html__('Center', 'mint'),
				'right' => esc_html__('Right', 'mint'),
			),
			'default' => 'center',
		),

		array(
			'id' => 'projects--animation',
			'type' => 'select',
			'title' => esc_html__('Animation on hover', 'mint'),
			'options' => array(
				'1' => esc_html__('Simple', 'mint'),
				'2' => esc_html__('Simple (reverse)', 'mint'),
				'3' => esc_html__('Blur', 'mint'),
				'4' => esc_html__('Colorful', 'mint'),
				'5' => esc_html__('Bordered', 'mint'),
				'6' => esc_html__('Slice', 'mint'),
				'7' => esc_html__('Grayscale', 'mint'),
			),
			'default' => '',
		),

		array(
			'id' => 'projects--nav',
			'type' => 'switch',
			'title' => esc_html__('Projects navigation', 'mint'),
			'subtitle' => esc_html__('If on, navigation will be displayed below projects.', 'mint'),
			'default' => 1,
		),
	)
) );
