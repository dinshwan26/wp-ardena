<form
	class="search-form"
	role="search"
	method="get"
	action="<?php echo esc_url(home_url('/')); ?>"
>
	<input
		class="search-form__input js-focus-me"
		type="search"
		value="<?php echo get_search_query(); ?>"
		name="s"
		placeholder="<?php esc_html_e('Search', 'mint'); ?>"
		size="40"
	>
	<button
		class="search-form__submit"
		type="submit"
		value="<?php esc_html_e('Search', 'mint'); ?>"
	>
		<span class="search-form__submit-icon"><span class="icon-search"></span></span>
		<span class="search-form__submit-text"><?php esc_html_e('Search', 'mint'); ?></span>
	</button>
	<?php if (MintOptions::get('search--post_type') !== 'all') { ?>
		<input type="hidden" name="post_type" value="<?php echo esc_attr(MintOptions::get('search--post_type')); ?>">
	<?php } ?>
</form>
