<?php
$theme_dir = get_template_directory();

require_once $theme_dir . '/classes/Theme.php';
require_once $theme_dir . '/classes/Options.php';
require_once $theme_dir . '/classes/CSS.php';
require_once $theme_dir . '/classes/Setup.php';
require_once $theme_dir . '/classes/Helpers.php';
require_once $theme_dir . '/classes/Modules.php';
require_once $theme_dir . '/classes/PostFormats.php';
require_once $theme_dir . '/classes/Shop.php';
require_once $theme_dir . '/classes/Addons.php';
require_once $theme_dir . '/plugins/tgm-plugin-activation/init.php';
