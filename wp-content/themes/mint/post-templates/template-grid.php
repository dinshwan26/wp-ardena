<?php
global $mint_post_loop;

$post_format = get_post_format();
$post_classes = 'post-grid js-post-grid _' . ($post_format ? $post_format : 'standard');
$content_width = MintOptions::get('layout--content_width');
$sidebar_location = MintHelpers::get_sidebar_location();

if ($content_width == 'expanded') {
	if ($sidebar_location) {
		$column_classes = 'col-xs-12 col-md-6 col-xl-4 col-xxxl-3';
	} else {
		$column_classes = 'col-xs-12 col-sm-6 col-md-4 col-xl-3 col-xxxl-2';
	}
} elseif ($content_width == 'normal') {
	if ($sidebar_location) {
		$column_classes = 'col-xs-12 col-md-6';
	} else {
		$column_classes = 'col-xs-12 col-sm-6 col-md-4';
	}
} else {
	$column_classes = 'col-xs-12 col-sm-6';
}

if ($mint_post_loop++ == 1) {
	?><div class="<?php echo esc_attr($column_classes); ?> js-masonry-sizer"></div><?php
}

$double_width = MintOptions::get_metaboxes_option('local_single_post--double_width');

if ($double_width) {

	$post_classes .= ' _double';

	if ($content_width == 'expanded') {
		if ($sidebar_location) {
			$column_classes = 'col-xs-12 col-xl-8 col-xxxl-6';
		} else {
			$column_classes = 'col-xs-12 col-md-8 col-xl-6 col-xxxl-4';
		}
	} elseif ($content_width == 'normal') {
		if ($sidebar_location) {
			$column_classes = 'col-xs-12';
		} else {
			$column_classes = 'col-xs-12 col-md-8';
		}
	} else {
		$column_classes = 'col-xs-12';
	}

}

?>

<div class="<?php echo esc_attr($column_classes); ?>">
	<div class="animate-on-screen js-animate-on-screen">
		<article id="post-<?php the_ID(); ?>" <?php post_class($post_classes); ?>>
			<?php get_template_part('post-templates/grid/content', $post_format); ?>
		</article>
	</div>
</div>
