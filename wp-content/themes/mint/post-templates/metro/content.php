<?php global $mint_post_loop; ?>

<?php MintPostFormats::image('post-metro__img'); ?>

<?php get_template_part('post-templates/metro/part', 'category'); ?>

<div class="post-metro__content">
	<div class="post-metro__date">
		<time datetime="<?php echo esc_attr(get_the_date('c')); ?>"><?php echo esc_html(get_the_date()); ?></time>
	</div>

	<?php the_title('<header><h3 class="post-metro__title">', '</h3></header>'); ?>

	<?php if ($mint_post_loop % 3 == 2) { ?>
		<div class="post-metro__desc hidden-xxs">
			<?php echo apply_filters('the_excerpt', MintPostFormats::esc(get_the_excerpt())); ?>
		</div>
	<?php } ?>
</div>

<?php get_template_part('post-templates/metro/part', 'link'); ?>
