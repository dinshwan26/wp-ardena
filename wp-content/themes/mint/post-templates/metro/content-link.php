<?php $ar_current_post_content = MintPostFormats::link(); ?>

<?php MintPostFormats::image('post-metro__img'); ?>

<?php get_template_part('post-templates/metro/part', 'category'); ?>

<div class="post-metro__content">
	<?php the_title('<header><h3 class="post-metro__title">', '</h3></header>'); ?>

	<div class="post-metro__desc">
		<?php echo esc_url($ar_current_post_content['link']); ?>
	</div>
</div>

<a
	href="<?php echo esc_url($ar_current_post_content['link']); ?>"
	class="post-metro__link"
	rel="bookmark"
	title="<?php the_title(); ?>"
></a>
