<?php MintPostFormats::image('post-metro__img'); ?>

<?php get_template_part('post-templates/metro/part', 'category'); ?>

<div class="post-metro__content">
	<blockquote class="post-metro__desc">
		<?php echo apply_filters('the_content', MintPostFormats::esc()); ?>
	</blockquote>

	<?php the_title('<cite class="post-metro__title">', '</cite>'); ?>
</div>

<?php get_template_part('post-templates/metro/part', 'link'); ?>
