<?php get_template_part('post-templates/grid/part', 'category'); ?>

<div class="post-grid__content">
	<div class="post-grid__icon"><i class="fa fa-twitter"></i></div>

	<blockquote class="post-grid__desc"><?php echo apply_filters('the_content', MintPostFormats::esc()); ?></blockquote>
</div>

<?php get_template_part('post-templates/grid/part', 'meta'); ?>
