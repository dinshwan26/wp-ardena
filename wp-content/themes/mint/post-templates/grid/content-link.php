<?php $ar_current_post_content = MintPostFormats::link(); ?>

<?php get_template_part('post-templates/grid/part', 'category'); ?>

<div class="post-grid__content">
	<div class="post-grid__icon">
		<span class="icon-link"></span>
	</div>

	<?php get_template_part('post-templates/grid/part', 'header'); ?>

	<div class="post-grid__desc">
		<?php echo esc_url($ar_current_post_content['link']); ?>
	</div>
</div>

<?php get_template_part('post-templates/grid/part', 'meta'); ?>

<a
	href="<?php echo esc_url($ar_current_post_content['link']); ?>"
	class="post-grid__link"
	rel="bookmark"
	title="<?php the_title(); ?>"
></a>
