<?php get_template_part('post-templates/grid/part', 'category'); ?>

<div class="post-grid__content">
	<div class="post-grid__icon"></div>

	<blockquote class="post-grid__desc"><?php echo apply_filters('the_content', MintPostFormats::esc()); ?></blockquote>
</div>

<?php the_title('<cite class="post-grid__title">', '</cite>'); ?>

<?php get_template_part('post-templates/grid/part', 'meta'); ?>
