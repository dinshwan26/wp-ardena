<?php get_template_part('post-templates/grid/part', 'img'); ?>

<?php get_template_part('post-templates/grid/part', 'category'); ?>

<div class="post-grid__content">
	<?php get_template_part('post-templates/grid/part', 'header'); ?>

	<div class="post-grid__desc">
		<?php echo apply_filters('the_excerpt', MintPostFormats::esc(get_the_excerpt())); ?>
	</div>
</div>

<?php get_template_part('post-templates/grid/part', 'meta'); ?>

<?php get_template_part('post-templates/grid/part', 'link'); ?>
