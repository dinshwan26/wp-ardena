<?php
$post_format = get_post_format();
$post_classes = 'post-masonry _' . ($post_format ? $post_format : 'standard');
$content_width = MintOptions::get('layout--content_width');
$sidebar_location = MintHelpers::get_sidebar_location();

if ($content_width == 'expanded') {
	if ($sidebar_location) {
		$column_classes = 'col-xs-12 col-md-6 col-xl-4 col-xxxl-3';
	} else {
		$column_classes = 'col-xs-12 col-sm-6 col-md-4 col-xl-3 col-xxxl-2';
	}
} elseif ($content_width == 'normal') {
	if ($sidebar_location) {
		$column_classes = 'col-xs-12 col-md-6';
	} else {
		$column_classes = 'col-xs-12 col-sm-6 col-md-4';
	}
} else {
	$column_classes = 'col-xs-12 col-sm-6';
}
?>

<div class="<?php echo esc_attr($column_classes); ?>">
	<div class="animate-on-screen js-animate-on-screen">
		<article id="post-<?php the_ID(); ?>" <?php post_class($post_classes); ?>>
			<?php get_template_part('post-templates/masonry/content', $post_format); ?>
		</article>
	</div>
</div>
