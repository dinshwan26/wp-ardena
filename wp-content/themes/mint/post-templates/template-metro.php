<?php
global $mint_post_loop;

$post_format = get_post_format();
$post_classes = 'post-metro _' . ($post_format ? $post_format : 'standard');
$column_classes = 'col-xs-12 col-md-6';

if ($mint_post_loop == 1) {
	?><div class="col-xs-12 col-md-6 col-lg-3 js-masonry-sizer"></div><?php
}

if (++$mint_post_loop % 3 == 2) {
	$post_classes .= ' _double';
} else {
	$column_classes .= ' col-lg-3';
}
?>

<div class="<?php echo esc_attr($column_classes); ?>">
	<div class="animate-on-screen js-animate-on-screen">
		<article id="post-<?php the_ID(); ?>" <?php post_class($post_classes); ?>>
			<?php get_template_part('post-templates/metro/content', $post_format); ?>
		</article>
	</div>
</div>
