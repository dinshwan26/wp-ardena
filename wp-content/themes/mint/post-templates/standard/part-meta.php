<div class="post-standard__meta">
	<?php // @todo if (MintOptions::get('posts--author')) { ?>
		<?php MintHelpers::post_author(); ?>
	<?php // } ?>

	<?php if (!post_password_required() && comments_open() && get_comments_number() != 0) { ?>
		<a href="<?php comments_link(); ?>" class="post-standard__comments">
			<span class="icon-speech-bubble"></span>
			<span class="post-standard_comments-count"><?php comments_number('', '1', '%'); ?></span>
		</a>
	<?php } ?>
</div>
