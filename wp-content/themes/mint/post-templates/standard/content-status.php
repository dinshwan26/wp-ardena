<div class="post-standard__content">
	<div class="post-standard__aside">
		<div class="post-standard__icon"><i class="fa fa-twitter"></i></div>
	</div>

	<div class="post-standard__main">
		<blockquote class="post-standard__desc">
			<?php echo apply_filters('the_content', MintPostFormats::esc()); ?>
		</blockquote>
	</div>
</div>
