<?php
the_title(sprintf('
	<header>
		<h3 class="post-standard__title">
			<a href="%s" class="post-standard__link" rel="bookmark">', esc_url(get_permalink())), '</a>
		</h3>
	</header>
');
?>
