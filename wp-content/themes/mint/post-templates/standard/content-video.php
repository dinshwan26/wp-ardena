<?php $current_post_content = MintPostFormats::video(); ?>

<div class="post-standard__content">
	<?php get_template_part('post-templates/standard/part', 'category'); ?>

	<?php get_template_part('post-templates/standard/part', 'aside'); ?>

	<div class="post-standard__main">
		<?php get_template_part('post-templates/standard/part', 'header'); ?>

		<div class="post-standard__desc">
			<?php echo apply_filters('the_content', $current_post_content); ?>
		</div>

		<?php get_template_part('post-templates/standard/part', 'meta'); ?>
	</div>
</div>
