<?php $ar_current_post_content = MintPostFormats::link(); ?>

<div class="post-standard__content">
	<div class="post-standard__aside">
		<div class="post-standard__icon"><span class="icon-link"></span></div>
	</div>

	<div class="post-standard__main">
		<?php the_title(sprintf('
			<header>
				<h3 class="post-standard__title">
					<a href="%s" class="post-standard__link" rel="bookmark">
					', esc_url($ar_current_post_content['link'])), '
					</a>
				</h3>
			</header>
		'); ?>

		<div class="post-standard__desc">
			<?php echo apply_filters('the_content', $ar_current_post_content['link']); ?>
		</div>
	</div>
</div>
