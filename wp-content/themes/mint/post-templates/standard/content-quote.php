<div class="post-standard__content">
	<div class="post-standard__aside">
		<div class="post-standard__icon"></div>
	</div>

	<div class="post-standard__main">
		<blockquote class="post-standard__desc">
			<?php echo apply_filters('the_content', MintPostFormats::esc()); ?>
		</blockquote>

		<?php the_title('<cite class="post-standard__title">', '</cite>'); ?>
	</div>
</div>
