<div class="post-standard__aside">
	<time datetime="<?php echo esc_attr(get_the_date('c')); ?>" class="post-standard__date">
		<span class="post-standard__date-day"><?php echo esc_html(get_the_date('j')); ?></span>
		<br>
		<span class="post-standard__date-month"><?php echo esc_html(get_the_date('F')); ?></span>
	</time>
</div>
