<?php
$post_format = get_post_format();
$classes = 'post-standard _' . ($post_format ? $post_format : 'standard');
?>

<article id="post-<?php the_ID(); ?>" <?php post_class($classes); ?>>
	<?php get_template_part('post-templates/standard/content', $post_format); ?>
</article>
