<?php the_title('
	<header>
		<h3 class="post-boxed__title">
			<a href="' . esc_url(get_permalink()) . '" class="post-boxed__title-link" rel="bookmark">', '</a>
		</h3>
	</header>
'); ?>
