<?php get_template_part('post-templates/boxed/part', 'category'); ?>

<div class="post-boxed__content">
	<div class="post-boxed__icon"></div>

	<blockquote class="post-boxed__desc">
		<?php echo apply_filters('the_content', MintPostFormats::esc()); ?>
	</blockquote>

	<?php the_title('<cite class="post-boxed__title">', '</cite>'); ?>
</div>

<?php get_template_part('post-templates/boxed/part', 'meta'); ?>
