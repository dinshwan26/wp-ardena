<?php get_template_part('post-templates/masonry/part', 'category'); ?>

<div class="post-masonry__content">
	<div class="post-masonry__icon"><i class="fa fa-twitter"></i></div>

	<blockquote class="post-masonry__desc"><?php echo apply_filters('the_content', MintPostFormats::esc()); ?></blockquote>
</div>

<?php get_template_part('post-templates/masonry/part', 'meta'); ?>
