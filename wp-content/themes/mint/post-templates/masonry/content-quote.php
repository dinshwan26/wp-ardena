<?php get_template_part('post-templates/masonry/part', 'category'); ?>

<div class="post-masonry__content">
	<div class="post-masonry__icon"></div>

	<blockquote class="post-masonry__desc"><?php echo apply_filters('the_content', MintPostFormats::esc()); ?></blockquote>
</div>

<?php the_title('<cite class="post-masonry__title">', '</cite>'); ?>

<?php get_template_part('post-templates/masonry/part', 'meta'); ?>
