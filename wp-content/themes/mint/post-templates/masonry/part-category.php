<?php if (MintHelpers::is_categorized_blog()) { ?>
	<div class="post-masonry__categories">
		<?php the_category(' '); ?>
	</div>
<?php } ?>
