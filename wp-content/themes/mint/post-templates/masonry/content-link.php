<?php $ar_current_post_content = MintPostFormats::link(); ?>

<?php get_template_part('post-templates/masonry/part', 'category'); ?>

<div class="post-masonry__content">
	<div class="post-masonry__icon">
		<span class="icon-link"></span>
	</div>

	<?php get_template_part('post-templates/masonry/part', 'header'); ?>

	<div class="post-masonry__desc">
		<?php echo esc_url($ar_current_post_content['link']); ?>
	</div>
</div>

<?php get_template_part('post-templates/masonry/part', 'meta'); ?>

<a
	href="<?php echo esc_url($ar_current_post_content['link']); ?>"
	class="post-masonry__link"
	rel="bookmark"
	title="<?php the_title(); ?>"
></a>
