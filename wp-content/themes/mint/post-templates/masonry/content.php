<?php get_template_part( 'post-templates/masonry/part', 'img' ); ?>

<div class="post-masonry__content">
	<?php get_template_part('post-templates/masonry/part', 'category'); ?>

	<?php get_template_part('post-templates/masonry/part', 'header'); ?>

	<div class="post-masonry__desc">
		<?php echo apply_filters('the_excerpt', MintPostFormats::esc(get_the_excerpt())); ?>
	</div>
</div>

<?php get_template_part('post-templates/masonry/part', 'meta'); ?>

<?php get_template_part('post-templates/masonry/part', 'link'); ?>
