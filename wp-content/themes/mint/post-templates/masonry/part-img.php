<div class="post-masonry__img-wrapper">
	<?php
	if (has_post_thumbnail()) {
			the_post_thumbnail(MintOptions::get('posts--img_size'), array('title' => get_the_title(), 'class' => 'post-masonry__img'));
	}
	?>
</div>
