<?php
/**
 * The template for displaying search results pages.
 */

get_header(); ?>

	<?php if (have_posts()) { ?>

		<div class="search-page">
			<p><?php esc_html_e('If you are not happy with the results below please do another search.', 'mint'); ?></p>
			<?php get_search_form(); ?>
		</div>

		<div class="row js-masonry">

			<?php
			$column_classes = MintHelpers::get_responsive_column_classes(MintOptions::get('search--columns'));

			while (have_posts()) { the_post();
			?>

				<div class="<?php echo esc_attr(join(' ', $column_classes)); ?>">
					<div class="animate-on-screen js-animate-on-screen">
						<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
							<?php get_template_part('content', 'search'); ?>
						</article>
					</div>
				</div>

			<?php } ?>

		</div>

		<?php MintHelpers::posts_navigation(); ?>

	<?php } else { ?>

		<?php get_template_part('content', 'none'); ?>

	<?php } ?>

<?php get_footer(); ?>
